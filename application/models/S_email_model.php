<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class S_email_model extends CI_Model
{

    public function __construct()
	{

        parent::__construct();

    }

   
	public function find_suggestion() 
    {
	        $this->db->select('suggestion_email');
			$this->db->from('bom_config');
			
            $query = $this->db->get();		   
		    if($query)
		    return $query->row(); 
    }
	 	
}