<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
 
class Login_user_model extends CI_Model
{

    public function __construct()
	{

        parent::__construct();

    }

   
	public function sel($email,$password){
	$password=md5($password);
	       
		    $this->db->select('*');
			$this->db->from('bom_manage_user');
			$this->db->where('u_email',$email);    
			$this->db->where('password',$password);
			$this->db->where('is_active','1');
           $query=$this->db->get();
		if($query)
		return $query->row(); 
    }
	
	public function selectdate($email){
	
				$this->db->select('manageuser.m_id,addmatch.*');
				$this->db->from('bom_manage_user as manageuser');
				$this->db->where('u_email',$email);
				$this->db->join('bom_add_match as addmatch','addmatch.m_id=manageuser.m_id','left');
				 $query=$this->db->get();
				if($query!=NULL){
						return $query->row();
				}else{
					return 0;
				}
	}
	public function update_last_login($email){
		$c_date=time();	
		$data=array('last_login'=>$c_date);
     	$this->db->where('u_email',$email);    
        $this->db->update('bom_manage_user',$data);
		return true;
    } 	
	public function config_detail(){

	       
		    $this->db->select('*');
			$this->db->from('bom_config');
			
           $query=$this->db->get();
		if($query)
		return $query->row(); 
    }
	public function user_detail($email){

	       
		    $this->db->select('manageuser.*,userteam.player_id,sum(livescoreboard.price) as price');
			
			$this->db->from('bom_manage_user as manageuser');
			
			$this->db->where('u_email',$email);
			
			$this->db->join('bom_user_team as userteam','manageuser.user_id=userteam.user_id','left'); 
			
			$this->db->join('bom_live_score_board as livescoreboard','userteam.player_id=livescoreboard.player_id','left');
            $query=$this->db->get();
		   			
				if($query)
						return $query->row(); 
			/* $this->db->select('*');
			$this->db->from('bom_manage_user');
			$this->db->where('u_email',$email); 
            $query=$this->db->get();
			if($query)
			return $query->row(); */
    }
}