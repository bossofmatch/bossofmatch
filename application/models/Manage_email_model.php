<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Manage_email_model extends CI_Model
{

    public function __construct()
	{

        parent::__construct();

    }

   
	public function record_count() 
	{
        return $this->db->count_all("bom_invite_mail");
    }
	
	
	public function fetch_emails($limit, $start) 
	 {
        $this->db->limit($limit, $start);
		$this->db->select('*');
		$this->db->from('bom_invite_mail');
		$this->db->join('bom_manage_user', 'bom_invite_mail.freiend_id  = bom_manage_user.user_id', 'left');
		$this->db->order_by("i_id ", "desc"); 
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
   }
	 
	
	public function m_delete($id)
	{
			
		$this->db->where('i_id', $id);
        $this->db->delete('bom_invite_mail');	
         $query = $this->db->get();
				
		if($query)
		return $query;
				
	} 	
	
	
	public function record_count1($keys) 
			{
				$this->db->select('*');
				$this->db->from('bom_invite_mail');
				$this->db->join('bom_manage_user', 'bom_invite_mail.freiend_id  = bom_manage_user.user_id', 'left');
				$this->db->like('i_email', $keys, 'both');
				$this->db->or_like('i_name', $keys, 'both');
				$this->db->or_like('bom_manage_user.u_name', $keys, 'both');
			
				$this->db->order_by("i_id", "desc"); 
				$query = $this->db->get();
				return $count = $query->num_rows();
				
			}
			
			public function fetch_search($limit, $start,$keys) 
			{
		
				$this->db->limit($limit, $start);	
				$this->db->select('*');
				$this->db->from('bom_invite_mail');
				$this->db->join('bom_manage_user', 'bom_invite_mail.freiend_id  = bom_manage_user.user_id', 'left');
				$this->db->like('i_email', $keys, 'both');
				$this->db->or_like('i_name', $keys, 'both');
				$this->db->or_like('bom_manage_user.u_name', $keys, 'both');
				$this->db->order_by("i_id", "desc"); 
				$query = $this->db->get(); 
				if ($query->num_rows() > 0)
				 {
					foreach ($query->result() as $row)
					 {
						$data[] = $row;
					}
					
					return $data;
				}
			}
}
