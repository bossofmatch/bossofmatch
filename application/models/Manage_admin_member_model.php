<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Manage_admin_member_model extends CI_Model
{

		  public function __construct(){
	  
			  parent::__construct();
	  
		  }
	  
	  
		 public function check($password,$user_email) 
		 {
				 $this->db->select('password');
				 $this->db->from('bom_manage_admin_user');
				 $this->db->where('a_email',$user_email);
				 $query = $this->db->get();
				 if($query)
				 return $query->row();
		  }
		  
		  
		  public function update($npassword,$user_email) 
		  {
			  	$this->db->where('a_email',$user_email);
				$this->db->set('password',$npassword);
			  	$query=$this->db->update('bom_manage_admin_user');
	  			if($query)
			    return $query;
		  }
		  
		  public function record_count() 
		  {
				return $this->db->count_all("bom_manage_admin_user");
		  }
		  
		  
		  public function fetch_member($limit, $start){
			  
				$this->db->limit($limit, $start);
				$this->db->select('*');
				$this->db->from('bom_manage_admin_user');
	  			$this->db->join('bom_role', 'bom_manage_admin_user.role = bom_role.r_id', 'left');
			  	$this->db->order_by("a_id", "desc"); 
			  	$query = $this->db->get();
	   			if ($query->num_rows() > 0) {
					
				  	foreach ($query->result() as $row) {
						  $data[] = $row;
				  	}
				  	return $data;
			  	}
			  	return false;
		  }
		  
		  
		   public function m_active($id,$active){
				
				$this->db->where('a_id',$id);
				$this->db->set('is_active',$active);
			  	$query=$this->db->update('bom_manage_admin_user');
	  			if($query)
			    return $query;
	  }
	  
	  
	   public function m_delete($email){
			 
			  $this->db->where('a_email', $email);
			  $query=$this->db->delete('bom_manage_admin_user'); 		
			  if($query)
			  return $query;
	   }
	   
	   public function selectrole(){
		
		
			  $this->db->select('*');
			  $this->db->from('bom_role');
			  $query=$this->db->get();
			  foreach ($query->result() as $row) {
				  
					$data[] = $row;
			  }
			  return $data;
			     
	   }
	   
	   
	   public function insertdata($data){
		   
		   		return $query=$this->db->insert('bom_manage_admin_user',$data);
	   }
	   
	   	public function m_edit($id){
			
			$this->db->select('*');
			$this->db->from('bom_manage_admin_user');
			$this->db->where('a_id',$id);
			$query=$this->db->get();
			return $query->row();
		}
		
		
		public function userroll(){
			
			  $this->db->select('*');
			  $this->db->from('bom_role');
			  $query=$this->db->get();
			  foreach ($query->result() as $row){
				  
					   $data[]=$row;		 
			  }
			  return $data;
		}
		
		
		public function update_data($id,$data){
			  
			  	$this->db->where('a_id',$id);
			    $query=$this->db->update('bom_manage_admin_user',$data);
				return $query;
		}

}