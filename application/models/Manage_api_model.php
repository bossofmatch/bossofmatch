<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Manage_api_model extends CI_Model
{

		public function __construct(){
	
			   parent::__construct();
	
		}

		 public function tournament_name(){
			 
				$this->db->select('*');
				$this->db->from('bom_tournament_teams');
				$query = $this->db->get();
	   			if ($query->num_rows() > 0) {
					
						foreach ($query->result() as $row) {
						
							  $data[] = $row;
						}
						return $data;
				}
				return false;
				
		 }
		 
		 
		  public function fetch_data($id){
			
				 $this->db->select('*')->from('bom_manage_player')->where('team_id',$id);
				 $query=$this->db->get();
				 if ($query->num_rows() > 0){
					 
					   foreach ($query->result() as $row){
						   
								$data[] = $row;
					   }
					   return $data;
				 }
				 return 0;
		}
		
		
		public function checkexitsornot($arrayid,$arrayname){
		
				$i=0;
				foreach($arrayid as $key=>$value){
				
					$unike_name=preg_replace('/\s\s+/', ' ',$arrayname[$i]);
					$api_unike_name=array('api_unike_name'=>$unike_name);
					$this->db->where('player_id',$value);
					$this->db->update('bom_manage_player',$api_unike_name);
					$i++;
				}
				return true;
		}

}