<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Manage_member_model extends CI_Model
{

    public function __construct(){

        parent::__construct();

    }
	public function record_count(){
        return $this->db->count_all("bom_manage_user");
    }
	
	
	public function fetch_member($limit, $start){
        $this->db->limit($limit, $start);
		$this->db->select('*');
		$this->db->from('bom_manage_user');
		$this->db->order_by("user_id ", "desc"); 
        $query = $this->db->get();
        if ($query->num_rows() > 0){
            foreach ($query->result() as $row){
                $data[] = $row;
            }
            return $data;
        }
        return false;
   }
   
   public function m_active($user_email,$active){
		$data=array('is_active'=>$active);
        
       $this->db->where('u_email',$user_email);
        $query=$this->db->update('bom_manage_user',$data);
		if($query)
		return $query;

	}
	
	public function m_delete($user_name){
		$this->db->where('u_email', $user_name);
		$query=$this->db->delete('bom_manage_user'); 		
		if($query)
		return $query;
		
	}
	
	
	public function insert($data,$user_email){
		$insert="";
		$count=1;
		$this->db->select('u_email');
		$this->db->from('bom_manage_user');
		$query=$this->db->get();
		

		foreach ($query->result() as $row){
			if($row->u_email==$user_email){
				$count++;
			}									
		}
	 	if($count==1){
			 $insert= $this->db->insert('bom_manage_user',$data);
		}
			return $insert;	
		  
    }
	
	
	public function member_edit_open($id){
		$this->db->select('*');
		$this->db->from('bom_manage_user');
		$this->db->where('user_id',$id);
		$query=$this->db->get();
		if($query)
		return $query->row(); 
	}
	
	
	public function update($u_name,$age,$sex,$user_email,$total_account_amount){
		$data=array('u_name'=>$u_name,'age'=>$age,'sex'=>$sex,'total_account_amount'=>$total_account_amount);
        $this->db->where('u_email',$user_email);
        $query=$this->db->update('bom_manage_user',$data);
		if($query)
		return $query;
		
	}
	
	public function record_count1($keys){
		$this->db->select('*');
		$this->db->from('bom_manage_user');
		$this->db->like('u_name', $keys, 'both'); 
		$this->db->or_like('u_email', $keys, 'both'); 
		$this->db->order_by("user_id", "desc"); 
      	$query = $this->db->get();
	    return $count = $query->num_rows();
        
    }
	
	public function fetch_search($limit, $start,$keys){

        $this->db->limit($limit, $start);	
		$this->db->select('*');
		$this->db->from('bom_manage_user');
		$this->db->like('u_name', $keys, 'both'); 
		$this->db->or_like('u_email', $keys, 'both'); 
		$this->db->order_by("user_id", "desc"); 
      	$query = $this->db->get(); 
        if ($query->num_rows() > 0){
            foreach ($query->result() as $row){
                $data[] = $row;
            }
			
            return $data;
        }
		
        return false;
   }
   
   public function select($email2){
		
		$this->db->select('u_email,password,u_name');
		$this->db->from('bom_manage_user');
		$this->db->where('u_email',$email2);
		$query=$this->db->get();
		if($query)
		return $query->row();
	}
	public function select1(){
		
		$this->db->select('u_email,password,u_name');
		$this->db->from('bom_manage_user');
		$query=$this->db->get();
		if ($query->num_rows() > 0){
            foreach ($query->result() as $row){
                $data[] = $row;
            }
            return $data;
        }
        return false;
	}
	
	
	public function forgot_pass_insert($user_email,$password){
		
		 $data=array('password'=>$password);
        $this->db->where('u_email',$user_email);
        $query=$this->db->update('bom_manage_user',$data);
		if($query)
		return $query;
	}
}