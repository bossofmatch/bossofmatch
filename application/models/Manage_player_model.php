<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Manage_player_model extends CI_Model
{

		  public function __construct(){
	  
			  parent::__construct();
	  
		  }
	  
	  
		  public function record_count(){
			  
					  return $this->db->count_all("bom_manage_player");
		  }
		  
		  
		  public function fetch_match($limit, $start){
			  
				  $this->db->limit($limit, $start);
				  $this->db->select('bom_manage_player.*,bom_manage_tournament.t_short_name,btt.*');
				  $this->db->from('bom_manage_player');
				  $this->db->join('bom_manage_tournament', 'bom_manage_player.t_id= bom_manage_tournament.t_id', 'left');
				  $this->db->join('bom_tournament_teams as btt', 'bom_manage_player.team_id= btt.team_id', 'left');
				  $this->db->order_by("player_id ", "desc");
				  $query = $this->db->get();
				  if ($query->num_rows() > 0){
					  
						foreach ($query->result() as $row){
							$data[] = $row;
							
						}
						return $data;
				  }
				  return false;
		 }
		 
		 
		 public function record_count1($keys){
			 
				  $this->db->select('bom_manage_player.*,bom_manage_tournament.t_short_name,btt.*');
				  $this->db->from('bom_manage_player');
				  $this->db->join('bom_manage_tournament', 'bom_manage_player.t_id= bom_manage_tournament.t_id', 'left');
				  $this->db->join('bom_tournament_teams as btt', 'bom_manage_player.team_id= btt.team_id', 'left');
				  
				  $this->db->like('t_short_name', $keys, 'both');
				  $this->db->or_like('player_name', $keys, 'both');
				  $this->db->or_like('team_name', $keys, 'both');
				  $this->db->or_like('player_price', $keys, 'both');
				  
				  $this->db->order_by("player_id ", "desc");
				  $query = $this->db->get();
				  return $count = $query->num_rows();
		 }
		 
		 
		 public function fetch_search($limit, $start,$keys){
		
				  $this->db->limit($limit, $start);	
				  $this->db->select('bom_manage_player.*,bmt.t_short_name,btt.*');
				  $this->db->from('bom_manage_player');
				  $this->db->join('bom_manage_tournament as bmt', 'bom_manage_player.t_id= bmt.t_id', 'left');
				  $this->db->join('bom_tournament_teams as btt', 'bom_manage_player.team_id= btt.team_id', 'left');
				  
				  $this->db->like('t_short_name', $keys, 'both');
				  $this->db->or_like('player_name', $keys, 'both');
				  $this->db->or_like('team_name', $keys, 'both');
				  $this->db->or_like('player_price', $keys, 'both');
				  
				  $this->db->or_like('bmt.t_id', $keys, 'both');
				  $this->db->or_like('btt.team_id', $keys, 'both'); 
				  
				  
				  $this->db->order_by("bom_manage_player.t_id ", "desc"); 
				  $query = $this->db->get(); 
				  if ($query->num_rows() > 0){
					  
					  foreach ($query->result() as $row){
						  
						  		$data[] = $row;
					  }
					  return $data;
				  }
				  return false;
		   }
		 
		 
		 
		  public function t_full_name(){
					   
					   $this->db->select('t_id');
					   $this->db->select('t_full_name');
					   $this->db->from('bom_manage_tournament');
					   $full_name=$this->db->get();
					   if ($full_name->num_rows() > 0){
						   
							 foreach ($full_name->result() as $row){
								 
									  $data[] = $row;
							 }
							 return $data;
					   }
					   return 0;
		  }
	   
		  
		  public function m_delete($user_name){
		  
				  $this->db->where('player_id', $user_name);
				  $query=$this->db->delete('bom_manage_player'); 		
				  if($query)
				  return $query;
			  
		  }
		  
		  
		  public function check_name($name,$id,$teamid){
			  
				  $count=0;
				  $this->db->select('unike_payer_name');
				  $this->db->from('bom_manage_player');
				  $this->db->where('unike_payer_name',$name);
				  $this->db->where('t_id ',$id);
				  $this->db->where('team_id',$teamid);
				  $query=$this->db->get();
				  foreach ($query->result() as $row){
					  
						if($row->unike_payer_name==$name){
							
							$count++;
						 }
				  }
				  if($count){
					  
						  return $count;
				  }
				  else{
					  
					   return 0;
				  }
		  }
		  
		  public function insert_player($data){
			  
				  $query=$this->db->insert('bom_manage_player',$data);
				  return $query;
		  }
	  
		 
		 
		 public function ediplayer($id){
			 
				 $this->db->select('*');
				 $this->db->from('bom_manage_player');
				 $this->db->where('player_id',$id);
				 $query=$this->db->get();
				 if ($query->num_rows() > 0){
					 
					  foreach ($query->result() as $row){
						  
						  $data[] = $row;
					  }
					  return $query->row();
				}       
		 }
		 
		 
		 public function update_manage_player($id,$data){
			 
				$this->db->where('player_id',$id);
				$query=$this->db->update('bom_manage_player',$data);
				return $query;
		 }
		 
		 
		 public function select($id){
					   
					   $this->db->select('team_id');
					   $this->db->select('t_id');
					   $this->db->select('team_name');
					   $this->db->from('bom_tournament_teams');
					   $this->db->where('t_id',$id);
					   $teamselect=$this->db->get();
					   foreach ($teamselect->result() as $row){
						   
								$data[] = $row;
					   }
					   if($data>0){
						   
							 return $data;
					   }
					   else{
						   
							 return false;
					   }
		 }
		 
		 
		 public function t_id($mid){
		 			
					$this->db->select('*')->from('bom_tournament_teams')->where('t_id',$mid);
					$teamselect=$this->db->get();
					foreach ($teamselect->result() as $row){
						   
								$data[] = $row;
					 }
					 return $data;
		 }
}