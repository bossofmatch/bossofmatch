<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Manage_match_model extends CI_Model
{

		public function __construct(){
	
			   parent::__construct();
	
		}

		 public function t_full_name(){
			 
				 $this->db->select('t_id');
				 $this->db->select('t_full_name');
				 $this->db->from('bom_manage_tournament');
				 $this->db->where('is_active','0');
				 $full_name=$this->db->get();
				 if ($full_name->num_rows() > 0){
					 
					   foreach ($full_name->result() as $row){
						   
								$data[] = $row;
								
					   }
					   return $data;
				 }
				 return 0;
		 }
		 
		 
		 public function select($id){
			     
				 $this->db->select('team_id');
				 $this->db->select('t_id');
				 $this->db->select('team_name');
				 $this->db->from('bom_tournament_teams');
				 $this->db->where('t_id',$id);
				 $teamselect=$this->db->get();
				 foreach ($teamselect->result() as $row){
						  $data[] = $row;
						
				 }
				 if($data>0){
				       return $data;
				 }
				 else
				 {
				       return false;
				 }
			    
		 }
		 
		 
		 public function insertdata($data){ 
		 	
			     $query=$this->db->insert('bom_add_match',$data);
				 if($query>0){
					 
					   return $query; 
				 }
				 else{
					 
				       return false;
				 }
		 }
		 

		 
		 public function record_count(){
			 
				return $this->db->count_all("bom_add_match");
				
		 }
			
			
			public function fetch_match($limit, $start){
				
				$this->db->limit($limit, $start);
				$this->db->select('bom_add_match.*,bom_manage_tournament.t_full_name,bom_tournament_teams.team_name,a.team_name as team2');
				$this->db->from('bom_add_match');
				$this->db->join('bom_manage_tournament', 'bom_add_match.t_id= bom_manage_tournament.t_id', 'left');
				$this->db->join('bom_tournament_teams as a', 'bom_add_match.team1_id= a.team_id', 'left');
				$this->db->join('bom_tournament_teams', 'bom_add_match.team2_id= bom_tournament_teams.team_id', 'left');
				$this->db->order_by("match_start_date_time_minute", "desc");
				$query = $this->db->get();
				if ($query->num_rows() > 0){
					
					foreach ($query->result() as $row){
						$data[] = $row;
					}
					return $data;
				}

				return false;     			 
		 }

		 
		 public function teamselect(){
			     
				 $this->db->select('*');
				 $this->db->from('bom_tournament_teams');
				 $query=$this->db->get();
			    
				 foreach ($query->result() as $row){
					 
						  $data[] = $row;
	
				 }
				 return $data;
		 }
		 
		 
		 public function match_select($id){
			 
			     $this->db->select('*');
				 $this->db->from('bom_add_match');
				 $this->db->where('m_id',$id);
				 $query=$this->db->get();
				 if(count($query)>0){
					
				      return $query->row();
				 }
				 else{
					  return false;
				 }
		 }
		 
		 
		 public function editmanagematch($id,$data){
			 
			      $query=$this->db->where('m_id',$id)->update('bom_add_match',$data);
				  return $query;
		 }
		 
		 public function m_delete($id){
			 
			  $this->db->where('m_id', $id);
			  $query=$this->db->delete('bom_add_match'); 		
			  if($query)
			  return $query;
		
		 }
	
		public function m_active($id,$active){
				  
				  $data=array('is_active'=>$active);
				  $this->db->where('m_id',$id);
				  $query=$this->db->update('bom_add_match',$data);
				  if($query)
				  return $query;
		}
			
		
		public function record_count1($keys){
			    
				$this->db->from('bom_add_match');
				$this->db->select('bom_add_match.*,bmt.t_full_name,bom_tournament_teams.team_name,btt.team_name as team2');
				$this->db->join('bom_manage_tournament as bmt', 'bom_add_match.t_id= bmt.t_id', 'left');
				$this->db->join('bom_tournament_teams as btt', 'bom_add_match.team1_id= btt.team_id', 'left');
				$this->db->join('bom_tournament_teams', 'bom_add_match.team2_id= bom_tournament_teams.team_id', 'left');
				
				$this->db->like('t_full_name', $keys, 'both'); 
				$this->db->or_like('bom_tournament_teams.team_name', $keys, 'both'); 
				$this->db->or_like('btt.team_name', $keys, 'both'); 
				
				$this->db->order_by("m_id", "desc");
				
				$query = $this->db->get();
				return $count = $query->num_rows();
		}
		
		
		public function fetch_search($limit, $start,$keys){
		
				$this->db->limit($limit, $start);	
				$this->db->from('bom_add_match');
				$this->db->select('bom_add_match.*,bmt.t_full_name,bom_tournament_teams.team_name,btt.team_name as team2');
				$this->db->join('bom_manage_tournament as bmt', 'bom_add_match.t_id= bmt.t_id', 'left');
				$this->db->join('bom_tournament_teams as btt', 'bom_add_match.team1_id= btt.team_id', 'left');
				$this->db->join('bom_tournament_teams', 'bom_add_match.team2_id= bom_tournament_teams.team_id', 'left');
				
				$this->db->like('t_full_name', $keys, 'both'); 
				$this->db->or_like('bom_tournament_teams.team_name', $keys, 'both'); 
				$this->db->or_like('btt.team_name', $keys, 'both'); 
				
				$this->db->or_like('bmt.t_id', $keys, 'both');
				$this->db->or_like('btt.team_id', $keys, 'both');
								
				$this->db->order_by("m_id", "desc");
				
				$query = $this->db->get(); 
				if ($query->num_rows() > 0){
					foreach ($query->result() as $row){
						
							$data[] = $row;
					}
					
					return $data;
				}
				
				return false;
		   }
		   
		   
		   
		    public function exitdataornot($team1,$team2,$sdate,$edate){
			 		  
					  $this->db->select('*');
					  $this->db->from('bom_add_match');
					  $this->db->where('team1_id',$team1);
					  $this->db->where('team2_id',$team2);
					  $this->db->where('match_start_date_time_minute',$sdate);
					  $this->db->where('match_end_date_time_minute',$edate);
					  $query=$this->db->get();
					  return $query->num_rows();
		   }
		   
		   
		   public function checkidforend($id1,$id2){
			   		
			   		$total_point=0;
					$count=0;
					$batches_id=1;
					$count1=1;

					$this->db->select('*')->from('bom_add_match')->where('team1_id',$id2)->where('team2_id',$id1)->where('is_active',1);
					$query=$this->db->get();
					$user_id=$query->row();
					$this->db->select('*')->from('bom_manage_user')->where('m_id',$user_id->m_id)->where('team_selected',1);
                     $this->db->order_by("c_t_time");
					$query1=$this->db->get();
					if($query1!=""){

							foreach($query1->result() as $row){
										
									$data1=$row;
									$total_point=0;
									$this->db->select('*')->from('bom_user_team')->where('user_id',$data1->user_id);
                                   
									$query2=$this->db->get();
									if($query2!=""){
									
											foreach($query2->result() as $row){
											
													$data2=$row;
													$this->db->select('price')->from('bom_live_score_board')->where('player_id',$data2->player_id);
													$query2=$this->db->get();
													$price=$query2->row();
													$exits=$query2->num_rows();
													if($exits){
													
															$total_point=$total_point+$price->price;
													}
													else{
											
															$total_point=$total_point+0;
													}
													$delete = $this->db->delete('bom_user_team',array('player_id'=>$data2->player_id,'user_id'=>$data2->user_id));													
											}
											date_default_timezone_set("Asia/Calcutta");
											$end_date=gmdate("d-m-Y H:i",$user_id->match_start_date_time_minute);
											$enddate = strtotime($end_date);
											$data3=array('user_id'=>$data1->user_id,'m_id'=>$user_id->m_id,'total_earn'=>$total_point,'batches'=>'','match_end_date'=>$enddate);
											$insertdata=$this->db->insert('bom_result',$data3);
											$data4=array('total_account_amount'=>$data1->total_account_amount+$total_point,'current_win_ammount'=>$total_point,'t_id'=>'','team_name'=>'','m_id'=>'','team_selected'=>'0');
											$updatequery=$this->db->where('user_id',$data1->user_id)->update('bom_manage_user',$data4);
									}
							}												
					}
					$point='4500';
					$updatepoint = array('total_account_amount'=>'4500');
					$this->db->select('bom_manage_user.user_id,bom_manage_user.total_account_amount,bom_invite_mail.freiend_id');
					$this->db->from('bom_manage_user');
					$this->db->join('bom_invite_mail','bom_manage_user.user_id=bom_invite_mail.freiend_id','left');
					$this->db->group_by('bom_invite_mail.freiend_id');
					$selectresult=$this->db->get();
					foreach ($selectresult->result() as $value) {
							
							$this->db->where('total_account_amount <=',$point)->where('user_id',$value->freiend_id)->update('bom_manage_user',$updatepoint);
					
					}
					$pricaplayer=array('run'=>0,'wicket'=>0,'price'=>0);
					$this->db->update('bom_live_score_board', $pricaplayer);
					$isactive=array('is_active'=>2);
					$q1=$this->db->where('team1_id',$id2)->where('team2_id',$id1)->update('bom_add_match',$isactive);
					$this->db->select('*')->from('bom_result')->where('m_id',$user_id->m_id)->order_by('total_earn','desc');
					$query9=$this->db->get();
					foreach($query9->result() as $row1){
					
							$datad=$row1;
							if($batches_id<5){
										
								$bachesid=array('batches'=>$batches_id);
								$this->db->where('total_earn',$datad->total_earn);
								$this->db->where('m_id',$datad->m_id);
								$this->db->where('user_id',$datad->user_id);
								$this->db->update('bom_result',$bachesid);
								$batches_id++;
							}
							else{
										
								$bachesid=array('batches'=>$batches_id);
								$this->db->where('total_earn',$datad->total_earn);
								$this->db->where('m_id',$datad->m_id);
								$this->db->where('user_id',$datad->user_id);
								$this->db->update('bom_result',$bachesid);
							}
					}
					$delete=$this->db->empty_table('bom_last_score_board');
					
		   			$getdata=$this->db->get('bom_live_match_record');
					foreach ($getdata->result() as $row) {
					
     						 $this->db->insert('bom_last_score_board',$row);
					}
		   			$delete=$this->db->empty_table('bom_live_match_record');
					return $bachesid;
		   }
		   
		   public function deletelivescore(){
		   
		   			$delete=$this->db->empty_table('bom_last_score_board');
					
		   			$getdata=$this->db->get('bom_live_match_record');
					foreach ($getdata->result() as $row) {
					
     						 $this->db->insert('bom_last_score_board',$row);
					}
		   			$delete=$this->db->empty_table('bom_live_match_record');
					return $delete;
		   		
		   }
		   public function checkstatus($id){
		   
		   			$this->db->select('is_active')->from('bom_add_match')->where('m_id',$id);
					$isactive=$this->db->get();
					return $isactive->row();
		   }

		   public function selectcurrentmatch(){

				$this->db->select_max('match_end_date','lastmatch');
				$query = $this->db->get('bom_result');
				if($query){
						$query=$query->row();
						$this->db->select('bom_result.*,bmu.u_name,bmu.u_email');
						$this->db->from('bom_result');
						$this->db->join('bom_manage_user as bmu','bom_result.user_id=bmu.user_id','left');
						$this->db->where('bom_result.match_end_date',$query->lastmatch);
						$this->db->order_by('batches');
						$query1=$this->db->get();
						foreach ($query1->result() as $row){
			
									$data[] = $row;
						}
						//print_r($data);exit;
						return $data;
				}
		 }
}