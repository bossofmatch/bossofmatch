<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Config_changes_model extends CI_Model
{

    public function __construct()
	{

        parent::__construct();

    }

   
	public function select() 
    {

				  
		    $this->db->select('*');
			$this->db->from('bom_config');
			
           $query=$this->db->get();
		if($query)
		return $query->row(); 
	       
    }
	public function update($data) 
    {

		
        $query=$this->db->update('bom_config',$data);
		if($query)
		return $query;
	       
    }
	 	
}