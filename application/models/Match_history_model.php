<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
 
class Match_history_model extends CI_Model
{

    public function __construct()
	{

        parent::__construct();

    }

	public function tournamentname($user_id){
			
			
			$this->db->select('bom_manage_user.t_id,managetournament.t_short_name,managetournament.t_full_name');
			$this->db->from('bom_manage_user');
			$this->db->join('bom_manage_tournament as managetournament','bom_manage_user.t_id=managetournament.t_id','left');
			$this->db->where('user_id',$user_id);
			$query=$this->db->get();
			if($query!=NULL){
				
				foreach($query->result() as $row){
			
						$data=$row;
				}
				return $data;	
			}
			else false;
	}
		
   
	public function fech_record($user_id){
	$this->db->select('bom_result.*,bom_manage_user.u_name,bom_manage_user.user_image,bom_manage_tournament.t_full_name,bom_tournament_teams.team_name,a.team_name as team2');
				$this->db->from('bom_result');
				$this->db->join('bom_add_match', 'bom_result.m_id= bom_add_match.m_id', 'left');
				$this->db->join('bom_manage_user', 'bom_manage_user.user_id= bom_result.user_id', 'left');
				$this->db->join('bom_manage_tournament', 'bom_add_match.t_id= bom_manage_tournament.t_id', 'left');
				$this->db->join('bom_tournament_teams as a', 'bom_add_match.team1_id= a.team_id', 'left');
				$this->db->join('bom_tournament_teams', 'bom_add_match.team2_id= bom_tournament_teams.team_id', 'left');
				$this->db->where('bom_manage_user.user_id',$user_id); 
				$this->db->order_by("match_end_date", "desc");
				
				$query = $this->db->get();
				if ($query->num_rows() > 0){
					
					foreach ($query->result() as $row){
						$data[] = $row;
					}
					return $data;
				}

				return false; 
	       
    }
	
	
	
	
}