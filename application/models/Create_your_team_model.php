<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Create_your_team_model extends CI_Model
{

    public function __construct(){
        parent::__construct();
    }
	public function create_team($team_name,$email,$t_id,$tour_team_name){
		    $data=array('team_name'=>$team_name,'t_id'=>$t_id,'m_id'=>$tour_team_name);
     	$this->db->where('u_email',$email);    
        $this->db->update('bom_manage_user',$data);
		return true;	   
		   
    }
	public function select_user_profile($email,$t_id,$mid){
		    $this->db->select('bom_manage_user.*,bom_manage_tournament.t_full_name,bom_tournament_teams.team_name as team_name1,a.team_name as team_name2,bom_add_match.match_start_date_time_minute,bom_tournament_teams.team_id as team_id1,a.team_id as team_id2,');
			$this->db->from('bom_manage_user');
			$this->db->join('bom_manage_tournament','bom_manage_user.t_id = bom_manage_tournament.t_id','left');
			$this->db->join('bom_add_match','bom_add_match.t_id = bom_manage_tournament.t_id','left');
			$this->db->join('bom_tournament_teams as a','bom_add_match.team1_id= a.team_id','left');
			$this->db->join('bom_tournament_teams','bom_add_match.team2_id= bom_tournament_teams.team_id','left');
			$this->db->where('u_email',$email); 
			$this->db->where('bom_add_match.m_id',$mid); 
			$query = $this->db->get();
			return $query->row();
		   
    }
	
	public function fetch_data($mid){
		
		 $i=0;
		 $this->db->distinct();
		 $this->db->select('bom_add_match.*,manageplayer.*');
		  $this->db->from('bom_add_match');
		  $this->db->join('bom_manage_player as manageplayer', 'bom_add_match.team1_id= manageplayer.team_id or bom_add_match.team2_id= manageplayer.team_id', 'left');
		  $this->db->where('bom_add_match.m_id',$mid);
		 $this->db->order_by('manageplayer.player_name');
		 $query=$this->db->get();
		 if ($query->num_rows() > 0){
			 
			   foreach ($query->result() as $row){
				  
				
				$data[$i] = $row;
				//$da[$i]= $da[$i.$alpha];
						$i++;
			   }
		
			   return $data;
		 }
		 return 0;
	}
		/*public function select_match_time($m_id){
		$this->db->select('match_start_date_time_minute');
				$this->db->from('bom_add_match');
				 $this->db->where('m_id',$m_id);
				 		$query = $this->db->get();
			return $query->row();
					 
					 
		}*/
	
	public function select($id){
	$c_time=time();
	//print_r($c_time);exit;
			     $i=0;
				 $this->db->limit(2);
				$this->db->select('bom_add_match.*,bom_manage_tournament.t_full_name,bom_tournament_teams.team_name,a.team_name as team2');
				$this->db->from('bom_add_match');
				$this->db->join('bom_manage_tournament', 'bom_add_match.t_id= bom_manage_tournament.t_id', 'left');
				$this->db->join('bom_tournament_teams as a', 'bom_add_match.team1_id= a.team_id', 'left');
				$this->db->join('bom_tournament_teams', 'bom_add_match.team2_id= bom_tournament_teams.team_id', 'left');
				 $this->db->where('bom_add_match.t_id',$id);
				 $this->db->where('bom_add_match.is_active','1');
				 $this->db->order_by("match_start_date_time_minute");
				 $teamselect=$this->db->get();
				 foreach ($teamselect->result() as $row){
				 if($c_time<$row->match_start_date_time_minute)
				 {
						  $data[$i] = $row;
						  $i++;
						  }
				 }
				 //print_r($data);exit;
				 if(isset($data)){
				 //print_r($data);exit;
				       return $data;
				 }
				 else
				 {
				       return false;
				 }
			    
		 }
		 
		 public function create_user_team($user_id,$player_id,$data){
		 
		 		$this->db->select('*');
				$this->db->from('bom_user_team');
				$this->db->where('player_id',$player_id);
				$this->db->where('user_id',$user_id);
				$query=$this->db->get();
				$exits=$query->num_rows();
				if($exits){
						return 1;
				}
				else{
			     		$insert= $this->db->insert('bom_user_team',$data);
				        return $insert;
				}
 }
		 public function select_user_team($user_id){
	$this->db->distinct();
			      $this->db->select('*');
		$this->db->from('bom_user_team');
		$this->db->join('bom_manage_player', 'bom_manage_player.player_id= bom_user_team.player_id', 'left');
		
		
		$this->db->where('user_id',$user_id);
		$query=$this->db->get();
		if ($query->num_rows() > 0){
            foreach ($query->result() as $row){
                $data[] = $row;
            }
            return $data; 
				 }
			    
		 }
		 
		 
		 public function fetch_data2($mid,$user_id){
		
		 $i=0;
		 $this->db->select('bom_add_match.*,manageplayer.*,bom_user_team.user_id,bom_manage_user.team_name,bom_manage_user.current_purches_amt,bom_manage_user.total_account_amount');
		  $this->db->from('bom_add_match');
			$this->db->join('bom_manage_player as manageplayer', 'bom_add_match.team1_id= manageplayer.team_id or bom_add_match.team2_id= manageplayer.team_id', 'left');
		  $this->db->join('bom_user_team', 'bom_user_team.player_id= manageplayer.player_id', 'left');
$this->db->join('bom_manage_user', 'bom_user_team.user_id= bom_manage_user.user_id', 'left');
		
		  $this->db->where('bom_add_match.m_id',$mid);
		  $this->db->or_where('bom_user_team.user_id',$user_id);
		 
		 $this->db->order_by("manageplayer.player_name");
		 
		 $this->db->group_by('manageplayer.player_id');
		 
		 $query=$this->db->get();
		 
		 if ($query->num_rows() > 0){
			 
			   foreach ($query->result() as $row){
				  
				
				  $data[$i]= $row;
						$i++;
			   }
			   return $data;
		 }
		 return 0;
	}
	
	
	public function player_purchageornot($user_id,$player_id){
	
	
				$this->db->select('*');
				$this->db->from('bom_user_team');
				$this->db->where('user_id',$user_id);
				$this->db->where('player_id',$player_id);
				$query=$this->db->get();
				return $exits=$query->num_rows();
	}
	
	
	public function updatepurchageplayer($total,$email,$player_id,$user_id){
	
				$this->db->select('player_price');
				$this->db->from('bom_manage_player');
				$this->db->where('player_id',$player_id);
				$query=$this->db->get();
				$player_price=$query->row();
				
				$this->db->select('total_account_amount');
				$this->db->from('bom_manage_user');
				$this->db->where('u_email',$email);
				$query1=$this->db->get();
				$total_amt=$query1->row();
				
				$total_amt=$total_amt->total_account_amount-$player_price->player_price;
				
				$total=array('total_account_amount'=>$total_amt,'current_purches_amt'=>$total);
				$this->db->where('u_email',$email);
				$update=$this->db->update('bom_manage_user',$total);
				
				$this->db->select('total_account_amount');
				$this->db->from('bom_manage_user');
				$selectprice=$this->db->get();
				return $selectprice->row();
	
	}
	
	
	public function selectprice($player_id,$email){
	
			$this->db->select('player_price');
			$this->db->from('bom_manage_player');
			$this->db->where('player_id',$player_id);
			$query=$this->db->get();
			$player_price=$query->row();
			$player_price=$player_price->player_price;
			$this->db->select('current_purches_amt,total_account_amount');
			$this->db->from('bom_manage_user');
			$this->db->where('u_email',$email);
			$query1=$this->db->get();
			$total_amt=$query1->row();
			$increase_amt=$total_amt->total_account_amount;
			$decrease_purchse=$total_amt->current_purches_amt;
			$array=array('total_account_amount'=>$increase_amt+$player_price,'current_purches_amt'=>$decrease_purchse-$player_price);
			$this->db->where('u_email',$email);
			$update=$this->db->update('bom_manage_user',$array);
			
	}
	
	public function m_delete($user_id,$player_id){
	
		$this->db->where('user_id', $user_id);
		$this->db->where('player_id', $player_id);
		$query=$this->db->delete('bom_user_team'); 		
		if($query)
		return $query;
		
	}	 
		  public function fetch_data3($mid,$user_id,$search_item){
		
		 $i=0;
		 $this->db->distinct();
		 $this->db->select('bom_add_match.*,manageplayer.*,bom_user_team.user_id,bom_manage_user.team_name,bom_manage_user.total_account_amount');
		  $this->db->from('bom_add_match');
$this->db->join('bom_manage_player as manageplayer', 'bom_add_match.team1_id= manageplayer.team_id or bom_add_match.team2_id= manageplayer.team_id', 'left');
		  $this->db->join('bom_user_team', 'bom_user_team.player_id= manageplayer.player_id', 'left');
$this->db->join('bom_manage_user', 'bom_user_team.user_id= bom_manage_user.user_id', 'left');
		 $this->db->like('manageplayer.player_name', $search_item, 'after');
		  $this->db->where('bom_add_match.m_id',$mid);
		  $this->db->or_where('bom_user_team.user_id',$user_id);
		 $this->db->order_by("manageplayer.player_name");
		 $query=$this->db->get();
		 if ($query->num_rows() > 0){
			 
			   foreach ($query->result() as $row){

				$data[$i] = $row;
				//$da[$i]= $da[$i.$alpha];
						$i++;
			   }
		
			   return $data;
		 }
		 return 0;
	}
		  public function insert_sected_team($email,$data){
		  $this->db->where('u_email',$email);    
        $this->db->update('bom_manage_user',$data);
		return true;	   
		  }
		  
		  public function clear($user_id){
		   
		    $this->db->select('manageuser.*,userteam.*,sum(manageplayer.player_price) as price');

		    $this->db->from('bom_manage_user as manageuser');

		    $this->db->join('bom_user_team as userteam','manageuser.user_id=userteam.user_id','left');

		    $this->db->join('bom_manage_player as manageplayer','userteam.player_id=manageplayer.player_id','left');
		    $this->db->where('manageuser.user_id', $user_id);
		    $query=$this->db->get();
		    $query1=$query->row();
		    if($query1->price!=NULL){

		    	$update = array('total_account_amount' =>$query1->total_account_amount+$query1->price,'current_win_ammount'=>'0');
		    	$this->db->where('user_id',$user_id);
		    	$k=$this->db->update('bom_manage_user',$update);
		    }
			$this->db->where('user_id', $user_id);
			$this->db->delete('bom_user_team'); 
			return true;
		   
    }
	public function datetimeselect($email){
			
			$this->db->select('created_date')->from('bom_manage_user')->where('u_email',$email);
			$datetime1=$this->db->get();
			return $datetime1->row();
	}

//t_id 	team1_id 	team2_id

	public function select_user_profile1($email){
		    
		    $this->db->select('manageuser.*,addmatch.*,tornamentteams.team_name as team1,tornamentteams1.team_name as team2');
		    $this->db->from('bom_manage_user as manageuser');
		    $this->db->join('bom_add_match as addmatch','addmatch.m_id=manageuser.m_id','left');
		    $this->db->join('bom_tournament_teams as tornamentteams','tornamentteams.team_id=addmatch.team1_id','left');
		    $this->db->join('bom_tournament_teams as tornamentteams1','tornamentteams1.team_id=addmatch.team2_id','left');
		    
		   /* $this->db->select('bom_manage_user.*,managetou.t_full_name,b.team_name as team_name1,a.team_name as team_name2,addmatch.match_start_date_time_minute,b.team_id as team_id1,a.team_id as team_id2,');
			$this->db->from('bom_manage_user');
			//$this->db->join();
			$this->db->join('bom_manage_tournament as managetou','bom_manage_user.t_id = managetou.t_id','left');
			$this->db->join('bom_add_match as addmatch','addmatch.t_id = managetou.t_id','left');
			$this->db->join('bom_tournament_teams as a','addmatch.team1_id= a.team_id','left');
			$this->db->join('bom_tournament_teams as b','addmatch.team2_id= b.team_id','left');*/
			$this->db->where('manageuser.u_email',$email);  
			$query = $this->db->get();
			return $query->row();
		   
    }

 public function teamselect($email){

    	$this->db->select('team_selected');
    	$this->db->from('bom_manage_user');
    	$this->db->where('u_email',$email);
    	$query=$this->db->get();
    	return $query->row();
		}
		
		public function retrivedate($user_id){
		
				$this->db->select('bom_manage_user.m_id,bam.match_start_date_time_minute');
				$this->db->from('bom_manage_user');
				$this->db->join('bom_add_match as bam','bom_manage_user.m_id=bam.m_id','left');
				$this->db->where('user_id',$user_id);
				$query=$this->db->get();
				$query=$query->row();
				$matchdate=$query->match_start_date_time_minute;
				$matchdate=$matchdate+300;
				$time=time();
				if($matchdate<$time){
					//print_r($time);	echo "rrrrrr";exit;
					return $matchdate;
				}
				else{
				  //print_r($time);	echo "rrrrrr";exit;
					return 0;		
				}
		}
		
		public function retrivedate1($user_id){
			
				$this->db->select('bom_manage_user.m_id,bam.match_start_date_time_minute');
				$this->db->from('bom_manage_user');
				$this->db->join('bom_add_match as bam','bom_manage_user.m_id=bam.m_id','left');
				$this->db->where('user_id',$user_id);
				$query=$this->db->get();
				$query=$query->row();
				$matchdate=$query->match_start_date_time_minute;
				$time=time();
				if($matchdate<$time){
					return $matchdate;
					}
					else{
				  //print_r($time);	echo "rrrrrr";exit;
					return 0;		
				}
			}
}