<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Live_score_board_model extends CI_Model
{

		public function __construct(){
	
			parent::__construct();
	
		}
	
	   
		public function t_full_name(){
			
			  
	
			     $i=0;
				$this->db->select('bom_add_match.*,bom_manage_tournament.t_full_name,bom_tournament_teams.team_name,a.team_name as team2');
				$this->db->from('bom_add_match');
				$this->db->join('bom_manage_tournament', 'bom_add_match.t_id= bom_manage_tournament.t_id', 'left');
				$this->db->join('bom_tournament_teams as a', 'bom_add_match.team1_id= a.team_id', 'left');
				$this->db->join('bom_tournament_teams', 'bom_add_match.team2_id= bom_tournament_teams.team_id', 'left');
				 //$this->db->where('bom_add_match.is_active','1');
				 $query=$this->db->get();
				  if ($query->num_rows() > 0){
					 
					   foreach ($query->result() as $row){
						   
								$data[] = $row;
					   }
					   return $data;
				 }
				 return 0;
		}
	 
		
		public function fetch_data($id){
			
				 $this->db->select('*')->from('bom_manage_player')->where('team_id',$id);
				 $query=$this->db->get();
				 if ($query->num_rows() > 0){
					 
					   foreach ($query->result() as $row){
						   
								$data[] = $row;
					   }
					   return $data;
				 }
				 return 0;
		}
		
		
		public function checkexitsornot($array){
			
			
				$this->db->select('bmp.*,blsb.*');
				$this->db->from('bom_manage_player as bmp');
				$this->db->join('bom_live_score_board as blsb', 'blsb.player_id = bmp.player_id', 'left');
				$this->db->where_in('bmp.player_id',$array);
				$query = $this->db->get();
				if ($query->num_rows() > 0){

						  foreach ($query->result() as $row){

							$data[] = $row;
						  }
				}
				foreach($data as $key=>$value){
					
					$this->db->select('bom_manage_player.player_id');
					$this->db->where('unike_payer_name',$value->unike_payer_name);
					$this->db->from('bom_manage_player');
					$query1=$this->db->get();
					$player_id=$query1->row();
					$this->db->select('bom_live_score_board.player_id');
					$this->db->from('bom_live_score_board');
					$this->db->where('bom_live_score_board.player_id',$player_id->player_id);
					$query2=$this->db->get();
					$count = $query2->num_rows();
					if($count==0){
						
						$player_id1=$player_id->player_id;
						$insert_data=array('player_id'=>$player_id->player_id,'run'=>'0','wicket'=>'0','price'=>'0');
						$this->db->insert('bom_live_score_board',$insert_data);
					}
				}
				return 1;
		}
		
		
		public function update_scroe($array){
				
				$this->db->select('bmp.player_name,blsb.*');
				$this->db->from('bom_live_score_board as blsb');
				$this->db->join('bom_manage_player as bmp', 'bmp.player_id = blsb.player_id', 'left');
				$this->db->where_in('blsb.player_id',$array);
				$query = $this->db->get();
				if ($query->num_rows() > 0){

						  foreach ($query->result() as $row){
  
							$data[] = $row;
						  }
				}
				return $data;
				
		}
		
		public function select_rupess(){
			
				$this->db->select('rupess_for_wicket,rupess_for_run')->from('bom_config');
				$pointforrunwicket=$this->db->get();
				return $pointforrunwicket->row();
		}
		
		
		public function exitsid($id){
			
				$this->db->select('player_id');
				$this->db->where('player_id',$id);
				$this->db->from('bom_live_score_board');
				$query = $this->db->get();
				return $count = $query->num_rows();
		}
		
		public function replace($id,$data){
			
				$this->db->where('player_id',$id);
			    $replace=$this->db->update('bom_live_score_board',$data);
			    if($replace){
					
					$this->db->select('price');
					$this->db->where('player_id',$id);
					$this->db->from('bom_live_score_board');
					$query = $this->db->get();
					foreach ($query->result() as $row){

						$data= $row->price;
					}
					return $data;
				}
		}
		
		
		public function insertrunwicket($id,$data){
			
				$update=$this->db->insert('bom_live_score_board',$data);
				if($update){
					
					$this->db->select('price');
					$this->db->where('player_id',$id);
					$this->db->from('bom_live_score_board');
					$query = $this->db->get();
					foreach ($query->result() as $row){

						$data= $row->price;
					}
					return $data;
				}
		}		
		public function select_user_profile($mid){
		    $this->db->select('bom_tournament_teams.team_id as team_id1,a.team_id as team_id2,');
			$this->db->from('bom_manage_user');
			$this->db->join('bom_manage_tournament','bom_manage_user.t_id = bom_manage_tournament.t_id','left');
			$this->db->join('bom_add_match','bom_add_match.t_id = bom_manage_tournament.t_id','left');
			$this->db->join('bom_tournament_teams as a','bom_add_match.team1_id= a.team_id','left');
			$this->db->join('bom_tournament_teams','bom_add_match.team2_id= bom_tournament_teams.team_id','left');
			$this->db->where('bom_add_match.m_id',$mid); 
			$query = $this->db->get();
			return $query->row();
		   
    }
}