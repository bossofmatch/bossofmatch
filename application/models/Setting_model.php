<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
 
class Setting_model extends CI_Model
{

    public function __construct()
	{

        parent::__construct();

    }

   public function select_match($email){
   
   			$this->db->select('bom_user.m_id,bom_user.team_name as team1,addmatch.t_id as tid,addmatch.team1_id,addmatch.team2_id,tournamentteams.team_name as team3,tournamentteams1.team_name as team2');
			
			$this->db->from('bom_manage_user as bom_user');
			
			$this->db->where('u_email',$email);
			
			$this->db->join('bom_add_match as addmatch','addmatch.m_id=bom_user.m_id','left');
			
			$this->db->join('bom_tournament_teams as tournamentteams','addmatch.team1_id=tournamentteams.team_id','left');
			
			$this->db->join('bom_tournament_teams as tournamentteams1','addmatch.team2_id=tournamentteams1.team_id','left');
			$query=$this->db->get();
			if($query!=NULL){

					return $query->row();
					
			}else{
			
					echo '0';exit;
			}
		
   }
   
   
	public function select_user_profile($email){
		    $this->db->select('*');
			$this->db->from('bom_manage_user');
			$this->db->where('u_email',$email);    
			$query=$this->db->get();
		if($query)
		return $query->row(); 
    }
	
	
	
	
	public function update_profile($email,$data){
     	$this->db->where('u_email',$email);    
        $this->db->update('bom_manage_user',$data);
		return true;
    }

    public function selectimage($email){

    	$this->db->select('user_image')->from('bom_manage_user')->where('u_email',$email);
    	$query=$this->db->get();
		if($query)
		return $query->row(); 
    }
	
	public function get_time(){
     	 $this->db->select('time_user_entry');
			$this->db->from('bom_config');
		   
			$query=$this->db->get();
		if($query)
		return $query->row(); 
    }
}