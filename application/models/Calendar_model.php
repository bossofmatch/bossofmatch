<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
 
class Calendar_model extends CI_Model
{

    public function __construct()
	{

        parent::__construct();

    }

   
	public function fetch_match(){
		  $i=0;
				$this->db->select('bom_add_match.*,bom_manage_tournament.t_full_name,bom_tournament_teams.team_name,a.team_name as team2');
				$this->db->from('bom_add_match');
				$this->db->join('bom_manage_tournament', 'bom_add_match.t_id= bom_manage_tournament.t_id', 'left');
				$this->db->join('bom_tournament_teams as a', 'bom_add_match.team1_id= a.team_id', 'left');
				$this->db->join('bom_tournament_teams', 'bom_add_match.team2_id= bom_tournament_teams.team_id', 'left');
				
				$teamselect=$this->db->get();
				foreach ($teamselect->result() as $row){
						  $data[$i] = $row;
						  $i++;
				}
				if(isset($data)){
				      return $data;
				}
				else
				{
				      return false;
				}
		 
		}

}