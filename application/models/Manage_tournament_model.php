<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Manage_tournament_model extends CI_Model
{

		  public function __construct(){
	  
			  parent::__construct();
	  
		  }
	  
		 
		  public function gamename(){
				   $i=0;
				   $this->db->select('*');
				   $this->db->from('bom_game_name');
				   $query=$this->db->get();
				   foreach ($query->result() as $row){
						  $data[$i] = $row;
						  $i++;
				   }
				   return $data;
		  }
		  
			  
		  public function insertdata($data){
				  $insert= $this->db->insert('bom_manage_tournament',$data);
				  if($insert){
					   return $insert;  
				  }
		  }
		  
		  
		  public function retrive_data($data){
			      $this->db->select('t_id')->from('bom_manage_tournament')->where($data);
				  $retrive =$this->db->get();
				   foreach ($retrive->result() as $row){
						  $data = $row;
				   }
				   return $data;
		  }
	
	
	      public function insert_tou_team($id,$data){
			      $insertdata=array('t_id'=>$id,'team_name'=>$data);
			      $insert=$this->db->insert('bom_tournament_teams',$insertdata);
				  return $insert; 
		  }
		  
		  public function record_count(){
				return $this->db->count_all("bom_manage_tournament");
			}
			
			
			public function fetch_member($limit, $start){
				$this->db->limit($limit, $start);
				$this->db->select('*');
				$this->db->from('bom_manage_tournament');
				$this->db->join('bom_game_name', 'bom_manage_tournament.game_id = bom_game_name.g_id', 'left');
				$this->db->order_by("t_id", "desc"); 
				$query = $this->db->get();
				if ($query->num_rows() > 0){
					foreach ($query->result() as $row){
						$data[] = $row;
					}
					return $data;
				}
				return false;
		   }
		   
		   
		   public function fetch_search($limit, $start,$keys){
		
				$this->db->limit($limit, $start);	
				$this->db->select('*');
				$this->db->from('bom_manage_tournament');
				$this->db->join('bom_game_name', 'bom_manage_tournament.game_id = bom_game_name.g_id', 'left');
				$this->db->like('t_short_name', $keys, 'both'); 
				$this->db->or_like('bom_game_name.g_name', $keys, 'both');
				$this->db->order_by("t_id ", "desc"); 
				$query = $this->db->get(); 
				if ($query->num_rows() > 0){
					foreach ($query->result() as $row){
						$data[] = $row;
					}
					
					return $data;
				}
				
				return false;
		   }
		   
		   
		   public function record_count1($keys){
			   
				$this->db->select('*');
				$this->db->from('bom_manage_tournament');
				$this->db->join('bom_game_name', 'bom_manage_tournament.game_id = bom_game_name.g_id', 'left');
				$this->db->like('t_short_name', $keys, 'both'); 
			
				$this->db->order_by("t_id", "desc"); 
				$query = $this->db->get();
				return $count = $query->num_rows();
				
			}
		   
		    public function m_active($id,$active){
				
				$data=array('is_active'=>$active);
				$this->db->where('t_id',$id);
				$query=$this->db->update('bom_manage_tournament',$data);
				
				if($query)
				return $query;
			}
			
			public function m_delete($id){


				$this->db->where('t_id', $id);
				$query=$this->db->delete('bom_manage_tournament');
				if($query){

						$this->db->where('t_id',$id);
						$query1=$this->db->delete('bom_tournament_teams');
						if($query1){

								$this->db->select('m_id');
								$this->db->from('bom_add_match');
								$this->db->where('t_id',$id);
								$m_id=$this->db->get();
								foreach ($m_id->result() as $row) {

									$this->db->where('m_id',$row->m_id);
									$delete=$this->db->delete('bom_result');
								}
								

								$this->db->where('t_id',$id);
								$query2=$this->db->delete('bom_add_match');
								if($query2){
									
									$this->db->where('t_id',$id);
									$query3=$this->db->delete('bom_manage_player');
									if($query3){
										
											return $query3;
									}
									else{
										
											return $query;
									}
								}
								else{
									
										$this->db->where('t_id',$id);
										$query3=$this->db->delete('bom_manage_player');
										if($query3){
										
												return $query3;
										}	
										else{
										
												return $query;
										}
										
								}
						}
							
				}
			}
			
			
			
		   public function tournament_edit_open($id){
			   
				   $this->db->select('*');
				   $this->db->from('bom_manage_tournament');
				   $this->db->where('t_id',$id);
				   $query= $this->db->get();
				   return $query->row(); 
			}
			
			public function selectdata(){
				
				   $this->db->select('*');
				   $this->db->from('bom_game_name');
				   $select= $this->db->get();
				   foreach ($select->result() as $row){
						$data[] = $row;
				   }
				   return $data;
			}
			
			
			public function update_tournament($id,$data){
				   $this->db->where('t_id', $id);
                   $update= $this->db->update('bom_manage_tournament',$data);
				   return $update;
			}
			
			
			public function teamselect($id){
				   $this->db->select('*');
				   $this->db->from('bom_tournament_teams');
				   $this->db->where('t_id',$id);
				   $query=$this->db->get();
				   foreach ($query->result() as $row){
						$data[] = $row;
				   }
				   return $data;
			}
			
			public function updateteam($id,$team_name){
				
				  $data=array('team_name'=>$team_name);
				  $this->db->where('team_id',$id);
				  $query=$this->db->update('bom_tournament_teams',$data);
				  if($query)
				  return $query;
			}
			
			public function new_team_insert($name){
				  $insert_team=$this->db->insert('bom_tournament_teams',$name);
				  return $insert_team;
			}
}