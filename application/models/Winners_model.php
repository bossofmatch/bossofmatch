<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Winners_model extends CI_Model
{

    public function __construct()
	{

        parent::__construct();

    }

   
	public function fech_record() 
    {

			$this->db->select('bom_result.*,bom_manage_user.u_name,bom_manage_user.user_image,bom_manage_user.mobile_no,bom_manage_user.service_provider,bom_manage_user.u_email,bom_manage_tournament.t_full_name,bom_tournament_teams.team_name,a.team_name as team2,bom_add_match.match_start_date_time_minute'); 
			
				$this->db->from('bom_result');
				$this->db->join('bom_add_match', 'bom_result.m_id= bom_add_match.m_id', 'left');
				$this->db->join('bom_manage_user', 'bom_manage_user.user_id= bom_result.user_id', 'left');
				$this->db->join('bom_manage_tournament', 'bom_add_match.t_id= bom_manage_tournament.t_id', 'left');
				$this->db->join('bom_tournament_teams as a', 'bom_add_match.team1_id= a.team_id', 'left');
				$this->db->join('bom_tournament_teams', 'bom_add_match.team2_id= bom_tournament_teams.team_id', 'left');
				$this->db->where('batches','1');
				$this->db->order_by("match_end_date", "desc");
				$this->db->order_by("batches");
				$query = $this->db->get();
				if ($query->num_rows() > 0){
				
					foreach ($query->result() as $row){
						$data1[] = $row;
					}
					//print_r($data1);exit;
					return $data1;
			}					
			

	
	       
    }
	
	public function last_match_winner() 
    {

	$this->db->select_max('match_end_date','m_id');

            $this->db->limit(1);
			
            $query = $this->db->get('bom_result');
            if($query)
		$query=$query->row();
			$this->db->select('bom_result.*,bom_manage_user.user_image,bom_manage_user.u_name,bom_manage_user.user_image,bom_manage_tournament.t_full_name,bom_tournament_teams.team_name,a.team_name as team2,bom_add_match.match_start_date_time_minute')
				->limit(1)
				->from('bom_result')
				->join('bom_add_match', 'bom_result.m_id= bom_add_match.m_id', 'left')
				->join('bom_manage_user', 'bom_manage_user.user_id= bom_result.user_id', 'left')
				->join('bom_manage_tournament', 'bom_add_match.t_id= bom_manage_tournament.t_id', 'left')
				->join('bom_tournament_teams as a', 'bom_add_match.team1_id= a.team_id', 'left')
				->join('bom_tournament_teams', 'bom_add_match.team2_id= bom_tournament_teams.team_id', 'left')
				->where('bom_result.match_end_date',$query->m_id)
				->where('batches','1')
				->order_by("total_earn", "desc");
			$query1 = $this->db->get();
            if($query1){
				return $query1->row(); 
			}
		else{
			return ("No data Found");
		}
	       
    }
	public function last_match_winner1() 
    {
	//$this->db->select('m_id');
	$this->db->select_max('match_end_date','m_id');
 //$this->db->where(max('match_end_date'));
            $this->db->limit(1);
			
            $query = $this->db->get('bom_result');
            if($query)
		$query=$query->row();
		
			$this->db->select('bom_result.*,bom_manage_user.user_image,bom_manage_user.u_name,bom_manage_user.u_email,bom_manage_user.user_image,bom_manage_tournament.t_full_name,bom_tournament_teams.team_name,a.team_name as team2,bom_add_match.match_start_date_time_minute'); 
			$this->db->limit(4);
				$this->db->from('bom_result');
				$this->db->join('bom_add_match', 'bom_result.m_id= bom_add_match.m_id', 'left');
				$this->db->join('bom_manage_user', 'bom_manage_user.user_id= bom_result.user_id', 'left');
				$this->db->join('bom_manage_tournament', 'bom_add_match.t_id= bom_manage_tournament.t_id', 'left');
				$this->db->join('bom_tournament_teams as a', 'bom_add_match.team1_id= a.team_id', 'left');
				$this->db->join('bom_tournament_teams', 'bom_add_match.team2_id= bom_tournament_teams.team_id', 'left');
                	$this->db->where('batches','1')->or_where('batches','2')->or_where('batches','3')->or_where('batches','4');
					$this->db->where('bom_result.match_end_date',$query->m_id);
				$this->db->order_by("total_earn", "desc");
			$query = $this->db->get();
			if ($query->num_rows() > 0){
				
					foreach ($query->result() as $row){
						$data1[] = $row;
					}
					//print_r($data1);exit;
					return $data1;
			}
		
	       
    }

    public function last_match_winner2() 
    {
	//$this->db->select('m_id');
	$this->db->select_max('match_end_date','m_id');
 //$this->db->where(max('match_end_date'));
            $this->db->limit(1);
			
            $query = $this->db->get('bom_result');
            if($query)
		$query=$query->row();
		
			$this->db->select('bom_result.*,bom_manage_user.user_image,bom_manage_user.u_name,bom_manage_user.u_email,bom_manage_user.user_image,bom_manage_tournament.t_full_name,bom_tournament_teams.team_name,a.team_name as team2,bom_add_match.match_start_date_time_minute'); 
		//	$this->db->limit(4);
				$this->db->from('bom_result');
				$this->db->join('bom_add_match', 'bom_result.m_id= bom_add_match.m_id', 'left');
				$this->db->join('bom_manage_user', 'bom_manage_user.user_id= bom_result.user_id', 'left');
				$this->db->join('bom_manage_tournament', 'bom_add_match.t_id= bom_manage_tournament.t_id', 'left');
				$this->db->join('bom_tournament_teams as a', 'bom_add_match.team1_id= a.team_id', 'left');
				$this->db->join('bom_tournament_teams', 'bom_add_match.team2_id= bom_tournament_teams.team_id', 'left');
					$this->db->where('bom_result.match_end_date',$query->m_id);
				$this->db->order_by("total_earn", "desc");
			$query = $this->db->get();
			if ($query->num_rows() > 0){
				
					foreach ($query->result() as $row){
						$data1[] = $row;
					}
					//print_r($data1);exit;
					return $data1;
			}
	       
    }


    public function count1(){

    	return $table_row_count = $this->db->count_all('bom_result');
	}

	public function record_count(){

        return $this->db->count_all("bom_result");
  
	}

	public function allwinner($limit, $start){

			 $this->db->limit($limit, $start);
			$this->db->select('bom_result.*,bom_manage_user.u_name,bom_manage_user.mobile_no,bom_manage_user.service_provider,bom_manage_user.u_email,bom_manage_tournament.t_full_name,bom_tournament_teams.team_name,a.team_name as team2,bom_add_match.match_start_date_time_minute'); 
			
				$this->db->from('bom_result');
				$this->db->join('bom_add_match', 'bom_result.m_id= bom_add_match.m_id', 'left');
				$this->db->join('bom_manage_user', 'bom_manage_user.user_id= bom_result.user_id', 'left');
				$this->db->join('bom_manage_tournament', 'bom_add_match.t_id= bom_manage_tournament.t_id', 'left');
				$this->db->join('bom_tournament_teams as a', 'bom_add_match.team1_id= a.team_id', 'left');
				$this->db->join('bom_tournament_teams', 'bom_add_match.team2_id= bom_tournament_teams.team_id', 'left');
				$this->db->where('batches','1')->or_where('batches','2')->or_where('batches','3')->or_where('batches','4');
				$this->db->order_by("match_end_date", "desc");
				$this->db->order_by("batches");
				$query = $this->db->get();
				if ($query->num_rows() > 0){
				
					foreach ($query->result() as $row){
						$data1[] = $row;
					}
					//print_r($data1);exit;
					return $data1;
			}
		}
		
		
		
		public function last_match_winner_mail() 
    {
	//$this->db->select('m_id');
	$this->db->limit(1);
	$this->db->select_max('match_end_date','m_id');
    $query = $this->db->get('bom_result');
            if($query)
		$query=$query->row();
		
			$this->db->select('bom_result.*,bom_manage_user.user_image,bom_manage_user.u_name,bom_manage_user.u_email,bom_manage_user.user_image,bom_manage_tournament.t_full_name,bom_tournament_teams.team_name,a.team_name as team2,bom_add_match.match_start_date_time_minute'); 
			
				$this->db->from('bom_result');
				$this->db->join('bom_add_match', 'bom_result.m_id= bom_add_match.m_id', 'left');
				$this->db->join('bom_manage_user', 'bom_manage_user.user_id= bom_result.user_id', 'left');
				$this->db->join('bom_manage_tournament', 'bom_add_match.t_id= bom_manage_tournament.t_id', 'left');
				$this->db->join('bom_tournament_teams as a', 'bom_add_match.team1_id= a.team_id', 'left');
				$this->db->join('bom_tournament_teams', 'bom_add_match.team2_id= bom_tournament_teams.team_id', 'left');
					$this->db->where('bom_result.match_end_date',$query->m_id);
				$this->db->order_by("total_earn", "desc");
			$query = $this->db->get();
			if ($query->num_rows() > 0){
				
					foreach ($query->result() as $row){
						$data1[] = $row;
					}
					return $data1;
			}
		
	       
    }
	
public function selemail() 
    {
		$this->db->select('u_email,u_name');
			$this->db->from('bom_manage_user');
			$query = $this->db->get();
			if ($query->num_rows() > 0){
				
					foreach ($query->result() as $row){
						$data1[] = $row;
					}
					return $data1;
			}
		
	}
public function selmatch() 
    {
			     $i=0;
				 $this->db->limit(1);
				$this->db->select('bom_add_match.*,bom_manage_tournament.t_full_name,bom_tournament_teams.team_name,a.team_name as team2');
				$this->db->from('bom_add_match');
				$this->db->join('bom_manage_tournament', 'bom_add_match.t_id= bom_manage_tournament.t_id', 'left');
				$this->db->join('bom_tournament_teams as a', 'bom_add_match.team1_id= a.team_id', 'left');
				$this->db->join('bom_tournament_teams', 'bom_add_match.team2_id= bom_tournament_teams.team_id', 'left');
				
				 $this->db->where('bom_add_match.is_active','1');
				 $this->db->order_by("match_start_date_time_minute");
				 $query=$this->db->get();
				  if($query)
		$query=$query->row();
		return $query;
		
	}
	 	
}