<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
 
class Livescore_model extends CI_Model
{

    public function __construct()
	{

        parent::__construct();

    }

   
	public function update($data){
		 
       
        $query=$this->db->insert('bom_live_match_record',$data);
		if($query)
		return $query;
		 
		  }
		  
		  
		public function select(){
		
		$this->db->select('*');
		$this->db->from('bom_live_match_record');
      	$query = $this->db->get(); 
        if ($query->num_rows() > 0){
            foreach ($query->result() as $row){
                $data[] = $row;
            }
			
            return $data;
       		 }
			
        	return false;
		 
		  }
		  
		  public function select2(){
		
		$this->db->select('*');
		$this->db->from('bom_last_score_board');
      	$query = $this->db->get(); 
        if ($query->num_rows() > 0){
            foreach ($query->result() as $row){
                $data[] = $row;
            }
			
            return $data;
       		 }
			
        	return false;
		 
		  }
		  
		  public function select_update($player_key){
		
		$this->db->select('*');
		$this->db->from('bom_live_match_record');
		 $this->db->where('player_key',$player_key);
      	$query = $this->db->get(); 
        if ($query->num_rows() > 0){
            foreach ($query->result() as $row){
                $data[] = $row;
           	 }
			
            return $data;
       		 }
		
        return false;
		 
		  }
		  
		  
		  public function update_runs($runs,$player_name,$rprice,$wprice){
		  $this->db->select('bom_manage_player.player_id as p1,bom_live_score_board.run,bom_live_score_board.wicket,bom_live_score_board.player_id as p2');
		 $this->db->from('bom_manage_player');
				
				$this->db->join('bom_live_score_board', 'bom_manage_player.player_id = bom_live_score_board.player_id', 'left');
		$this->db->where('bom_manage_player.api_unike_name',$player_name);		
       	$query = $this->db->get(); 
        if ($query->num_rows() > 0){
            foreach ($query->result() as $row){
                $data[] = $row;
            }
		//print_r($data);exit;
		if($data[0]->p2=="")
		{
		$price1=$rprice*$data[0]->run;
		$price2=$wprice*$data[0]->wicket;
		$price=$price1+$price2;
		  $dataa=array('player_id'=>$data[0]->p1,'run'=>$runs,'price'=>$price);
		$query=$this->db->insert('bom_live_score_board',$dataa);
		if($query)
		return $query;
		}
		else
		{
		//print_r($data[0]->p2);exit;
		$price1=$rprice*$data[0]->run;
		$price2=$wprice*$data[0]->wicket;
		$price=$price1+$price2;
      $dataa=array('run'=>$runs,'price'=>$price);
        $this->db->where('player_id',$data[0]->p1);
           $query=$this->db->update('bom_live_score_board',$dataa);
		
		if($query)
		return $query;
		}}
		  }
		  
		  
		  public function update_wickets($wickets,$player_name,$rprice,$wprice){
		  $this->db->select('bom_manage_player.player_id as p1,bom_live_score_board.run,bom_live_score_board.wicket,bom_live_score_board.player_id as p2');
		 $this->db->from('bom_manage_player');
				
				$this->db->join('bom_live_score_board', 'bom_manage_player.player_id = bom_live_score_board.player_id', 'left');
		$this->db->where('bom_manage_player.api_unike_name',$player_name);		
       	$query = $this->db->get(); 
        if ($query->num_rows() > 0){
            foreach ($query->result() as $row){
                $data[] = $row;
            }
			//print_r($data[0]->p1);exit;
		if($data[0]->p2=="")
		{
		$price1=$rprice*$data[0]->run;
		$price2=$wprice*$data[0]->wicket;
		$price=$price1+$price2;
		  $dataa=array('player_id'=>$data[0]->p1,'wicket'=>$wickets,'price'=>$price);
		$query=$this->db->insert('bom_live_score_board',$dataa);
		if($query)
		return $query;
		}
		else
		{
		$price1=$rprice*$data[0]->run;
		$price2=$wprice*$data[0]->wicket;
		$price=$price1+$price2;
      $dataa=array('wicket'=>$wickets,'price'=>$price);
        $this->db->where('player_id',$data[0]->p2);
           $query=$this->db->update('bom_live_score_board',$dataa);
		
		if($query)
		return $query;
		}}
		  }
		  
public function config(){
		   $this->db->select('*');
		   
		 $this->db->from('bom_config');
				$query = $this->db->get(); 
        if ($query->num_rows() > 0){
            foreach ($query->result() as $row){
                $data[] = $row;
            }	
			return $data;
			}
		
		
		}	
			
			public function insert($data){
			
			//print_r($data);exit;
			$dataa=$data[0]->api_url+1;
		       $dataa=array('api_url'=>$dataa);
        $query1=$this->db->update('bom_config',$dataa);
		if($query1)
		return $query1;
		 
		  }
		  
		  
		  public function select_user($date){
		// print_r($date);exit;
		 
		 			      $this->db->select('m_id')->from('bom_add_match')->where('match_start_date_time_minute',$date);
				 
				$query=$this->db->get();
				if($query)
				$data=$query->row(); 
	//print_r($data);exit;
				if($data!="")
				{if(isset($data->m_id)){

         			$this->db->select('*')->from('bom_manage_user')->where('m_id',$data->m_id)->where('team_selected','1')->where('is_active','1');
					$this->db->order_by('c_t_time');

					$queryy=$this->db->get();
					  if ($queryy->num_rows() > 0){
            				foreach ($queryy->result() as $row){
                				$dataa[] = $row;
            				}
							return($dataa);
						}}
		//print_r($dataa);exit; 
			//$user_id[]=$dataa[0]->user_id;
			// print_r($user_id);exit; 
			
			}
			}
			
			 public function select_user_team($dataa){
			  		
					$this->db->distinct();
			      	$this->db->select('*');
					$this->db->from('bom_user_team');
					$this->db->join('bom_manage_player', 'bom_manage_player.player_id= bom_user_team.player_id', 'left');
					$this->db->join('bom_live_score_board', 'bom_live_score_board.player_id= bom_user_team.player_id', 'left');
					
					$this->db->where_in('user_id',$dataa);
					$query=$this->db->get();
					if ($query->num_rows() > 0){
				
				            foreach ($query->result() as $row){
               				
								 $dataaa[] = $row;
            				}
							return $dataaa;
					}
			}
			
			
			 public function key_find(){
			  		
					
			      	$this->db->select('*');
					$this->db->from('key');
					
					$query=$this->db->get();
					if ($query){
				
				            
						return $query->row(); 
					}
			}
			
			
			 public function key_insert($key){
			  		
					
			      	
			       $query=$this->db->insert('key',$key);
				 if($query>0){
					 
					   return $query; 
				 }
				 else{
					 
				       return false;
				 }
			}
			
			
			 public function key_update($key,$pkey){
			  $query=$this->db->where('mkey',$pkey)->update('key',$key);
				  return $query;
			}
			
			
			 public function findid($key){
			  		
					
			      	$this->db->select('*');
					$this->db->from('bom_add_match');
					$this->db->where_in('mkey',$key);
					$query=$this->db->get();
					if ($query){
				
				            
						return $query->row(); 
					}
			}
			 public function accesskey(){
			  		
					
			      	$this->db->select('*')
					->from('bom_add_match')
					->where('is_active','1')
					->order_by("match_start_date_time_minute");
					$query=$this->db->get();
					if ($query){
				 
						return $query->row(); 
					}
			}
		// print_r($dataaa);exit; 
    

}