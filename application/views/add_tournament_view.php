
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Add Tournament</title>
<meta name="keywords" content="">
<meta name="description" content="">
<link rel="<?php  echo base_url(); ?>public/shortcut icon" href="http://invoice.enthuons.com/images/favicon.ico" />
<link href="<?php  echo base_url(); ?>public/css/style.css" rel="stylesheet" type="text/css" media="handheld, screen" />
<link href="<?php  echo base_url(); ?>public/css/jquery-ui.css" rel="stylesheet" type="text/css" />
<link href="<?php  echo base_url(); ?>public/css/admin_panel.css" rel="stylesheet" type="text/css" />
<!--<link rel="stylesheet" href="http://invoice.enthuons.com/js/admin_panel/lavalamp_test.css" type="text/css" media="screen" />-->
<!--[if IE]>  
<script src="http://invoice.enthuons.com/js/html5.js"></script>
<![endif]-->
<!--[if lte IE 6]>
<script type="text/javascript" src="http://invoice.enthuons.com/js/pngfix.js"></script>
<script type="text/javascript" src="http://invoice.enthuons.com/js/ie6.js"></script>
<link rel="stylesheet" href="http://invoice.enthuons.com/css/ie6.css" type="text/css" />
<![endif]-->
<!-- Menu Bar Start -->
<script type="text/javascript" src="<?php  echo base_url(); ?>public/js/jquery.js"></script>
<script type="text/javascript" src="<?php  echo base_url(); ?>public/js/jquery.validate.js"></script>
<script type="text/javascript" src="<?php  echo base_url(); ?>public/js/jquery-ui.js"></script>
<script type="text/javascript" src="<?php  echo base_url(); ?>public/js/maskedinput.js"></script>
<script type="text/javascript" src="<?php  echo base_url(); ?>public/js/admin_panel.js"></script>
<style type="text/css">
.errormessage {display:none; color:#FF0000 !important; background-color:#CCCCCC !important; width:335px; padding:2px;}
#errormess { color:#FF0000; padding:5px; font-size:14px; font-weight:bold;}
#sucessmess {background-color:#CCCCCC; color:#00FF00; padding:5px; font-size:14px; font-weight:bold;}
</style>
<!--<script type="text/javascript" src="http://invoice.enthuons.com/js/banner/jquery.min.js"></script>-->
<!--<script type="text/javascript" src="http://invoice.enthuons.com/js/admin_panel/jquery.lavalamp.js"></script>
<script type="text/javascript" src="http://invoice.enthuons.com/js/admin_panel/jquery.easing.min.js"></script>
<script type="text/javascript" src="http://invoice.enthuons.com/js/admin_panel/jquery.lavalamp.min.js"></script>-->
<!--<script type="text/javascript"> 
    $(function() {
        $("#1, #2, #3").lavaLamp({
            fx: "backout", 
            speed: 900,
            click: function(event, admin_panelItem) {
                return true;
            }
        });
    });
    //jQuery.noConflict();
</script>-->
<!-- Menu Bar End -->
<script type="text/javascript"> 
    function dynamic_change(){
        var sel = document.getElementById("select_lan").value;
        document.getElementById("m_lan").innerHTML = sel;
    }
</script>
<!--Text Field Start-->
<script type="text/javascript"> 
 
    function clearText(thefield){
        if (thefield.defaultValue==thefield.value)
            thefield.value = ""
    } 
 
    function fillText(thefield){
        if (thefield.value=="")
            thefield.value = thefield.defaultValue;
    }
</script>

<script type="text/javascript">        
    ddsmoothadmin_panel.init({
        mainadmin_panelid: "smoothadmin_panel1", //admin_panel DIV id
        orientation: 'h', //Horizontal or vertical admin_panel: Set to "h" or "v"
        classname: 'ddsmoothadmin_panel', //class added to admin_panel's outer DIV
        //customtheme: ["#1c5a80", "#18374a"],
        contentsource: "markup" //"markup" or ["container_id", "path_to_admin_panel_file"]
    })    
    ddsmoothadmin_panel.init({
        mainadmin_panelid: "smoothadmin_panel2", //Menu DIV id
        orientation: 'v', //Horizontal or vertical admin_panel: Set to "h" or "v"
        classname: 'ddsmoothadmin_panel-v', //class added to admin_panel's outer DIV
        //customtheme: ["#804000", "#482400"],
        contentsource: "markup" //"markup" or ["container_id", "path_to_admin_panel_file"]
    })        
    </script>
    
    <script type="text/javascript">
$(document).ready(function()
{
	document.getElementById("dataid").value=1;
});
</script>
    
     <script>
 function checkform()
 {
 var start_date=document.forms['frm']['tou_start_date'].value;
 var end_date=document.forms['frm']['tou_end_date'].value;  
 if(start_date>end_date)
 {
	 
	     document.getElementById('message2').innerHTML="Select correct date your start date is older than end date";
		 return false;
     
 }}
  </script>   
    
   <script>
$(document).ready(function(){
    $( "#datepicker" ).datepicker();
	 $( "#datepicker1" ).datepicker();
});
  </script>   
    
    
    <script type="text/javascript" charset="utf-8">
	
	$(document).ready(function(){
		
		$("#addField").click( function() {
			var iid=document.getElementById("dataid").value;
            var k=parseInt(iid)+1; 
		 var final_data='<fieldset  class="filedset_new"><div class="rowone"><label class="payment_size1">Tournament Team '+k+'</label>  <input id="tou_team_'+iid+'" type="text" name="tou_team_'+iid+'" class="required character"></div></fieldset>';
		
			$("#nameFields").append(final_data);

			iid=parseInt(iid)+1;
			document.getElementById("dataid").value=iid;
		
		});

	});
</script>
    
    
    
    
    
    
<!--Text Field End-->
</head>
<body>
<!-- Navigation Section Start -->

<?php $this->view($header); ?>
<!-- Navigation Section End -->
<!-- Banner Section Start -->

<?php $this->view($banner); ?>
<!-- Banner Section End -->
<div class="cl"></div>
<!-- Mid Section Start -->
<!-- Mid Section Start -->
<section id="mid_pan">
  <div class="mid-wrapper">
    <!--Content Start-->
    <div id="mid-cont">
      <div class="mid-ctr">
        <div class="mid-lt">
          <div class="mid-rt">
            <div class="main-table">
              <!--Head-->
              <div class="head">Add Tournament</div>
              <!--Head end-->
              <div class="col-wt">
                <div class="form-add">
                  <script type="text/javascript">

$(document).ready(function(){

$("#frm").validate();
$(".checklimit").keypress(function(){

var str=$(this).val();
if(str.length >49)
{
$("#"+$(this).attr('id')+'_er').html('<font color="red">characher limit exceed.</font>');
}
else{
$("#"+$(this).attr('id')+'_er').html('');
}
//alert($(this).val());
});
var newemailNO = 0;
var emailNO = 0;

$("#button1").click(function(){
emailNO = document.getElementById('emailNO').value;
newemailNO = parseInt(emailNO)+1; 
$(".xyz").append('<input type="text" name="email_'+newemailNO+'" id="email_'+newemailNO+'" size="32" class="email" value="" style="margin-top:10px;" />');
$(".xyz").append('<input type="button" class="btndelete" name="delete_'+newemailNO+'" id="delete_'+newemailNO+'" align="right" value="Delete&nbsp;" onclick="hidefun('+newemailNO+');" /><div class="cl"></div>');
document.getElementById('emailNO').value = newemailNO;
});
$('#name').blur(function(){
var a = $("#name").val();
if(a!='')
{
$.post("http://invoice.enthuons.com/client/checkname", {	Name: $('#name').val()}, function(response){ $('#Loading').html(response); });
}
else
{
$('#Loading').html('');
}
return false;
});

});
function hidefun(value)
{

if(document.getElementById('email_'+value))
{ 
document.getElementById('email_'+value).value='';
document.getElementById('email_'+value).style.display = 'none';
document.getElementById('delete_'+value).style.display = 'none';
}
}

</script>
                  <form action="<?php echo base_url()?>manage_tournament/add"  method="post" enctype="multipart/form-data" id="frm" onsubmit="return checkform();">
                  
                    <div class="rowone">
                         <label>Tournament short Name * </label>
                         <input id="tou_short_name" class="required character" type="text" value="<?php echo $ms;?>" name="tou_short_name" />
                    </div>
                    
                    <div class="rowone">
                          <label>Tournament full Name * </label>
                          <input id="tou_full_Name" class="required character" type="text" value="" name="tou_full_Name" />
                    </div>
                    
                    
                    <div class="rowone">
                            <label>Game name *  </label>
                            <select id="abc" name="game_name" >
                   	         <?php 
					               foreach($gamename as $key=>$value)
					                {
					                      echo " <option value=".$value->g_id.">".$value->g_name."</option>";
					                }
                    		?>
                            </select>
                    </div>
                    
                    <div class="rowone">
                         <label>Tournament Start date *  </label>
                         <input id="datepicker" class="required character" type="text" name="tou_start_date">
                         <span id="address1_er"></span>
                    </div>
                   
                    <div class="rowone">
                          <label>Tournament End date* </label>
                          <input id="datepicker1" class="required character" type="text" name="tou_end_date">
                           <span id="address1_er"></span><span id="message2"style="color: #FF0000;display: block; float: right; margin-left: 5px;  font-size: 16px; width: 456px;margin-top: -37px;"></span>
                    </div>
                    
                    <div id="nameFields">
                    
                          <div class="rowone">
                                <label>Tournament Team 1 </label>
                                <input id="tou_team_0" class="required character" type="text" name="tou_team_0">
                                 <span id="address1_er"></span>
                          </div>
                   
                   </div>
                   <input type="hidden" id="dataid" value="1" name="loop" /><br />
                   <div class="hyperlink_aaa"><a href="#" onclick="return false;" id="addField" style="float: left;margin-left: -154px;"><img src="<?php echo base_url()?>public/images/add-newclient.png" alt="" />Add More</a></div>
                   
                    <div class="add-right-mn">
                               <div class="total" style="padding-left: 268px;">
                                       <div class="cl"></div>
                       
                                              <input name="Submit"  id="BtnSubmit" type="submit" class="btn_save" value="Save;" />
						                            	<a href="<?php echo base_url()?>admin_panel/manage_tournament"><img src="<?php echo base_url()?>public/images/cancel.png" /></a>
						<span id="errormess"><?php	if( isset ($msgg))	{ echo $msgg;} ?></span>
                               </div>
					   
                    </div>
                  </form>
                </div>
              </div>
              <div class="cl"></div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!--Content Start-->
    <div class="cl"></div>
  </div>
</section>
<!-- Mid Section End -->
<?php $this->view($footer); ?>
<!-- Footer Section End -->
</body>
</html>
