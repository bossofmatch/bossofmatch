<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Manage Match</title>
<meta name="keywords" content="">
<meta name="description" content="">
<link rel="shortcut icon" href="<?php  echo base_url(); ?>public/images/favicon.ico" />
<link href="<?php  echo base_url(); ?>public/css/style.css" rel="stylesheet" type="text/css" media="handheld, screen" />
<link href="<?php  echo base_url(); ?>public/css/jquery-ui.css" rel="stylesheet" type="text/css" />
<link href="<?php  echo base_url(); ?>public/css/admin_panel.css" rel="stylesheet" type="text/css" />
<!--<link rel="stylesheet" href="http://invoice.enthuons.com/js/admin_panel/lavalamp_test.css" type="text/css" media="screen" />-->
<!--[if IE]>  
<script src="http://invoice.enthuons.com/js/html5.js"></script>
<![endif]-->
<!--[if lte IE 6]>
<script type="text/javascript" src="http://invoice.enthuons.com/js/pngfix.js"></script>
<script type="text/javascript" src="http://invoice.enthuons.com/js/ie6.js"></script>
<link rel="stylesheet" href="http://invoice.enthuons.com/css/ie6.css" type="text/css" />
<![endif]-->
<!-- Menu Bar Start -->
<script type="text/javascript" src="<?php  echo base_url(); ?>public/js/jquery.js"></script>
<script type="text/javascript" src="<?php  echo base_url(); ?>public/js/jquery.validate.js"></script>
<script type="text/javascript" src="<?php  echo base_url(); ?>public/js/jquery-ui.js"></script>
<script type="text/javascript" src="<?php  echo base_url(); ?>public/js/maskedinput.js"></script>
<script type="text/javascript" src="<?php  echo base_url(); ?>public/js/admin_panel.js"></script>
<!--<script type="text/javascript" src="http://invoice.enthuons.com/js/banner/jquery.min.js"></script>-->
<!--<script type="text/javascript" src="http://invoice.enthuons.com/js/admin_panel/jquery.lavalamp.js"></script>
<script type="text/javascript" src="http://invoice.enthuons.com/js/admin_panel/jquery.easing.min.js"></script>
<script type="text/javascript" src="http://invoice.enthuons.com/js/admin_panel/jquery.lavalamp.min.js"></script>-->
<!--<script type="text/javascript"> 
    $(function() {
        $("#1, #2, #3").lavaLamp({
            fx: "backout", 
            speed: 900,
            click: function(event, admin_panelItem) {
                return true;
            }
        });
    });
    //jQuery.noConflict();
</script>-->
<!-- Menu Bar End -->
<script type="text/javascript"> 
    function dynamic_change(){
        var sel = document.getElementById("select_lan").value;
        document.getElementById("m_lan").innerHTML = sel;
    }
</script>
<!--Text Field Start-->
<script type="text/javascript"> 
 
    function clearText(thefield){
        if (thefield.defaultValue==thefield.value)
            thefield.value = ""
    } 
 
    function fillText(thefield){
        if (thefield.value=="")
            thefield.value = thefield.defaultValue;
    }
</script>
<script>
 function check()
 {
 
     var status=document.forms['search']['keys'].value;
	 
	  if(status=="")
	  {
document.getElementById("sea").innerHTML= "Please enter some text to search";

		  return false;
		  }
		

 
 }
 function check1()
 {
 
     var status=document.forms['search']['keys'].value;
	 
	  if(status!="")
	  {
	
		  document.getElementById("sea").innerHTML= "";
		  }
		  
	 
 
 }

</script>

<script type="text/javascript">        
    ddsmoothadmin_panel.init({
        mainadmin_panelid: "smoothadmin_panel1", //admin_panel DIV id
        orientation: 'h', //Horizontal or vertical admin_panel: Set to "h" or "v"
        classname: 'ddsmoothadmin_panel', //class added to admin_panel's outer DIV
        //customtheme: ["#1c5a80", "#18374a"],
        contentsource: "markup" //"markup" or ["container_id", "path_to_admin_panel_file"]
    })    
    ddsmoothadmin_panel.init({
        mainadmin_panelid: "smoothadmin_panel2", //Menu DIV id
        orientation: 'v', //Horizontal or vertical admin_panel: Set to "h" or "v"
        classname: 'ddsmoothadmin_panel-v', //class added to admin_panel's outer DIV
        //customtheme: ["#804000", "#482400"],
        contentsource: "markup" //"markup" or ["container_id", "path_to_admin_panel_file"]
    })        
    </script>
	<script>
function validateForm(id,active)
{
 var param = {'id' : id,'active' : active};

 $.ajax({
        type: 'POST',
		 data : param,
        url: "<?php echo base_url()?>manage_match/active",
       success : function(){
	if(active==1)
	{
	active=0;
	  document.getElementById(id).innerHTML="<a href='javascript:void(0);' onclick=\"return validateForm('"+id+"','"+active+"')\">Active</a>";
     }
	 else
	 {
	 active=1;
	  document.getElementById(id).innerHTML="<a href='javascript:void(0);' onclick=\"return validateForm('"+id+"','"+active+"')\">Dactive</a>";     
      }
	            },
    }); 

}
function validateForma(id,ii)
{
var r=confirm("Are you sure to delete this!");


if (r==true)
  {
 var parama = {'name' : ii};

 $.ajax({
        type: 'POST',
		 data : parama,
        url: "<?php echo base_url()?>manage_match/delete",
        success : function(){
		 document.getElementById(id).innerHTML="";
		 document.getElementById('message').innerHTML="User has been Deleted";
		
		},
    }); 
  }


 

}

</script>
<!--Text Field End-->
</head>
<body>
<!-- Navigation Section Start -->

<?php  $this->view($header); ?>

<!-- Navigation Section End -->
<!-- Banner Section Start -->

<?php  $this->view($banner); ?>

<!-- Banner Section End -->
<div class="cl"></div>
<!-- Mid Section Start -->
<!-- Mid Section Start -->
<section id="mid_pan">
  <div class="mid-wrapper">
    <!--Title Bar-->
    	<div class=""><h1 style="margin-left: 12px; padding-top: 15px;">Send mail About Match</h1></div>
   
      <div class="pad">
        <div class="title-bg">
          <div class="title-main">
            <label>Want to send mail<a href="<?php echo base_url()?>admin_panel/fmail">&nbsp;Click here</a>&nbsp;<span style="color:#F00;"><?php if(isset($smsg)){echo $smsg;} ?></span>  </label>
           </div>
        
              <div class="serchm">
              
				
              </div>
			
            </div>
	
            <div class="colmh1 colmh1-last" style="width: 42%; padding-left:2px;padding-top: 10px;">
              <ul style="float:right;">
                              </ul><span id="sea" style="color:#FF0000"></span>
            </div>
            <!--<span class="view_all"><a href="http://invoice.enthuons.com/client/">View All</a></span>--> </div>
        </div>
      </div>

    <!--Title Bar end-->
	<span id="message" style="color:#006600;margin-left: 490px; font-size:12px; font-weight:bold"></span>
    <div class="cl"></div><span class="add_client"></span>
    <!--Content Start-->
    <div id="mid-cont" style="margin-top:0px;">
      <div class="mid-ctr">
        <div class="mid-lt">
          <div class="mid-rt">
            <div class="cl"></div>
            <div class="main-table">
              <!--Head-->
           
       
                  
                </div>
                
                
                              </div>
              <div class="cl"></div>
			          <div class="pre_next">
    <ul>
    <p></p>
	</ul>
    </div>
            </div>
          </div>
        </div>
      </div>
    </div>
	  
    <!--Content end-->
    <div class="cl"></div>
	
	

  </div>
</section>
<!-- Mid Section End -->
<!-- Footer Section Start -->

<?php  $this->view($footer); ?>

<!-- Footer Section End -->
</body>
</html>
