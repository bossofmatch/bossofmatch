<!DOCTYPE html>
<html lang="en">
<head>
<meta name="google-site-verification" content="P_ySFK7sK6kN2vAAEwJGGZsaOuQiQCfjsVNta1mzHAc" />
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="Get T20 IPL Cricket News. BossOfMatch" />
<meta name="author" content="">
<meta name="keywords" content="Winners" />
<link rel="shortcut icon" type="image/x-icon" href="<?php  echo base_url(); ?>public/f_img/icons.ico" >

<title>IPL News, Cricket T20 News-BossOfMatch </title>
<!-- Bootstrap core CSS -->
<link href="<?php  echo base_url(); ?>public/css/f_css/bootstrap.min.css" rel="stylesheet">
<!-- Custom styles for this template -->
<link href="<?php  echo base_url(); ?>public/css/f_css/justified-nav.css" rel="stylesheet">
<link href="<?php  echo base_url(); ?>public/css/f_css/style.css" rel="stylesheet">
<link href="<?php  echo base_url(); ?>public/css/f_css/carousel.css" rel="stylesheet">
<!-- Just for debugging purposes. Don't actually copy this line! -->
<!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
<link rel="stylesheet" href="<?php  echo base_url(); ?>public/css/f_css/media.css" type="text/css"   >\

</head>
<body>
<div class="container container_clr">
  <div class="masthead mrg_top">
    <!--header-->
	<?php $this->view($header); ?>
	<!--header close-->
 
  <!-- Jumbotron -->
 	<?php  //echo $this->view($top); ?>
    <!-- Example row of columns -->
    <div class="row">
      <div class="col-lg-9 col_mrg_ftsk_3">
        <div class="how_it_work">
          <p>IPL News
          <p> 
        </div>
        <div class="how_it_work_fild news_fild_w">
          <p> Get up to date with the news of cricket. You can find latest cricket news and catch the events, what your favourite players are doing, what is happening in cricket etc. You will get all the news here.</p>
          <div class="how_it_work_step" id="live_news"><br>
<?php foreach ($inc as $value)
{
?>
  <li>          <?php  echo $value; ?></li>
  <?php } ?>
          
          </div>
        </div>
        
      </div>
      <div class="col-lg-3 add_rgt"> <!--<img src="<?php  echo base_url(); ?>public/f_img/add.jpg"/> --></div>
    </div>
   <div class="row mrg_row">
        
        <div class="col-lg-12 ipl_mrg_rgt "> <img src="<?php  echo base_url(); ?>public/f_img/google_add.jpg" class="add_bottom"/> </div>
      </div>
  </div>
    <!-- Site footer -->
  </div>
</div>
<?php $this->view($footer); ?>
<!-- /container -->
<!-- Bootstrap core JavaScript
    ================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
<script src="<?php  echo base_url(); ?>public/js/f_js/bootstrap.min.js"></script>
<script src="<?php  echo base_url(); ?>public/js/f_js/docs.min.js"></script>
</body>
</html>
