<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>
<?php if(isset($msg)){echo $msg;} ?>
</title>
<meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">
<link rel="shortcut icon" type="image/x-icon" href="<?php  echo base_url(); ?>public/f_img/icons.ico" >
<link rel="stylesheet" type="text/css" href="<?php  echo base_url(); ?>public/lib/bootstrap/css/bootstrap.css">
<link rel="stylesheet" type="text/css" href="<?php  echo base_url(); ?>public/stylesheets/theme.css">
<link rel="stylesheet" href="<?php  echo base_url(); ?>public/lib/font-awesome/css/font-awesome.css">
<link rel="stylesheet" type="text/css" href="<?php  echo base_url(); ?>public/stylesheets/style.css">
<script src="<?php  echo base_url(); ?>public/lib/jquery-1.7.2.min.js" type="text/javascript"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
<!--<script type="text/javascript" src="<?php  //echo base_url(); ?>public/js1/jquery.min.js"></script>-->
<script type="text/javascript" src="<?php  echo base_url(); ?>public/js1/file_uploads.js"></script>
<script type="text/javascript" src="<?php  echo base_url(); ?>public/js1/vpb_script.js"></script>
<link href="<?php  echo base_url(); ?>public/css1/style.css" rel="stylesheet" type="text/css">
<!-- Demo page code -->
<style type="text/css">
#line-chart {
	height: 300px;
	width: 800px;
	margin: 0px auto;
	margin-top: 1em;
}
.brand {
	font-family: georgia, serif;
}
.brand .first {
	color: #ccc;
	font-style: italic;
}
.brand .second {
	color: #fff;
	font-weight: bold;
}
</style>
<link rel="shortcut icon" href="../public/images/favicon.ico">
<link rel="apple-touch-icon-precomposed" sizes="144x144" href="file:///D|/New folder/boss/assets/ico/apple-touch-icon-144-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="114x114" href="file:///D|/New folder/boss/assets/ico/apple-touch-icon-114-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="72x72" href="file:///D|/New folder/boss/assets/ico/apple-touch-icon-72-precomposed.png">
<link rel="apple-touch-icon-precomposed" href="file:///D|/New folder/boss/assets/ico/apple-touch-icon-57-precomposed.png">
<script>
function validate()
{

	 var name=document.getElementById("name1").value;
   var number=document.getElementById("number").value;
   var s_provider=document.getElementById("Service_provider_name").value;
   var age=document.getElementById("age").value;
	  if(name=="")
	  {
	       document.getElementById("name1_error").style.display = 'block';
		      document.getElementById("name1_error").innerHTML="Please enter your name.";	
	  }
	  else
	  {
	         document.getElementById("name1_error").innerHTML= "";
	  }
    if(number==""){
          document.getElementById("m_error").style.display = 'block';
          document.getElementById("m_error").innerHTML="Please enter your Mobile number."; 
    }
    else{
      document.getElementById("m_error").innerHTML= "";
    }
    if(s_provider=="Select your Operator"){
          document.getElementById("sevice_error").style.display = 'block';
          document.getElementById("sevice_error").innerHTML="Select Operator Service"; 
    }
    else{

       document.getElementById("sevice_error").innerHTML= "";

    }
    if(age==""){

          document.getElementById("a_error").style.display = 'block';
          document.getElementById("a_error").innerHTML="Enter your age"; 
    }
    else{
      document.getElementById("a_error").innerHTML= "";

    }
	  
	  if(name!="" && number!="" && age!="" && s_provider!="Select your Operator")
	  {
	 
	  document.getElementById('vasPLUS_Programming_Blog_Form').submit();
	  }
	 
}
function validate1()
{
     document.getElementById("number_error").innerHTML= "";
    document.getElementById("sevice_error").innerHTML= "";
    document.getElementById("name1_error").innerHTML= ""; 
    document.getElementById("password_error").innerHTML= "";
    document.getElementById("email_error").innerHTML= "";
    document.getElementById("caperror").innerHTML= "";


}

function error2(){
document.getElementById("m_error").innerHTML= "";

}

function check(e){

  if(e=='Select your Operator'){

      document.getElementById("sevice_error").style.display = 'block';  
      document.getElementById("sevice_error").innerHTML= "Select Operator Service.";
    }
    else{

      document.getElementById("sevice_error").innerHTML= "";
    }
}
</script>
</head>

<body class="">
<!--<![endif]--> 
<!--------------------------------------------------------------header---------------------------------------------------------------------------------->
<?php  $se=$this->session->userdata; $this->view($header); ?>
<!---------------------------------------------------------------header close--------------------------------------------------------------------------> 
<!---------------------------------------------------------------sidebar-------------------------------------------------------------------------------->
<?php $this->view($sidebar); ?>
<!---------------------------------------------------------------sidebar close-------------------------------------------------------------------------->

<div class="content">
  <div class="header">
    <div class="stats"> 
      <!-- <p class="stat"><span class="number">53</span>tickets</p>
      <p class="stat"><span class="number">27</span>tasks</p>
      <p class="stat"><span class="number">15</span>waiting</p>--> 
    </div>
    <h1 class="page-title">Setting</h1>
  </div>
  <ul class="breadcrumb">
    <li><a href="<?php echo base_url()?>dashboard">Home</a> <span class="divider">/</span></li>
    <li class="active">Setting</li>
  </ul>
  <div class="container-fluid"> 
    <!-- <div class="row-fluid">
        <div class="row-fluid"> <img  src="<?php  echo base_url(); ?>public/u_images/google_add.jpg" alt=""> </div>
    </div>-->
    <div class="row-fluid">
      <div class="block span12 bg_clr"> <a href="#tablewidget" class="block-heading" data-toggle="collapse">Setting</a>
        <div id="tablewidget" class="block-body ">
          <div class="account_setting">
            <ul>
              <form id="vasPLUS_Programming_Blog_Form" action="<?php echo base_url()?>setting/update_profile" method="post" enctype="multipart/form-data" autocomplete="off" >
                <li>
                  <div class="account_setting_lft"> Name :</div>
                  <div class="account_setting_rgt">
                    <input type="text" value="<?php echo $profile->u_name;?>"  class="set_mrg_int" name="name1" id="name1" onFocus="validate1();"/>
                    <div onMouseOver="document.getElementById('tt2').style.display='block'" onMouseOut="document.getElementById('tt2').style.display='none'" class="questionMark">
                      <div id="tt2" class="toolTip">
                        <h6 class="justifyed_text">
                          <p><font color="#FF0000"> Note*:</font>Don't use abusing language, otherwise your account will be block right away, without give you prior information.</p>
                        </h6>
                      </div>
                    </div>
                    <span id="name1_error" style="display:none; color:red;"></span></div>
                </li>
                <li>
                  <div class="account_setting_lft">Email Id : </div>
                  <div class="account_setting_rgt">
                    <input type="text" value="<?php echo $profile->u_email;?>" class="set_mrg_int" readonly/>
                  </div>
                </li>
                <li>
                  <div class="account_setting_lft">Mobile No. : </div>
                  <div class="account_setting_rgt">
                    <input type="text" name="number" value="<?php echo $profile->mobile_no;?>" class="set_mrg_int" onKeyUp="return error2();" id="number" maxlength="10">
                    <span id="m_error" style="display:none; color:red;"></span> <span id="mobile_error" style="display:none; color:red;">Please enter only number.</span> 
                    <script type="text/javascript">
                      var specialKeys = new Array();
                      specialKeys.push(8); //Backspace
                      $(function () {
                          $("#number").bind("keypress", function (e) {
                              var keyCode = e.which ? e.which : e.keyCode
                              var ret = ((keyCode >= 48 && keyCode <= 57) || specialKeys.indexOf(keyCode) != -1);
                              $("#mobile_error").css("display", ret ? "none" : "inline");
                              return ret;
                          });
                          $("#number").bind("paste", function (e) {
                              return false;
                          });
                          $("#number").bind("drop", function (e) {
                              return false;
                          });
                      });
                 </script>
                    <div onMouseOver="document.getElementById('tt1').style.display='block'" onMouseOut="document.getElementById('tt1').style.display='none'" class="questionMark">
                      <div id="tt1" class="toolTip">
                        <h6 class="justifyed_text">
                          <p>We will recharge this mobile number when you win the Boss Of Match title for that particular match. </p>
                          <strong>Terms & Conditions: </strong>When you win the Boss Of Match title for a match, we will email you to get your confirmation. Please reply to that mail within 24 hours so that we can start recharge process for your registered mobile number. If we do not get any reply within 24 hours, your mobile number will not be recharged. Mobile recharge is available only for indian users.</h6>
                      </div>
                    </div>
                  </div>
                  <span id="number_error" class="span_error1" style="display:none"></span></li>
                <div class="clear"></div>
                <li class="pre_postpaid">
                  <div class="account_setting_lft"> Mobile Connection </div>
                  <div class="account_setting_rgt">
                    <input type="radio" name="connection_type" <?php if($profile->connection_type=='Prepaid'){ echo ' checked="checked"'; }?> value="Prepaid" class="form-control wdt_int" >
                  </div>
                  <div class="account_setting_lft"> Prepaid </div>
                  <div class="account_setting_rgt">
                    <input type="radio" name="connection_type" <?php if($profile->connection_type=='Postpaid'){ echo ' checked="checked"'; }?> value="Postpaid" class="form-control wdt_int" >
                  </div>
                  <div class="account_setting_lft"> Postpaid </div>
                </li>
                <li>
                  <div class="account_setting_lft">Service Provider : </div>
                  <div class="account_setting_rgt">
                    <select id="Service_provider_name"  class="set_mrg_int" name="Service_provider_name" onChange="check(this.value);">
                      <option value="Select your Operator">Select your Operator</option>
                      <option value="Aircel"<?php if($profile->service_provider=='Aircel'){ echo 'selected="selected"'; }?>>Aircel</option>
                      <option value="AirTel"<?php if($profile->service_provider=='AirTel'){ echo 'selected="selected"'; }?>>AirTel</option>
                      <option value="Cheers"<?php if($profile->service_provider=='Cheers'){ echo 'selected="selected"'; }?>>Cheers</option>
                      <option value="Loop Mobile"<?php if($profile->service_provider=='Loop Mobile'){ echo 'selected="selected"'; }?>>Loop Mobile</option>
                      <option value="MTNL"<?php if($profile->service_provider=='MTNL'){ echo 'selected="selected"'; }?>>MTNL</option>
                      <option value="MTS"<?php if($profile->service_provider=='MTS'){ echo 'selected="selected"'; }?>>MTS</option>
                      <option value="Ping"<?php if($profile->service_provider=='Ping'){ echo 'selected="selected"'; }?>>Ping</option>
                      <option value="Reliance CDMA"<?php if($profile->service_provider=='Reliance CDMA'){ echo 'selected="selected"'; }?>>Reliance CDMA</option>
                      <option value="Reliance GSM"<?php if($profile->service_provider=='Reliance GSM'){ echo 'selected="selected"'; }?>>Reliance GSM</option>
                      <option value="S Tel"<?php if($profile->service_provider=='S Tel'){ echo 'selected="selected"'; }?>>S Tel</option>
                      <option value="T24"<?php if($profile->service_provider=='T24'){ echo 'selected="selected"'; }?>>T24</option>
                      <option value="Tata Docomo CDMA"<?php if($profile->service_provider=='Tata Docomo CDMA'){ echo 'selected="selected"'; }?>>Tata Docomo CDMA</option>
                      <option value="Tata Docomo GSM"<?php if($profile->service_provider=='Tata Docomo GSM'){ echo 'selected="selected"'; }?>>Tata Docomo GSM</option>
                      <option value="Uninor"<?php if($profile->service_provider=='Uninor'){ echo 'selected="selected"'; }?>>Uninor</option>
                      <option value="Videocon"<?php if($profile->service_provider=='Videocon'){ echo 'selected="selected"'; }?>>Videocon</option>
                      <option value="Virgin CDMA"<?php if($profile->service_provider=='Virgin CDMA'){ echo 'selected="selected"'; }?>>Virgin CDMA</option>
                      <option value="Virgin GSM"<?php if($profile->service_provider=='Virgin GSM'){ echo 'selected="selected"'; }?>>Virgin GSM</option>
                      <option value="Vodafone"<?php if($profile->service_provider=='Vodafone'){ echo 'selected="selected"'; }?>>Vodafone</option>
                      <option value="BSNL"<?php if($profile->service_provider=='BSNL'){ echo 'selected="selected"'; }?>>BSNL</option>
                      <option value="Idea"<?php if($profile->service_provider=='Idea'){ echo 'selected="selected"'; }?>>Idea</option>
                      <option value="Dolphin"<?php if($profile->service_provider=='Dolphin'){ echo 'selected="selected"'; }?>>Dolphin</option>
                    </select>
                  </div>
                  <span id="sevice_error" style="display:none;  color:red; "></span> </li>
                <li>
                  <div class="account_setting_lft"> Age :</div>
                  <div class="account_setting_rgt">
                    <input type="text" value="<?php if($profile->age!=""){ echo $profile->age;}?>" class="set_mrg_int" id="age" name="age" maxlength="3" />
                  </div>
                  <span id="a_error" style="display:none;  color:red;"></span> <span id="Age_error" style="display:none; color:red;">Enter only numeric Value.</span> 
                  <script type="text/javascript">
                      var specialKeys = new Array();
                      specialKeys.push(8); //Backspace
                      $(function () {
                          $("#age").bind("keypress", function (e) {
                              var keyCode = e.which ? e.which : e.keyCode
                              var ret = ((keyCode >= 48 && keyCode <= 57) || specialKeys.indexOf(keyCode) != -1);
                              $("#Age_error").css("display", ret ? "none" : "inline");
                              return ret;
                          });
                          $("#age").bind("paste", function (e) {
                              return false;
                          });
                          $("#age").bind("drop", function (e) {
                              return false;
                          });
                      });
                 </script> 
                </li>
                <li>
                  <div class="account_setting_lft"> Sex : </div>
                  <div class="account_setting_rgt">
                    <select name="sex" class="set_mrg_int">
                      <option value="Male"<?php if($profile->sex=='Male'){ echo 'selected="selected"'; }?>>Male</option>
                      <option value="Male"<?php if($profile->sex=='Female'){ echo 'selected="selected"'; }?>>Female</option>
                    </select>
                  </div>
                </li>
                <li>
                  <div class="account_setting_lft">Upload your image</div>
                  <div class="account_setting_rgt"> 
                    <!--<input type="button" value="Upload" onclick="validateForm();" class="set_mrg2"/>-->
                    <center>
                      <div align="left" class="vpb_main_wrapper"> <br clear="all" />
                        <div id="vasPhoto_uploads_Status" align="center" style="font-family:Verdana, Geneva, sans-serif; font-size:12px; color:black; line-height:25px;"></div>
                        <center>
                          <div style="width:350px; margin-left:20px;" align="center">
                            <div style="padding:10px; padding-top:18px;float:left;font-family:Verdana, Geneva, sans-serif; font-size:12px; color:black; width:100px;" align="left">Browse Photo:</div>
                            <div style="padding:10px;float:left; font-family:Verdana, Geneva, sans-serif; font-size:12px; color:black; width:200px;" align="left">
                              <div class="vasplusfile_adds">
                                <input type="file" name="vasPhoto_uploads" id="vasPhoto_uploads" style="opacity:0;-moz-opacity:0;filter:alpha(opacity:0);z-index:9999;width:90px;padding:5px;cursor:default;" />
                                <input type="hidden" value="<?php echo base_url(); ?>" id="url_name" />
                              </div>
                            </div>
                            <br clear="all">
                          </div>
                        </center>
                        <br clear="all" />
                      </div>
                    </center>
                  </div>
                </li>
                <li>
                  <div class="account_setting_lft">&nbsp;</div>
                  <div id="vasPhoto_uploads_Status" align="center" style="font-family:Verdana, Geneva, sans-serif; font-size:12px; color:black; line-height:25px;"></div>
                </li>
                <li>
                  <div class="account_setting_lft sub_blank">&nbsp;</div>
                  <div class="account_setting_rgt">
                    <input type="button" value="Submit" class="set_mrg" onClick="return validate();" />
                  </div>
                </li>
              </form>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!---------------------------------------------------footer---------------------------------------------------->
<?php  $this->view($footer); ?>
<!---------------------------------------------------footer close---------------------------------------------->
</div>
</div>
</div>
<script src="<?php echo base_url();?>public/lib/bootstrap/js/bootstrap.js"></script> 
<script type="text/javascript">
        $("[rel=tooltip]").tooltip();
        $(function() {
            $('.demo-cancel-click').click(function(){return false;});
        });
    </script>
</body>
</html>
