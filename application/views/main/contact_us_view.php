<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="Contact Us" />
<meta name="keywords" content="" />
<meta name="author" content="">
<meta name="google-site-verification" content="P_ySFK7sK6kN2vAAEwJGGZsaOuQiQCfjsVNta1mzHAc" />
<link rel="shortcut icon" type="image/x-icon" href="<?php  echo base_url(); ?>public/f_img/icons.ico" >
<link rel="shortcut icon" href="../public/images/favicon.ico">
<!--<title>BossOfMatch | Boss of Match | Play with IPL | Create my team | cricket</title>-->
<title>Contact Us-BossOfMatch </title>
<!-- Bootstrap core CSS -->
<link href="<?php  echo base_url(); ?>public/css/f_css/bootstrap.min.css" rel="stylesheet">
<!-- Custom styles for this template -->
<link href="<?php  echo base_url(); ?>public/css/f_css/justified-nav.css" rel="stylesheet">
<link href="<?php  echo base_url(); ?>public/css/f_css/style.css" rel="stylesheet">
<link href="<?php  echo base_url(); ?>public/css/f_css/carousel.css" rel="stylesheet">
<script src="<?php base_url();?>public/captcha/md5.js"></script>
<script src="<?php base_url();?>public/captcha/main.js"></script>
<!-- Just for debugging purposes. Don't actually copy this line! -->
<!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
<link rel="stylesheet" href="<?php  echo base_url(); ?>public/css/f_css/media.css" type="text/css"   >
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<script>
function validate()
{

	var name=document.getElementById("name1").value;
	var email=document.getElementById("email1").value;
	var contact=document.getElementById("contact_no").value;
	var suggest=document.getElementById("suggest").value;
		var cap=document.getElementById("cap").value;
	var count=0;
	  if(name=="")
	  {
		document.getElementById("name1_error").innerHTML= "Please enter your name.";	
	  }
	  else
	  {
	  document.getElementById("name1_error").innerHTML= "";
	  }
	   
	   if(contact=="")
	  {
		document.getElementById("contact_error").innerHTML= "Please enter your contact number.";
	  }
	 else
	 {
	 document.getElementById("contact_error").innerHTML="";
	 }
	if(suggest=="")
	  {
		document.getElementById("suggest_error").innerHTML= "Please write some Message for us.";
	  }
	 else
	 {
	 document.getElementById("suggest_error").innerHTML="";
	 }
	var pattern=/^([a-zA-Z0-9_.-])+@([a-zA-Z0-9_.-])+\.([a-zA-Z])+([a-zA-Z])+/;
    if(pattern.test(email))
	{         
		 document.getElementById("email1_error").innerHTML= "";  
		 count=1;
    }
	else
	{   
		document.getElementById("email1_error").innerHTML= "Please enter your valid email.";
    }
	if(cap=="")
	  {
	 
		document.getElementById("caperror").innerHTML= "Please enter captca.";	
	  }
	  else
	  {
	  document.getElementById("caperror").innerHTML= "";
	  }
	  if((name!="")&&(count==1)&&(suggest!="")&&(contact!=""))
	  {
	  
	  document.getElementById('frm').submit();
	  }
	 
}
function validate1()
{
	document.getElementById("email1_error").innerHTML= "";  
	document.getElementById("name1_error").innerHTML= "";
   document.getElementById("suggest_error").innerHTML="";
      document.getElementById("caperror").innerHTML = "";
   document.getElementById("contact_error").innerHTML="";
	 
}
</script>
</head>
<body>
<div class="container container_clr">
  <div class="masthead mrg_top">
    <!--header-->
	<?php  $this->view($header); ?>
	<!--header close-->
 
  <!-- Jumbotron -->
 	<?php  //$this->view($top); ?>
    <!-- Example row of columns -->
  <div class="row">
      <div class="col-lg-9 col_mrg_ftsk_3">
        <div class="how_it_work">
          <p>Contact_us
          <p> 
        </div>
        <div class="how_it_work_fild">
          <p>We would be happy to hear your suggestions. Your suggestions will help us to improve BossOfMatch and make this game more entertaining and enthralling. Please tell us what do you think about this game. Please let us know if you are getting any issue with the game we would work on it immediately and let you know once the issue is resolved. Your participation will help us to improve the features of this game and make BossOfMatch more entertaining. Please do not forget to share BossOfMatch with your friends.</p>
          <div class="suggetion">
		  <span class="span_message"><?php if(isset($message)){ echo $message;}?></span>
            <form role="form " action="<?php echo base_url()?>email_f/email" method="post" class="" name="frm" id="frm">
			
              <div class="form-group">
                <label class="wdt_int_text_2" for="exampleInputPassword1">Name</label>
                <input type="text" class="form-control wdt_int" id="name1" name="name1" onFocus="validate1();" >
              </div><span id="name1_error" class="span_error"></span>
              <div class="form-group">
                <label class="wdt_int_text_2" for="exampleInputEmail1">Email </label>
                <input type="text" class="form-control wdt_int" id="email1" name="email1" onFocus="validate1();" >
              </div><span id="email1_error" class="span_error"></span>
              <div class="form-group">
                <label class="wdt_int_text_2" for="exampleInputPassword1">Contact No.</label>
                <input type="text" class="form-control wdt_int" id="contact_no" name="contact_no" onFocus="validate1();" >
              </div><span id="contact_error" class="span_error"></span>
              <div class="form-group mrg_bott">
                <label class="wdt_int_text_2" for="exampleInputPassword1">Message</label>
                <textarea cols="3" rows="2" class="wdt_int_2" name="suggest" id="suggest" onFocus="validate1();" ></textarea>
              </div><span id="suggest_error" class="span_error"></span><br>
			   <div class="capcha">
                       <img src="<?php echo base_url();?>public/captcha/captcha.php?idd=<?php echo time(); ?>" class="form_captcha"/>
                   </div>
			   <label class="wdt_int_text_2" for="exampleInputPassword1">Enter above text</label>
			    <input type="text" name="cap" class="form-control wdt_int " id="cap" onFocus="validate1();">
               
              </div><span id="caperror" class="span_error" ><?php if(isset($err)){echo $err;}  ?></span><br>
			  
              <button type="button" onClick="validate();" class="btn btn-default sub_rgt_2">Send</button>
            </form>
          </div>
		  
        </div>
      </div>
      <div class="col-lg-3 add_rgt"> <!--<img src="<?php  echo base_url(); ?>public/f_img/add_small.jpg"/>--> </div>
    </div>
    <div class="row mrg_row">
        
        <div class="col-lg-12 ipl_mrg_rgt "><!-- <img src="<?php  echo base_url(); ?>public/f_img/google_add.jpg" class="add_bottom"/> --></div>
      </div>
  </div>
    <!-- Site footer -->
  </div>
</div>
<?php  $this->view($footer); ?>
<!-- /container -->
<!-- Bootstrap core JavaScript
    ================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
<script src="<?php  echo base_url(); ?>public/js/f_js/bootstrap.min.js"></script>
<script src="<?php  echo base_url(); ?>public/js/f_js/docs.min.js"></script>
</body>
</html>
