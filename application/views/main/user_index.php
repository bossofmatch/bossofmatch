<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title><?php if(isset($msg)){echo $msg;} ?></title>
<meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<link rel="shortcut icon" type="image/x-icon" href="<?php  echo base_url(); ?>public/f_img/icons.ico" >
<meta name="author" content="">
<link rel="stylesheet" type="text/css" href="<?php  echo base_url(); ?>public/lib/bootstrap/css/bootstrap.css">
<link rel="stylesheet" type="text/css" href="<?php  echo base_url(); ?>public/stylesheets/theme.css">
<link rel="stylesheet" href="<?php  echo base_url(); ?>public/lib/font-awesome/css/font-awesome.css">
<link rel="stylesheet" type="text/css" href="<?php  echo base_url(); ?>public/stylesheets/style.css">
<script src="<?php  echo base_url(); ?>public/lib/jquery-1.7.2.min.js" type="text/javascript"></script>
<!-- Demo page code -->
<style type="text/css">
        #line-chart {
            height:300px;
            width:800px;
            margin: 0px auto;
            margin-top: 1em;
        }
        .brand { font-family: georgia, serif; }
        .brand .first {
            color: #ccc;
            font-style: italic;
        }
        .brand .second {
            color: #fff;
            font-weight: bold;
        }
    </style>
<!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
<!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
<!-- Le fav and touch icons -->
<link rel="shortcut icon" href="../public/images/favicon.ico">
<link rel="apple-touch-icon-precomposed" sizes="144x144" href="file:///D|/New folder/boss/assets/ico/apple-touch-icon-144-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="114x114" href="file:///D|/New folder/boss/assets/ico/apple-touch-icon-114-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="72x72" href="file:///D|/New folder/boss/assets/ico/apple-touch-icon-72-precomposed.png">
<link rel="apple-touch-icon-precomposed" href="file:///D|/New folder/boss/assets/ico/apple-touch-icon-57-precomposed.png">
</head>

<body class="">
<!--<![endif]-->
<!--------------------------------------------------------------header---------------------------------------------------------------------------------->
	<?php  $se=$this->session->userdata; $this->view($header); ?>
<!---------------------------------------------------------------header close-------------------------------------------------------------------------->
<!---------------------------------------------------------------sidebar-------------------------------------------------------------------------------->
	<?php  $this->view($sidebar); ?>
<!---------------------------------------------------------------sidebar close-------------------------------------------------------------------------->

<div class="content">
  <div class="header">
    <div class="stats">
     <!-- <p class="stat"><span class="number">53</span>tickets</p>
      <p class="stat"><span class="number">27</span>tasks</p>
      <p class="stat"><span class="number">15</span>waiting</p>-->
    </div>
    <h1 class="page-title">Dashboard</h1>
  </div>
  <ul class="breadcrumb">
    <li><a href="<?php echo base_url()?>dashboard">Home</a> <span class="divider">/</span></li>
    <li class="active">Dashboard</li>
  </ul>
  <div class="container-fluid">
    <div class="row-fluid"> <!-- remove add image for after login 
      <div class="row-fluid"> <img  src="<?php  echo base_url(); ?>public/u_images/google_add.jpg"> </div>-->
      <div class="block wlcm_text"> <a href="#page-stats" class="block-heading" data-toggle="collapse">Welcome <?php if(isset($msg)){echo $msg;} ?>  </a></div>
    </div>
    <div class="row-fluid">
      <div class="block span9 mrg_lft main_latest_status"> <a href="#tablewidget" class="block-heading" data-toggle="collapse">Latest Stats</a>
        <div id="tablewidget" class="block-body collapse in">
          <div class="latest_status">
            <ul>
              <li><!--`stdClass Object ( [m_id] => 52 [t_id] => 47 [team1_id] => 65 [team2_id] => 69 [match_start_date_time_minute] => 1396690960 1396691302[match_end_date_time_minute] => 1396723740 [player_allowed] => 11 [is_active] => 1 ) -->
                <div class="lft"><strong>You have</strong></div>
                <div class="rgt"><?php $t=time(); if($selectdate->m_id!=NULL){ if($selectdate->match_start_date_time_minute <= $t && $selectdate->match_end_date_time_minute >= $t){echo '<strong>You have earned</strong>';}}?></div>
              </li>
              <li>
                <div class="lft"><?php echo $u_detail->total_account_amount ?></div>
                <div class="rgt"><?php $t=time();if($selectdate->m_id!=NULL){ if($selectdate->match_start_date_time_minute <= $t && $selectdate->match_end_date_time_minute >= $t){echo $u_detail->price;}}?></div>
              <li>
                <div class="lft">Rupees</div>
                <div class="rgt"><?php $t=time();if($selectdate->m_id!=NULL){ if($selectdate->match_start_date_time_minute <=$t && $selectdate->match_end_date_time_minute >= $t){echo 'Rupees';}}?></div>
              </li>
              <li>
                <div class="lft">Create your team and increase your points</div>
                <div class="rgt"><?php $t=time(); if($selectdate->m_id!=NULL){ if($selectdate->match_start_date_time_minute <= $t && $selectdate->match_end_date_time_minute >= $t){echo 'These points will be add to your account after mach fineshed.';}}?></div>
              </li>
            </ul>
          </div>
        
        </div>
      </div>
      <!-- remove add image for after login 
      <div class="add_small_rgt"> <img src="<?php  echo base_url(); ?>public/u_images/add.jpg"/> </div>-->
      <div class="block span9 mrg_lft main_match_rule">
        <div class="block-heading"> <!--<span class="block-icon pull-right"> <a href="#" class="demo-cancel-click" rel="tooltip" title="Click to refresh"><i class="icon-refresh"></i></a> </span>--> <a href="#widget2container" data-toggle="collapse">Match Rules</a> </div>
        <div id="widget2container" class="block-body collapse in">
          <div id="tablewidget" class="block-body collapse in">
            <div class="match_rules">
              <ul>
                <li> 1. When you will sign up you will get dummy Rs. 5000. </li>
                <li> 2. You can purchase player for your team by this amount (Rs 5000).</li>
                <li> 3.  You will earn dummy money with follwing rule :- </li>
                <li> (i) Rs.<?php echo $cofig_detail->rupess_for_run ; ?>. on 1 run made by your purchased player </li>
                <li> (ii) Rs. <?php echo $cofig_detail->rupess_for_wicket; ?> on take one wicket by your purchased player. </li>
                <li> 4. If you will loose your money you can get more money by send invitation to your friends on gmail or other. You will get Rs.<?php echo $cofig_detail->rupee_for_invite; ?> for 1 request sent. </li>
                <li> 5. You can buy player for your team anytime and you can change the player of your team before the match starts. </li>
                <li> 6. If any player which is purchase by you, will not play in match then the player amount will add to your account. </li>
                <li> 7. You can buy your next team only after current match will be finished. If you are playing that match. </li>
                <li> 8. If you have any suggestions, please send that to us. </li>
              </ul>
            </div>
        
          </div>
        </div>
      </div>
    </div>
	
   <!---------------------------------------------------footer--------------------------------------------------->
   	<?php  $this->view($footer); ?>
   <!---------------------------------------------------footer close--------------------------------------------->
  </div>
</div>
</div>
<script src="<?php  echo base_url(); ?>public/lib/bootstrap/js/bootstrap.js"></script>
<script type="text/javascript">
        $("[rel=tooltip]").tooltip();
        $(function() {
            $('.demo-cancel-click').click(function(){return false;});
        });
    </script>
</body>
</html>
