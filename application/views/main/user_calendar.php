<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title><?php if(isset($msg)){echo $msg;} ?></title>
<meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">
<link rel="stylesheet" type="text/css" href="<?php  echo base_url(); ?>public/lib/bootstrap/css/bootstrap.css">
<link rel="stylesheet" type="text/css" href="<?php  echo base_url(); ?>public/stylesheets/theme.css">
<link rel="shortcut icon" type="image/x-icon" href="<?php echo base_url();?>public/f_img/icons.ico" >
<link rel="stylesheet" href="<?php  echo base_url(); ?>public/lib/font-awesome/css/font-awesome.css">
<link rel="stylesheet" type="text/css" href="<?php  echo base_url(); ?>public/stylesheets/style.css">
<script src="<?php  echo base_url(); ?>public/lib/jquery-1.7.2.min.js" type="text/javascript"></script>
<!-- Demo page code -->
<style type="text/css">
        #line-chart {
            height:300px;
            width:800px;
            margin: 0px auto;
            margin-top: 1em;
        }
        .brand { font-family: georgia, serif; }
        .brand .first {
            color: #ccc;
            font-style: italic;
        }
        .brand .second {
            color: #fff;
            font-weight: bold;
        }
    </style>
<!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
<!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
<!-- Le fav and touch icons -->
<link rel="shortcut icon" href="../public/images/favicon.ico">
<link rel="apple-touch-icon-precomposed" sizes="144x144" href="file:///D|/New folder/boss/assets/ico/apple-touch-icon-144-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="114x114" href="file:///D|/New folder/boss/assets/ico/apple-touch-icon-114-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="72x72" href="file:///D|/New folder/boss/assets/ico/apple-touch-icon-72-precomposed.png">
<link rel="apple-touch-icon-precomposed" href="file:///D|/New folder/boss/assets/ico/apple-touch-icon-57-precomposed.png">

</head>

<body class="">
<!--<![endif]-->
<!--------------------------------------------------------------header--------------------------------------------------------------------------------->
	<?php  $se=$this->session->userdata; $this->view($header); ?>
<!---------------------------------------------------------------header close-------------------------------------------------------------------------->
<!---------------------------------------------------------------sidebar------------------------------------------------------------------------------->
	<?php  $this->view($sidebar); ?>
<!---------------------------------------------------------------sidebar close------------------------------------------------------------------------->

<div class="content">
  <div class="header">
    <div class="stats">
     <!-- <p class="stat"><span class="number">53</span>tickets</p>
      <p class="stat"><span class="number">27</span>tasks</p>
      <p class="stat"><span class="number">15</span>waiting</p>-->
    </div>
    <h1 class="page-title">Upcoming events</h1>
  </div>
  <ul class="breadcrumb">
    <li><a href="<?php echo base_url()?>dashboard">Home</a> <span class="divider">/</span></li>
   <li class="active">Upcoming events</li>
        </ul>

        <div class="container-fluid">
            <div class="row-fluid">
       <link href='<?php  echo base_url(); ?>public/fullcalendar/fullcalendar.css' rel='stylesheet' />
<link href='<?php  echo base_url(); ?>public/fullcalendar/fullcalendar.print.css' rel='stylesheet' media='print' />
<script src='<?php  echo base_url(); ?>public/lib/jquery.min.js'></script>
<script src='<?php  echo base_url(); ?>public/lib/jquery-ui.custom.min.js'></script>
<script src='<?php  echo base_url(); ?>public/fullcalendar/fullcalendar.min.js'></script>             

<script>

	$(document).ready(function() {
	
		var date = new Date();
		var d = date.getDate();
		var m = date.getMonth();
		var y = date.getFullYear();
		
		$('#calendar').fullCalendar({
			editable: true,
			events: [<?php foreach($match as $key=>$value ){
			          date_default_timezone_set("Asia/Calcutta");
                $date_start=gmdate('d',$value->match_start_date_time_minute); 
								$date_end=gmdate('d',$value->match_end_date_time_minute);
								$start_time=date("h:i A",$value->match_start_date_time_minute);
								$month_start=gmdate('m',$value->match_start_date_time_minute);
								$month_end=gmdate('m',$value->match_end_date_time_minute);
                
					?>
				{
				
					title: '<?php echo $value->team_name." VS ".$value->team2.", Match will Start at ".$start_time; ?>',
					start: new Date(y, <?php echo $month_start; ?>-1, <?php echo $date_start; ?>),
					end: new Date(y, <?php echo $month_end; ?>-1, <?php echo $date_end; ?>),
					url: '<?php echo base_url().'create_your_team';?>'
				},
				<?php }?>
			]
		});
		
	});


</script>
<style type='text/css'>

	#calendar {
		width: 100%;
		margin: 0 auto;
		}

</style>



<!--<h2>upcoming events</h2>-->
<div id='calendar'></div>
<!-------------------------------------------footer--------------------------------------------------->
   	<?php  $this->view($footer); ?>
   <!---------------------------------------------------footer close--------------------------------------------->
  </div>
</div>
</div>
<script src="<?php  echo base_url(); ?>public/lib/bootstrap/js/bootstrap.js"></script>
<script type="text/javascript">
        $("[rel=tooltip]").tooltip();
        $(function() {
            $('.demo-cancel-click').click(function(){return false;});
        });
    </script>
</body>
</html>
