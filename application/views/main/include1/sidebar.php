<div class="sidebar-nav"> <a id="nav_barr" href="#dashboard-admin_panel" class="nav-header" data-toggle="collapse"><i class="icon-dashboard"></i>Dashboard</a>
  <ul id="dashboard-admin_panel" class="nav nav-list collapse in">
    <li><a href="<?php echo base_url()?>index.php/dashboard">Home</a></li>
    <li ><a href="<?php echo base_url()?>index.php/calendar">Upcomming events</a></li>
    <li ><a href="<?php echo base_url()?>index.php/my_team_scroe_live">My live score board</a></li>
    <li ><a href="<?php echo base_url()?>index.php/live_score_user">live score board</a></li>
  </ul>
  <a href="#accounts-admin_panel" class="nav-header" data-toggle="collapse"><i class="icon-briefcase"></i>Play<span></span></a>
  <ul id="accounts-admin_panel" class="nav nav-list collapse in">
    <li ><a href="<?php echo base_url()?>index.php/create_your_team">Create your team</a></li>
    <li ><a href="<?php echo base_url()?>index.php/update_team">Update team</a></li>
    <li ><a href="<?php echo base_url()?>index.php/match_history">My match history</a></li>
    <li ><a href="<?php echo base_url()?>index.php/invite_friends">Get More Money</a></li>
  </ul>
  <!--           /////////      Add image for after login sidebar       ///////
     <div class="add_small_lft"> <img src="<?php  echo base_url(); ?>public/u_images/add.jpg"/> </div>-->  
</div>
