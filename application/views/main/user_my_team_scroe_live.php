<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title><?php if(isset($msg)){echo $msg;} ?></title>
<meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">
<link rel="shortcut icon" type="image/x-icon" href="<?php  echo base_url(); ?>public/f_img/icons.ico" >
<link rel="stylesheet" type="text/css" href="<?php  echo base_url(); ?>public/lib/bootstrap/css/bootstrap.css">
<link rel="stylesheet" type="text/css" href="<?php  echo base_url(); ?>public/stylesheets/theme.css">
<link rel="stylesheet" href="<?php  echo base_url(); ?>public/lib/font-awesome/css/font-awesome.css">
<link rel="stylesheet" type="text/css" href="<?php  echo base_url(); ?>public/stylesheets/style.css">
<script src="<?php  echo base_url(); ?>public/lib/jquery-1.7.2.min.js" type="text/javascript"></script>
<!-- Demo page code -->
<style type="text/css">
        #line-chart {
            height:300px;
            width:800px;
            margin: 0px auto;
            margin-top: 1em;
        }
        .brand { font-family: georgia, serif; }
        .brand .first {
            color: #ccc;
            font-style: italic;
        }
        .brand .second {
            color: #fff;
            font-weight: bold;
        }
    </style>
<!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
<!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
<!-- Le fav and touch icons -->
<link rel="shortcut icon" href="../public/images/favicon.ico">
<link rel="apple-touch-icon-precomposed" sizes="144x144" href="file:///D|/New folder/boss/assets/ico/apple-touch-icon-144-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="114x114" href="file:///D|/New folder/boss/assets/ico/apple-touch-icon-114-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="72x72" href="file:///D|/New folder/boss/assets/ico/apple-touch-icon-72-precomposed.png">
<link rel="apple-touch-icon-precomposed" href="file:///D|/New folder/boss/assets/ico/apple-touch-icon-57-precomposed.png">
</head>

<body class="">
<!--<![endif]-->
<!--------------------------------------------------------------header--------------------------------------------------------------------------------->
	<?php  $se=$this->session->userdata; $this->view($header); ?>
<!---------------------------------------------------------------header close-------------------------------------------------------------------------->
<!---------------------------------------------------------------sidebar------------------------------------------------------------------------------->
	<?php  $this->view($sidebar); ?>
<!---------------------------------------------------------------sidebar close------------------------------------------------------------------------->

<div class="content">
  <div class="header">
    <div class="stats">
     <!-- <p class="stat"><span class="number">53</span>tickets</p>
      <p class="stat"><span class="number">27</span>tasks</p>
      <p class="stat"><span class="number">15</span>waiting</p>-->
    </div>
    <h1 class="page-title">My score board - Live</h1>
  </div>
  <ul class="breadcrumb">
    <li><a href="<?php echo base_url()?>dashboard">Home</a> <span class="divider">/</span></li>
    <li class="active">My score board - Live</li>
  </ul>
  <div class="container-fluid">
    <div class="row-fluid"> <!-- remove add image for after login 
      <div class="row-fluid"> <img  src="<?php  echo base_url(); ?>public/u_images/google_add.jpg"> </div>-->
    </div>
    <div class="row-fluid">
      <div class="block span12"> <a href="#tablewidget" class="block-heading" data-toggle="collapse">
		
		My score board - Live &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		<?php if(isset($teamname)){

						if($teamname->team1!=NULL){
								
										echo 'Team Name:&nbsp;&nbsp; '.$teamname->team1;
								}
								else{
									
									echo '';
								}
					?> &nbsp;&nbsp;&nbsp;<?php 
							if($teamname->team1_id!=NULL){
	  								
									echo '('.$teamname->team3.' Vs. ';
							}
							if($teamname->team2_id!=NULL){
							
									echo $teamname->team2.')';
							}
							else{
							
									echo '';
							}
				}
				else{
					
						echo "";
				}?></div></a>
        <div id="tablewidget" class="block-body ">
          <div class="my_score_board">
		  <div class="team_list_2">
		  <?php if(!isset($select))
					{echo 'You didn&prime;t purchase any team, Yet? Please purchase it now from  '?><a href="<?php echo base_url()?>create_your_team">HERE</a><?php } else{?>
					 
    <ul>
    <li><strong>S.No.</strong></li>
    <li><strong>Name</strong></li>
    <li><strong>Run</strong></li>
     <li><strong>Wicket</strong></li>
    <li><strong>Rupees</strong></li>
    </ul>    
        <?php 
					 $i=1;
					$price=0;
					
					if(isset($select))
					{
					 foreach($select as $key=>$value)
					 {
					
					 ?>
     <ul>
    <li><?php echo $i; ?></li>
    <li><?php echo $value->player_name; ?></li>
    <li><?php if(isset($value->run)){echo $value->run;}else{ echo '0';} ?></li>
     <li><?php if(isset($value->wicket)){echo $value->wicket;} else{ echo '0';}?></li>
    <li><?php if(isset($value->price)){echo $value->price;} else{ echo '0';}?></li>
	
    </ul>    
        
        <?php $i++; $price=$price+$value->price; }} ?>
 </div>
		<h5 style="text-align:right; padding-right:6%">Total Amount gained by you : Rs.<?php echo $price; ?>/-<?php }?></h5>	
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
   <!---------------------------------------------------footer--------------------------------------------------->
   	<?php  $this->view($footer); ?>
   <!---------------------------------------------------footer close--------------------------------------------->
  </div>
</div>
</div>
<script src="<?php  echo base_url(); ?>public/lib/bootstrap/js/bootstrap.js"></script>
<script type="text/javascript">
        $("[rel=tooltip]").tooltip();
        $(function() {
            $('.demo-cancel-click').click(function(){return false;});
        });
    </script>
</body>
</html>
