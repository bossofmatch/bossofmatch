<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title><?php if(isset($msg)){echo $msg;} ?></title>
<meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">
<link rel="shortcut icon" type="image/x-icon" href="<?php  echo base_url(); ?>public/f_img/icons.ico" >
<link rel="stylesheet" type="text/css" href="<?php  echo base_url(); ?>public/lib/bootstrap/css/bootstrap.css">
<link rel="stylesheet" type="text/css" href="<?php  echo base_url(); ?>public/stylesheets/theme.css">
<link rel="stylesheet" href="<?php  echo base_url(); ?>public/lib/font-awesome/css/font-awesome.css">
<link rel="stylesheet" type="text/css" href="<?php  echo base_url(); ?>public/stylesheets/style.css">
<script src="<?php  echo base_url(); ?>public/lib/jquery-1.7.2.min.js" type="text/javascript"></script>
<!-- Demo page code -->
<style type="text/css">
#line-chart {
height:300px;
width:800px;
margin: 0px auto;
margin-top: 1em;
}
.brand { font-family: georgia, serif; }
.brand .first {
color: #ccc;
font-style: italic;
}
.brand .second {
color: #fff;
font-weight: bold;
}
</style>
<!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
<!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
<!-- Le fav and touch icons -->
<link rel="shortcut icon" href="../public/images/favicon.ico">
<link rel="apple-touch-icon-precomposed" sizes="144x144" href="file:///D|/New folder/boss/assets/ico/apple-touch-icon-144-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="114x114" href="file:///D|/New folder/boss/assets/ico/apple-touch-icon-114-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="72x72" href="file:///D|/New folder/boss/assets/ico/apple-touch-icon-72-precomposed.png">
<link rel="apple-touch-icon-precomposed" href="file:///D|/New folder/boss/assets/ico/apple-touch-icon-57-precomposed.png">
</head>

<body class="">
<!--<![endif]-->
<!--------------------------------------------------------------header---------------------------------------------------------------------------------->
	<?php  $se=$this->session->userdata; $this->view($header); ?>
<!---------------------------------------------------------------header close-------------------------------------------------------------------------->
<!---------------------------------------------------------------sidebar-------------------------------------------------------------------------------->
	<?php $this->view($sidebar); ?>
<!---------------------------------------------------------------sidebar close-------------------------------------------------------------------------->

<div class="content">
  <div class="header">
    <div class="stats">
     <!-- <p class="stat"><span class="number">53</span>tickets</p>
      <p class="stat"><span class="number">27</span>tasks</p>
      <p class="stat"><span class="number">15</span>waiting</p>-->
    </div>
    <h1 class="page-title">My Account</h1>
  </div>
  <ul class="breadcrumb">
    <li><a href="<?php echo base_url()?>dashboard">Home</a> <span class="divider">/</span></li>
    <li class="active">My Account</li>
  </ul>
  <div class="container-fluid">
    <div class="row-fluid">
        <!--<div class="row-fluid"> <img  src="<?php  echo base_url(); ?>public/u_images/google_add.jpg" alt=""> </div>-->
    </div>
    <div class="row-fluid">
      <div class="block span12 bg_clr"> <a href="#tablewidget" class="block-heading" data-toggle="collapse">My Account</a>
        <div id="tablewidget" class="block-body ">

            <?php $imagelocation=$profile->user_image;
            if($imagelocation==""){?>
                  <div class="user_img"><img src="<?php  echo base_url().'public/u_images/user1.jpg'; ?>" alt="user"></div><?php
            }
            else{
                $imagename=explode('public/upload_folder/',$imagelocation);
                $filename = 'public/upload_folder/medium/'.$imagename[1];
                if (file_exists($filename)) {?>

                   <div class="user_img"><img src="<?php  echo base_url().$filename;?>" alt="user"></div>
          <?php  }
            else {?>
                 <div class="user_img"><img src="<?php  echo base_url().$imagelocation; ?>" alt="user"></div>
          <?php  }}?>
            
            <div class="my_account">
                <ul>
                    <li> <div class="my_account_lft"> Name :</div>   <div class="my_account_rgt"><?php echo $profile->u_name;?></div> </li>
                    <li> <div class="my_account_lft">Email Id : </div>   <div class="my_account_rgt"><?php echo $profile->u_email;?></div> </li>
                    <li> <div class="my_account_lft">Mobile No : </div>   <div class="my_account_rgt"><?php echo $profile->mobile_no;?></div> </li>
                    <li> <div class="my_account_lft"> Age :</div>   <div class="my_account_rgt"><?php echo $profile->age;?></div> </li>
                    <li> <div class="my_account_lft"> Sex : </div>   <div class="my_account_rgt"><?php echo $profile->sex;?> </div> </li>
                    <li><div class="my_account_lft">Total money: </div>   <div class="my_account_rgt"> Rs. <?php echo $profile->total_account_amount;?></div> </li>
                </ul>
                <a class="show_user_set" href="<?php echo base_url()?>setting">Account Setting</a>
              </div>
          </div>
      </div>
    </div>
  </div>
</div>
   <!---------------------------------------------------footer---------------------------------------------------->
   	<?php $this->view($footer); ?>
   <!---------------------------------------------------footer close---------------------------------------------->
  </div>
</div>
</div>
<script src="<?php  echo base_url(); ?>public/lib/bootstrap/js/bootstrap.js"></script>
<script type="text/javascript">
        $("[rel=tooltip]").tooltip();
        $(function() {
            $('.demo-cancel-click').click(function(){return false;});
        });
    </script>
</body>
</html>