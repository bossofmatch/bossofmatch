<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title><?php if(isset($msg)){echo $msg;} ?></title>
<meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js"></script>
<link rel="shortcut icon" type="image/x-icon" href="<?php  echo base_url(); ?>public/f_img/icons.ico" >
<link rel="stylesheet" type="text/css" href="<?php  echo base_url(); ?>public/lib/bootstrap/css/bootstrap.css">
<link rel="stylesheet" type="text/css" href="<?php  echo base_url(); ?>public/stylesheets/theme.css">
<link rel="stylesheet" href="<?php  echo base_url(); ?>public/lib/font-awesome/css/font-awesome.css">
<link rel="stylesheet" type="text/css" href="<?php  echo base_url(); ?>public/stylesheets/style.css">
<link href="<?php  echo base_url(); ?>public/stylesheets/style2.css" rel="stylesheet">
<script src="<?php  echo base_url(); ?>public/lib/jquery-1.7.2.min.js" type="text/javascript"></script>
<script>
$(document).ready(function() {
	$('a.login-window').click(function() {
		$.ajax({
        type: 'POST',
		// data : parama,
        url: "<?php echo base_url()?>winners/my_rank",
        success : function(data){
		 document.getElementById("login-box").innerHTML=data;
		},
    }); 
		// Getting the variable's value from a link 
		var loginBox = $(this).attr('href');
//Fade in the Popup and add close button
		$(loginBox).fadeIn(300);
	$(loginBox).css({ 
			'margin-top' : -220,
			'margin-left' : -225
		});
		// Add the mask to body
		$('body').append('<div id="mask"></div>');
		$('#mask').fadeIn(300);
		return false;
	});
	// When clicking on the button close or the mask layer the popup closed
	$('a.close, #mask').live('click', function() { 
	  $('#mask , .login-popup').fadeOut(300 , function() {
		$('#mask').remove();  
	}); 
	return false;
	});
});
</script>
<!-- Demo page code -->
<style type="text/css">
        #line-chart {
            height:300px;
            width:800px;
            margin: 0px auto;
            margin-top: 1em;
        }
        .brand { font-family: georgia, serif; }
        .brand .first {
            color: #ccc;
            font-style: italic;
        }
        .brand .second {
            color: #fff;
            font-weight: bold;
        }
    </style>
<!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
<!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
<!-- Le fav and touch icons -->
<link rel="shortcut icon" href="../public/images/favicon.ico">
<link rel="apple-touch-icon-precomposed" sizes="144x144" href="file:///D|/New folder/boss/assets/ico/apple-touch-icon-144-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="114x114" href="file:///D|/New folder/boss/assets/ico/apple-touch-icon-114-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="72x72" href="file:///D|/New folder/boss/assets/ico/apple-touch-icon-72-precomposed.png">
<link rel="apple-touch-icon-precomposed" href="file:///D|/New folder/boss/assets/ico/apple-touch-icon-57-precomposed.png">
</head>

<body class="">
<div id="login-box" class="login-popup" ></div>
<!--<![endif]-->
<!--------------------------------------------------------------header--------------------------------------------------------------------------------->
	<?php  $se=$this->session->userdata; $this->view($header); ?>
<!---------------------------------------------------------------header close-------------------------------------------------------------------------->
<!---------------------------------------------------------------sidebar------------------------------------------------------------------------------->
	<?php  $this->view($sidebar); ?>
<!---------------------------------------------------------------sidebar close------------------------------------------------------------------------->

<div class="content">
  <div class="header">
    <div class="stats">
     <!-- <p class="stat"><span class="number">53</span>tickets</p>
      <p class="stat"><span class="number">27</span>tasks</p>
      <p class="stat"><span class="number">15</span>waiting</p>-->
    </div>
    <h1 class="page-title">Match History</h1>
  </div>
  <ul class="breadcrumb">
    <li><a href="<?php echo base_url()?>dashboard">Home</a> <span class="divider">/</span></li>
    <li class="active">Match History</li>
  </ul>
  <div class="container-fluid">
    <div class="row-fluid"><!--     remove  image
      <div class="row-fluid"> <img  src="<?php  echo base_url(); ?>public/u_images/google_add.jpg"> </div>-->
    </div>
    <div class="row-fluid">
      <div class="block span12 bg_clr" align="center"> <a href="#tablewidget" class="block-heading" data-toggle="collapse">MY match History <?php
	  		 if(isset($tournamentname)){
			 
			 		if($tournamentname->t_short_name==$tournamentname->t_full_name){
					
						echo $tournamentname->t_short_name;
					}
					else{
						
						 echo $tournamentname->t_short_name.' ';
						 echo $tournamentname->t_full_name;
					}
				}?></a><a href="#login-box" style="margin:60px;" class="login-window"><img src="<?php echo base_url().'public/f_img/button_viewall.png'?>"></a>
        <div id="tablewidget" class="block-body ">
          <div class="my_score_board">
		  <div class="team_list_2">
    <ul>
    <li><strong>S.No.</strong></li>
    <li><strong>Team</strong></li>
    <li><strong>Date</strong></li>
     <li><strong>You gained</strong></li>
    <li><strong>Batch name</strong></li>
    </ul>    
        <?php 
					$price=0;
					$i=1;
					if($match!="")
					{
					 foreach($match as $key=>$value)
					 {
					
					 ?>
     <ul>
    <li><?php echo $i; ?></li>
    <li><?php echo $value->team_name." VS ".$value->team2; ?></li>
    <li><?php echo $date = date('d/m/Y', $value->match_end_date);?></li>
     <li><?php echo $value->total_earn; ?></li>
    <li><?php if($value->batches==1){
                        echo 'Boss of Match';
              }
              else if($value->batches==2){
                        echo 'Guru of Match';
              }else if($value->batches==3){
                        echo 'Man of Match';
              }else if($value->batches==4){
                        echo 'Attraction of Match';
              }else{echo 'Captain of Match';
				}?></li>
	
    </ul>    
        
        <?php $i++; $price=$price+$value->total_earn; }} ?>
 </div>
		<h5 style="text-align:right; padding-right:6%">Total amount gained by you in <?php if(isset($tournamentname)){ echo $tournamentname->t_short_name; }?> Tournament. <?php echo $price; ?>/-</h5>	
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
   <!---------------------------------------------------footer--------------------------------------------------->
   	<?php  $this->view($footer); ?>
   <!---------------------------------------------------footer close--------------------------------------------->
  </div>
</div>
</div>
<script src="<?php  echo base_url(); ?>public/lib/bootstrap/js/bootstrap.js"></script>
<script type="text/javascript">
        $("[rel=tooltip]").tooltip();
        $(function() {
            $('.demo-cancel-click').click(function(){return false;});
        });
    </script>
</body>
</html>
