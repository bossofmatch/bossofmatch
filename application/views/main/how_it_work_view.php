<!DOCTYPE html>
<html lang="en">
<head>
<title>How it Works-BossOfMatch</title>
<meta name="description" content="Find the way how it works." />
<meta name="keywords" content="How it works" />

<meta charset="utf-8">
<meta name="google-site-verification" content="P_ySFK7sK6kN2vAAEwJGGZsaOuQiQCfjsVNta1mzHAc" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<meta name="author" content="">

<link rel="shortcut icon" type="image/x-icon" href="<?php  echo base_url(); ?>public/f_img/icons.ico" >
<!--<title>BossOfMatch | Boss of Match | Play with IPL | Create my team | cricket</title>-->
<!-- Bootstrap core CSS -->
<link href="<?php  echo base_url(); ?>public/css/f_css/bootstrap.min.css" rel="stylesheet">
<!-- Custom styles for this template -->
<link href="<?php  echo base_url(); ?>public/css/f_css/justified-nav.css" rel="stylesheet">
<link href="<?php  echo base_url(); ?>public/css/f_css/style.css?bof09052014" rel="stylesheet">
<link href="<?php  echo base_url(); ?>public/css/f_css/carousel.css" rel="stylesheet">
<!-- Just for debugging purposes. Don't actually copy this line! -->
<!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
<link rel="stylesheet" href="<?php  echo base_url(); ?>public/css/f_css/media.css" type="text/css"   >\
<!-- <script>
function ipl_live()
{
 $.ajax({
       url: "<?php echo base_url()?>index.php/welcome/ipl_rss",
       success : function(data){
	
	  document.getElementById("live_news").innerHTML=data;
   
	            },
    }); 

}
</script> -->
</head>
<!-- <body onLoad="ipl_live();"> -->
<body>
<div class="container container_clr">
  <div class="masthead mrg_top">
    <!--header-->
	<?php $this->view($header); ?>
	<!--header close-->
 
  <!-- Jumbotron -->
 	<?php  // $this->view($top); ?>
    <!-- Example row of columns -->
   <div class="row howIt_w">
      <div class="col-lg-9 col_mrg_ftsk_3">
        <div class="how_it_work">
          <p>How it work
          <p> 
        </div>
        <div class="how_it_work_fild">
          <!--<p>This season of cricket going to outburst your passion for your players and for the cricket. Let the heat and passion of cricket enthral. If cricket is emotion for you then BossOfMatch is going to hold you spellbound with this game like you breath.
<p>Following are simple steps to take your enthusiasm and spirit fly high with the cricket:</p></p>-->
<p>You can create a team before the start of a match. You can create only one team for each match. Each team of a match will consist of all the players selected for that tournament for that team. You can even update your team up till 5 minutes after start of the match. However, once the5 minutes past to the match will be over then the user cannot update the team. Enthuons reserve the rights to accept or refuse the team names.<p>
Let your cricketing legend take the guard and follow these simple steps to be boss of match:

          <div class="how_it_work1">
            <ul class="work_stp">
              <li>Step 1
                <p>Sign up, if you are a new user.</p>
              </li>
              <li>Step 2
                <p>Login with your registered e-mail Id and password. Dashboard page appears.</p>
              </li>
             
              <li>Step 3
                <p>Click the <a href="<?php echo base_url();?>create_your_team">Create your team</a> option under the Play panel. The Create your team page appears</p>
              </li>
			  <li>Step 4
                <p>Select a tournament from the Select Tournament drop-down list. The Select your match drop-down list appears.</p>
              </li>
			  <li>Step 5
                <p>Select a team from the <strong>Select your match</strong> drop-down list.</p>
              </li>
			  <li>Step 6
                <p>Enter team name in the <strong>Your Team name</strong> text box.</p>
              </li>
              <li>Step 7
                <p>Click the <strong>Submit</strong> button. A list of players from appears.</p>
              </li>
              <li>Step 8
                <p>Click the <strong>Add</strong> link under the Action column from the table to buy players for your team.</p>
              </li>
              <li>Step 9
                <p>Click the <strong>Submit</strong> button after adding 11 players to your team. A confirmation message appears.</p>
              </li>
              <li>Step 10
                <p>Click the <strong>OK</strong> button to confirm the team.</p>
              </li>
              <li>Congratulations, now you are the boss of your team and ready to be the BOSS OF MATCH.
              </li>
			  
            </ul>
          </div>
        </div>
        <!--<div class="col-lg-11 margn_lft_vdo_2 ">
          <div class="video-container">
            <iframe src="http://www.youtube.com/embed/O0Wsu1Q8No4" frameborder="0"  allowfullscreen></iframe>
          </div>
        </div>-->
      </div>
      <div class="col-lg-3 add_rgt"><!-- <img src="<?php  echo base_url(); ?>public/f_img/add.jpg"/>--> </div>
    
    </div>
   <div class="row mrg_row">
        
        <div class="col-lg-12 ipl_mrg_rgt "> <!--<img src="<?php  echo base_url(); ?>public/f_img/google_add.jpg" class="add_bottom"/>--> </div>
      </div>
  </div>
    <!-- Site footer -->
  </div>
</div>
<?php $this->view($footer); ?>
<!-- /container -->
<!-- Bootstrap core JavaScript
    ================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
<script src="<?php  echo base_url(); ?>public/js/f_js/bootstrap.min.js"></script>
<script src="<?php  echo base_url(); ?>public/js/f_js/docs.min.js"></script>
</body>
</html>
