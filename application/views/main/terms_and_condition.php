<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">

<meta name="robots" content="index, follow" />
<meta name="googlebot" content="index, follow" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="" />
<meta name="keywords" content="" />
<meta name="author" content="">
<meta name="google-site-verification" content="P_ySFK7sK6kN2vAAEwJGGZsaOuQiQCfjsVNta1mzHAc" />
<link rel="shortcut icon" type="image/x-icon" href="<?php  echo base_url(); ?>public/f_img/icons.ico" >
<!--<title>Play IPL T20 Matches | Play with Your Favorite Players | Create Your Team-BossOfMatch</title>-->
<title>Terms and Condition-BossOfMatch </title>
<!-- Bootstrap core CSS -->

<link href="<?php  echo base_url(); ?>public/css/f_css/bootstrap.min.css" rel="stylesheet">
<!-- Custom styles for this template -->
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js"></script>
<link href="<?php  echo base_url(); ?>public/css/f_css/justified-nav.css" rel="stylesheet">
<link href="<?php  echo base_url(); ?>public/css/f_css/style.css" rel="stylesheet">
<link href="<?php  echo base_url(); ?>public/css/f_css/style1.css" rel="stylesheet">
<link href="<?php  echo base_url(); ?>public/css/f_css/carousel.css" rel="stylesheet">
<!-- Just for debugging purposes. Don't actually copy this line! -->
<!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
<link rel="stylesheet" href="<?php  echo base_url(); ?>public/css/f_css/media.css" type="text/css"   >

</head>
<body>
	<div id="login-box" class="login-popup" >
    </div>
		<div class="container container_clr">
			<div class="masthead mrg_top">
  					<!--header-->
  						<?php $this->view($header); ?>
  					<!--header close-->
  					<!-- Jumbotron -->
  					<div class="page_background" style="width: 100%;">
   							 <div class="row ">
      						<div class="col-lg-8 col_mrg"> </div>
     						 <div class="col-lg-4 col_mrg_2">
        					<div class="row"> </div>
      						</div>
    				</div>
    				<!-- Example row of columns -->
    				<div class="row">
      					<div class="col-lg-11 padng_lft2">
       		
					   		<!--     <img src="<?php  echo base_url(); ?>public/f_img/google_add.jpg"/> --></div>
     								
		
		
      <div style="float: left;font-size: 16px; height: auto;margin: 21px 0 0 117px; padding: 0; width: 80%; font">
<div id="tccc"><strong>Terms & Conditions</strong></div> <br><br>

<p class="ptcc">
By using bossofmatch.com Website you are deemed to have read and agreed to the following terms and conditions:
The following terminology applies to these Terms and Conditions, Privacy Statement, and Disclaimer Notice and any or all agreements: “Client”, “You”, and “Your” refers to the person accessing this Website and accepting the company’s terms and conditions. “The Company”, “Ourselves”, “We”, and “Us” refer to BOSSOFMATCH. “Party”, “Parties” or “Us” are refers to both the client and ourselves, or either the client or ourselves. Any use of the above terminology or other words in the singular, plural, capitalization and/or he/she or they, are taken as interchangeable and therefore as referring to same.
</p> <br>

<strong>* Who can participate: </strong>
<p class="ptcc">
Any person with a valid e-mail Id and mobile number can register with us and play the game. The prizes will be available for Indian users only. Employees of Enthuons, their families, game sponsors are excluded from winning a prize.</p><br>

<strong>* Privacy Statement:</strong>
<p class="ptcc">
 We record your registration details in our database and send a confirmation e-mail to the registered user. Only our authorized employees will access the records of the registered members. We constantly review our system and data to ensure the best possible service provided to our customers. Parliament has created specific offences for unauthorized access against computer systems and data. We will investigate any such actions with a view to prosecuting and/or taking civil proceedings to recover damages against those responsible.</p><br>
<strong>* Confidentiality:</strong>

<p class="ptcc">

However, client records are regarded as confidential and therefore will not be divulge to any third party, other than (our manufacturer/supplier(s) and) if legally required to do so to the appropriate authorities. Clients have the right to request sight of, and copies of any and all client records we keep, on the provision that we are given reasonable notice of such a request. Clients are requested to retain copies of any literature issued in relation to the provision of our services. Wherever appropriate we shall issue Client’s with appropriate written information, handouts or copies of records as part of an agreed contract, for the benefit of both parties.
We will not sell, share, or rent your personal information to any third party or use your e-mail address for unsolicited mail. Any e-mail sent by this company will only be in connection with the provision of agreed services and products.
 </p><br>
<strong>* How to Score:</strong>
 <p class="ptcc"> 
If  a player hits a single run in real match, Rs. 30 (fake money) will be added to user’s registered account. If a baller takes a wicket, it will be considered equivalent to 20 runs, that is, Rs. 600 (fake money) added to user’s registered account. Highest scoring team will be the winner and receive 1st prize. In case of more than one team earns same amount of money, then the one having the top entry in our system  will be the winner(s). This is an automatic entry and we do not have any control over it.
</p><br>
<strong>* Prizes:</strong><p class="ptcc">
User(s) winning highest amount of money will win Boss of Match title and will get Rs. 50 recharge on the registered mobile number.
User with 2nd position will win Guru of Match title and will get Rs. 10 recharge on the registered mobile number.
User with 3rd position will win Man of Match title and will get Rs. 10 recharge on the registered mobile number.
Users with 4th position will win Attraction of Match title and will get Rs. 10 recharge on the registered mobile number.
Winner’s uploaded photos will be posted on our wall and will be shared on social media sites, such as, Facebook, Twitter, LinkedIn, and Google+.
</p>
<ol><li>Prize Notification</li>
The Boss of Match title’s winner  will be receiving a  confirmation message on his/her registered e-mail Id. If you reply to the e-mail within 24 hours, a recharge request will be initiated for the registered mobile number.
In case, BOSSOFMATCH has not received your reply within 24 hours, the recharge request will be cancelled.
If easy recharge of a won recharge amount is not available with the service provider of registered mobile number then we will not able to recharge that mobile number.
</ol>
<br>
<strong>* Disclaimer:</strong><p class="ptcc">
The information on the bossofmatch.com is provided on an “as is” basis. To the fullest extent permitted by law,<a href="http://www.bossofmatch.com/login">bossofmatch.com:</a><ul><li>
Excludes all representations and warranties relating to this Website and its contents or which is or may be provided by any affiliate or any other third party, including in relation to any inaccuracies or omissions in this website and/or the company’s literature; and</li><li>
Excludes all liability or damages arising out of or in connection with your use of this Website. This includes without limitation, direct loss, loss of business or profits (whether or not the loss of such profits was foreseeable, arose in the normal course of things or you have advised this company of the possibility of such potential loss), damage caused to your computer, computer software, systems and programs and the data thereon or any other direct or indirect consequential and incidental damages. </li><li>
Enthuons reserve the rights to cancel the registration/winners without any prior confirmation.</li><li>
Enthuons have right to change the terms and conditions any time without prior information.</li></ul>
</p><br>
<strong>* Links:</strong><p class="ptcc">
Links for external Websites are operated and controlled by third parties and their inclusion does not imply any endorsement or approval by the bossofmatch.com of the materials on such websites. This company will not accept any responsibility for any loss or damage in whatever manner, however caused, resulting from your disclosure to third parties of personal information.
</p><br>
<strong>* Copyright: </strong><p class="ptcc">
2.8.  
Copyright and other relevant intellectual property rights exist on all text relating to the company’s services and the full content of this Website.
</p><br>										

        </div>
      </div>

      <!--<div class="row mrg_row">
        <div class="col-lg-12 ipl_mrg_rgt "> <img src="<?php  echo base_url(); ?>public/f_img/google_add.jpg" class="add_bottom" /> </div>
      </div>-->
    </div>
    <!-- Site footer -->
  </div>
</div>
<?php $this->view($footer); ?>
<!-- /container -->
<!-- Bootstrap core JavaScript
    ================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
<script src="<?php  echo base_url(); ?>public/js/f_js/bootstrap.min.js"></script>
<script src="<?php  echo base_url(); ?>public/js/f_js/docs.min.js"></script>
</body>
</html>
