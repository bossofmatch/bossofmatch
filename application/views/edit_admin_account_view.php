<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Edit user</title>
<meta name="keywords" content="">
<meta name="description" content="">
<link rel="<?php  echo base_url(); ?>public/shortcut icon" href="http://invoice.enthuons.com/images/favicon.ico" />
<link href="<?php  echo base_url(); ?>public/css/style.css" rel="stylesheet" type="text/css" media="handheld, screen" />
<link href="<?php  echo base_url(); ?>public/css/jquery-ui.css" rel="stylesheet" type="text/css" />
<link href="<?php  echo base_url(); ?>public/css/menu.css" rel="stylesheet" type="text/css" />
<link href="<?php  echo base_url(); ?>public/css/style1.css" rel="stylesheet" type="text/css"  />
<!--<link rel="stylesheet" href="http://invoice.enthuons.com/js/menu/lavalamp_test.css" type="text/css" media="screen" />-->
<!--[if IE]>  
<script src="http://invoice.enthuons.com/js/html5.js"></script>
<![endif]-->
<!--[if lte IE 6]>
<script type="text/javascript" src="http://invoice.enthuons.com/js/pngfix.js"></script>
<script type="text/javascript" src="http://invoice.enthuons.com/js/ie6.js"></script>
<link rel="stylesheet" href="http://invoice.enthuons.com/css/ie6.css" type="text/css" />
<![endif]-->
<!-- Menu Bar Start -->
<script type="text/javascript" src="<?php  echo base_url(); ?>public/js/jquery.js"></script>
<script type="text/javascript" src="<?php  echo base_url(); ?>public/js/jquery.validate.js"></script>
<script type="text/javascript" src="<?php  echo base_url(); ?>public/js/jquery-ui.js"></script>
<script type="text/javascript" src="<?php  echo base_url(); ?>public/js/maskedinput.js"></script>
<script type="text/javascript" src="<?php  echo base_url(); ?>public/js/menu.js"></script>
<style type="text/css">
.errormessage {display:none; color:#FF0000 !important; background-color:#CCCCCC !important; width:335px; padding:2px;}
#errormess { color:#FF0000; padding:5px; font-size:14px; font-weight:bold;}
#sucessmess {background-color:#CCCCCC; color:#00FF00; padding:5px; font-size:14px; font-weight:bold;}
</style>
<!--<script type="text/javascript" src="http://invoice.enthuons.com/js/banner/jquery.min.js"></script>-->
<!--<script type="text/javascript" src="http://invoice.enthuons.com/js/menu/jquery.lavalamp.js"></script>
<script type="text/javascript" src="http://invoice.enthuons.com/js/menu/jquery.easing.min.js"></script>
<script type="text/javascript" src="http://invoice.enthuons.com/js/menu/jquery.lavalamp.min.js"></script>-->
<!--<script type="text/javascript"> 
    $(function() {
        $("#1, #2, #3").lavaLamp({
            fx: "backout", 
            speed: 900,
            click: function(event, menuItem) {
                return true;
            }
        });
    });
    //jQuery.noConflict();
</script>-->
<script>
function validateForm(z,user_name)
{

var password=document.forms["myForm"]["pass"].value;
var npassword=document.forms["myForm"]["npass"].value;
var cpassword=document.forms["myForm"]["cpass"].value;
var parama = {'password' : password,'npassword' : npassword,'cpassword' : cpassword,'user_name':user_name};

 $.ajax({
        type: 'POST',
		 data : parama,
        url: "<?php echo base_url()?>manage_admin_member/update_pass",
        success : function(mg){
		

		 document.getElementById('error').innerHTML=mg;
		},
    }); 
}
</script>




<!-- Menu Bar End -->
<script type="text/javascript"> 
    function dynamic_change(){
        var sel = document.getElementById("select_lan").value;
        document.getElementById("m_lan").innerHTML = sel;
    }
</script>
<!--Text Field Start-->
<script type="text/javascript"> 
 
    function clearText(thefield){
        if (thefield.defaultValue==thefield.value)
            thefield.value = ""
    } 
 
    function fillText(thefield){
        if (thefield.value=="")
            thefield.value = thefield.defaultValue;
    }
</script>

<script type="text/javascript">        
    ddsmoothmenu.init({
        mainmenuid: "smoothmenu1", //menu DIV id
        orientation: 'h', //Horizontal or vertical menu: Set to "h" or "v"
        classname: 'ddsmoothmenu', //class added to menu's outer DIV
        //customtheme: ["#1c5a80", "#18374a"],
        contentsource: "markup" //"markup" or ["container_id", "path_to_menu_file"]
    })    
    ddsmoothmenu.init({
        mainmenuid: "smoothmenu2", //Menu DIV id
        orientation: 'v', //Horizontal or vertical menu: Set to "h" or "v"
        classname: 'ddsmoothmenu-v', //class added to menu's outer DIV
        //customtheme: ["#804000", "#482400"],
        contentsource: "markup" //"markup" or ["container_id", "path_to_menu_file"]
    })        
    </script>
	<script type="text/javascript">
$(document).ready(function() {
	$('a.login-window').click(function() {
		
		// Getting the variable's value from a link 
		var loginBox = $(this).attr('href');

		//Fade in the Popup and add close button
		$(loginBox).fadeIn(300);
		
		//Set the center alignment padding + border
		var popMargTop = ($(loginBox).height() + 24) / 2; 
		var popMargLeft = ($(loginBox).width() + 24) / 2; 
		
		$(loginBox).css({ 
			'margin-top' : -popMargTop,
			'margin-left' : -popMargLeft
		});
		
		// Add the mask to body
		$('body').append('<div id="mask"></div>');
		$('#mask').fadeIn(300);
		
		return false;
	});
	
	// When clicking on the button close or the mask layer the popup closed
	$('a.close, #mask').live('click', function() { 
	  $('#mask , .login-popup').fadeOut(300 , function() {
		$('#mask').remove();  
	}); 
	return false;
	});
});
</script>
<!--Text Field End-->
</head>
<body onload="<?php echo base_url()?>manage_user/add_user">
<!-- Navigation Section Start -->

<?php $this->view($header); ?>
<!-- Navigation Section End -->
<!-- Banner Section Start -->

<?php $this->view($banner); ?>
<!-- Banner Section End -->
<div class="cl"></div>
<!-- Mid Section Start -->
<!-- Mid Section Start -->
<section id="mid_pan">
  <div class="mid-wrapper">
    <!--Content Start-->
    <div id="mid-cont">
      <div class="mid-ctr">
        <div class="mid-lt">
          <div class="mid-rt">
            <div class="main-table">
              <!--Head-->
              <div class="head" style="color:#000000">Edit User</div>
              <!--Head end-->
              <div class="col-wt">
                <div class="form-add">
                  <script type="text/javascript">

$(document).ready(function(){

$("#frm").validate();
$(".checklimit").keypress(function(){

var str=$(this).val();
if(str.length >49)
{
$("#"+$(this).attr('id')+'_er').html('<font color="red">characher limit exceed.</font>');
}
else{
$("#"+$(this).attr('id')+'_er').html('');
}
//alert($(this).val());
});
var newemailNO = 0;
var emailNO = 0;

$("#button1").click(function(){
emailNO = document.getElementById('emailNO').value;
newemailNO = parseInt(emailNO)+1; 
$(".xyz").append('<input type="text" name="email_'+newemailNO+'" id="email_'+newemailNO+'" size="32" class="email" value="" style="margin-top:10px;" />');
$(".xyz").append('<input type="button" class="btndelete" name="delete_'+newemailNO+'" id="delete_'+newemailNO+'" align="right" value="Delete&nbsp;" onclick="hidefun('+newemailNO+');" /><div class="cl"></div>');
document.getElementById('emailNO').value = newemailNO;
});
$('#name').blur(function(){
var a = $("#name").val();
if(a!='')
{
$.post("http://invoice.enthuons.com/client/checkname", {	Name: $('#name').val()}, function(response){ $('#Loading').html(response); });
}
else
{
$('#Loading').html('');
}
return false;
});

});
function hidefun(value)
{

if(document.getElementById('email_'+value))
{ 
document.getElementById('email_'+value).value='';
document.getElementById('email_'+value).style.display = 'none';
document.getElementById('delete_'+value).style.display = 'none';
}
}

</script>
                    <form action="<?php echo base_url()?>manage_admin_member/Edit_user"  method="post" enctype="multipart/form-data" id="frm">
                    <div class="rowone">
                    <input type="hidden" name="user_id" value="<?php echo $ms->a_id?>">
                      <label>Name * </label>
                      <input id="user_name" class="required character" type="text" value="<?php echo $ms->a_name;?>" name="user_name"/>
                    </div>
                    <div class="rowone">
                      <label>Email id * </label>
                      <div id="xyz" class="xyz">
                       <input id="user_email" class="required email" type="text" value="<?php echo $ms->a_email;?>" name="user_email" readonly="true" />
                        <div class="cl"></div>
                    
                      </div>
                    </div>
                    
                    <div class="rowone">
                      <label>Role *  </label>

                     <select id="abc" class="required character"  name="user_roll" >
                    	
					 <?php 
					
					 foreach($u_roll as $key=>$value)
					 {
					 ?>
					  <option value="<?php echo $value->r_id;?>"<?php if($value->r_id==$ms->role){?> selected="selected"<?php }?>><?php echo $value->role;?></option>
					<?php } ?>
                    			
                                
                             </select>
                      <span id="address1_er"></span> </div>
                    <div class="rowone">
                      <label>User Password * </label>
                      <input id="password" class="required character" type="password" maxlength="25" size="32" value="***********" name="password" readonly="true">
					           &nbsp;&nbsp;
							   <div class="btn-sign">
				<a href="#login-box" class="login-window"><img src="<?php echo base_url()?>public/images/change_password.png" /></a>
        	</div>
          <span id="address2_er"></span> </div>
          
                    <div class="add-right-mn">
                      <div class="total" style="padding-left: 268px;">
                        <div class="cl"></div><br />
                       
                        <input name="Submit"  id="BtnSubmit" type="submit" class="btn_save" value="Save" />
							<a href="<?php echo base_url()?>admin_panel/manage_admin_account"><img src="<?php echo base_url()?>public/images/cancel.png" /></a>
						<span id="errormess">
						<?php
				if(isset($mss))
				{
				echo "$mss";
				}
				?>
						
						
						</span>
                      </div>
					   
                    </div>
                  </form>
                </div>
              </div>
              <div class="cl"></div>
            </div>
          </div>
        </div>
      </div>
    </div>
	<!--new change password				-->		   
							   
							   
						   
							   
							   
					<div class="container">
	<div id="content">
    
		<!--<div class="post">
    	<h2>Your Login or Sign In Box</h2>-->
        	
		</div>
        
        <div id="login-box" class="login-popup"  display: block; style="height: 204px; margin-left: -176px; margin-top: -178.5px; width: 251px; text-align:center">
        <a href="#" class="close"><img src="file:///C|/xampp/htdocs/bom/application/views/POp Up/login-box-modal-dialog-window/close_pop.png" class="btn_close" title="Close Window" alt="Close" /></a>
          <form method="post" class="signin" name="myForm">
                <fieldset class="textbox">
            	  <label class="password">
                <span style="color:#FFFFFF">Old Password</span>
                 <input id="pass" name="pass" value="" type="password" placeholder="Password">
				 
                </label>
				<label class="password">
                <span style="color:#FFFFFF">New Password</span>
                 <input id="npass" name="npass" value="" type="password" placeholder="Password">
				 
                </label>
                
                <label class="password">
                <span style="color:#FFFFFF">Confirm Password</span>
                <input id="cpass" name="cpass" value="" type="password" placeholder="Password">
                </label>
                 <span  id="error"></span>
         <input name="Submit1"  id="BtnSubmit1" type="button" onClick="return validateForm('this.value','<?php echo $ms->a_email;?>')" class="btn_save" value="Save" style=" background:url("../images/save.png") no-repeat scroll 0 0 rgba(0, 0, 0, 0)" />
               		
			
				
               
                
                </fieldset>
          </form>
		</div>
    
    </div>
</div>		   

							   
							   
			<!--			end of new change passord	   -->
    <!--Content Start-->
    <div class="cl"></div>
  </div>
</section>
<!-- Mid Section End -->
<?php $this->view($footer); ?>
<!-- Footer Section End -->
</body>
</html>
