
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Rupee Management</title>
<meta name="keywords" content="">
<meta name="description" content="">
<link rel="<?php  echo base_url(); ?>public/shortcut icon" href="http://invoice.enthuons.com/images/favicon.ico" />
<link href="<?php  echo base_url(); ?>public/css/style.css" rel="stylesheet" type="text/css" media="handheld, screen" />
<link href="<?php  echo base_url(); ?>public/css/jquery-ui.css" rel="stylesheet" type="text/css" />
<link href="<?php  echo base_url(); ?>public/css/admin_panel.css" rel="stylesheet" type="text/css" />
<!--<link rel="stylesheet" href="http://invoice.enthuons.com/js/admin_panel/lavalamp_test.css" type="text/css" media="screen" />-->
<!--[if IE]>  
<script src="http://invoice.enthuons.com/js/html5.js"></script>
<![endif]-->
<!--[if lte IE 6]>
<script type="text/javascript" src="http://invoice.enthuons.com/js/pngfix.js"></script>
<script type="text/javascript" src="http://invoice.enthuons.com/js/ie6.js"></script>
<link rel="stylesheet" href="http://invoice.enthuons.com/css/ie6.css" type="text/css" />
<![endif]-->
<!-- Menu Bar Start -->
<script type="text/javascript" src="<?php  echo base_url(); ?>public/js/jquery.js"></script>
<script type="text/javascript" src="<?php  echo base_url(); ?>public/js/jquery.validate.js"></script>
<script type="text/javascript" src="<?php  echo base_url(); ?>public/js/jquery-ui.js"></script>
<script type="text/javascript" src="<?php  echo base_url(); ?>public/js/maskedinput.js"></script>
<script type="text/javascript" src="<?php  echo base_url(); ?>public/js/admin_panel.js"></script>
<style type="text/css">
.errormessage {display:none; color:#FF0000 !important; background-color:#CCCCCC !important; width:335px; padding:2px;}
#errormess { color:#FF0000; padding:5px; font-size:14px; font-weight:bold;}
#sucessmess {background-color:#CCCCCC; color:#00FF00; padding:5px; font-size:14px; font-weight:bold;}
</style>
<!--<script type="text/javascript" src="http://invoice.enthuons.com/js/banner/jquery.min.js"></script>-->
<!--<script type="text/javascript" src="http://invoice.enthuons.com/js/admin_panel/jquery.lavalamp.js"></script>
<script type="text/javascript" src="http://invoice.enthuons.com/js/admin_panel/jquery.easing.min.js"></script>
<script type="text/javascript" src="http://invoice.enthuons.com/js/admin_panel/jquery.lavalamp.min.js"></script>-->
<!--<script type="text/javascript"> 
    $(function() {
        $("#1, #2, #3").lavaLamp({
            fx: "backout", 
            speed: 900,
            click: function(event, admin_panelItem) {
                return true;
            }
        });
    });
    //jQuery.noConflict();
</script>-->
<!-- Menu Bar End -->
<script type="text/javascript"> 
    function dynamic_change(){
        var sel = document.getElementById("select_lan").value;
        document.getElementById("m_lan").innerHTML = sel;
    }
</script>
<!--Text Field Start-->
<script type="text/javascript"> 
 
    function clearText(thefield){
        if (thefield.defaultValue==thefield.value)
            thefield.value = ""
    } 
 
    function fillText(thefield){
        if (thefield.value=="")
            thefield.value = thefield.defaultValue;
    }
</script>

<script type="text/javascript">        
    ddsmoothadmin_panel.init({
        mainadmin_panelid: "smoothadmin_panel1", //admin_panel DIV id
        orientation: 'h', //Horizontal or vertical admin_panel: Set to "h" or "v"
        classname: 'ddsmoothadmin_panel', //class added to admin_panel's outer DIV
        //customtheme: ["#1c5a80", "#18374a"],
        contentsource: "markup" //"markup" or ["container_id", "path_to_admin_panel_file"]
    })    
    ddsmoothadmin_panel.init({
        mainadmin_panelid: "smoothadmin_panel2", //Menu DIV id
        orientation: 'v', //Horizontal or vertical admin_panel: Set to "h" or "v"
        classname: 'ddsmoothadmin_panel-v', //class added to admin_panel's outer DIV
        //customtheme: ["#804000", "#482400"],
        contentsource: "markup" //"markup" or ["container_id", "path_to_admin_panel_file"]
    })        
    </script>
<!--Text Field End-->
</head>
<body onload="<?php echo base_url()?>manage_user_member/add_member_user">
<!-- Navigation Section Start -->

<?php $this->view($header); ?>
<!-- Navigation Section End -->
<!-- Banner Section Start -->

<?php $this->view($banner); ?>
<!-- Banner Section End -->
<div class="cl"></div>
<!-- Mid Section Start -->
<!-- Mid Section Start -->
<section id="mid_pan">
  <div class="mid-wrapper">
    <!--Content Start-->
    <div id="mid-cont">
      <div class="mid-ctr">
        <div class="mid-lt">
          <div class="mid-rt">
            <div class="main-table">
              <!--Head-->
              <div class="head">Rupee Management</div>
              <!--Head end-->
              <div class="col-wt">
                <div class="form-add">
                  <script type="text/javascript">

$(document).ready(function(){

$("#frm").validate();
$(".checklimit").keypress(function(){

var str=$(this).val();
if(str.length >49)
{
$("#"+$(this).attr('id')+'_er').html('<font color="red">characher limit exceed.</font>');
}
else{
$("#"+$(this).attr('id')+'_er').html('');
}
//alert($(this).val());
});
var newemailNO = 0;
var emailNO = 0;

$("#button1").click(function(){
emailNO = document.getElementById('emailNO').value;
newemailNO = parseInt(emailNO)+1; 
$(".xyz").append('<input type="text" name="email_'+newemailNO+'" id="email_'+newemailNO+'" size="32" class="email" value="" style="margin-top:10px;" />');
$(".xyz").append('<input type="button" class="btndelete" name="delete_'+newemailNO+'" id="delete_'+newemailNO+'" align="right" value="Delete&nbsp;" onclick="hidefun('+newemailNO+');" /><div class="cl"></div>');
document.getElementById('emailNO').value = newemailNO;
});
$('#name').blur(function(){
var a = $("#name").val();
if(a!='')
{
$.post("http://invoice.enthuons.com/client/checkname", {	Name: $('#name').val()}, function(response){ $('#Loading').html(response); });
}
else
{
$('#Loading').html('');
}
return false;
});

});
function hidefun(value)
{

if(document.getElementById('email_'+value))
{ 
document.getElementById('email_'+value).value='';
document.getElementById('email_'+value).style.display = 'none';
document.getElementById('delete_'+value).style.display = 'none';
}
}

</script>
                  <form action="<?php echo base_url()?>config_changes/update_config"  method="post" enctype="multipart/form-data" id="frm">
                    
                    <div class="rowone">
                      <label>Rupees for wicket * </label>
                       <input id="wicket" class="required character digits" type="text" value="<?php echo $select->rupess_for_wicket ;?>" name="wicket" />
                        
					  
                    </div>
					<div class="rowone">
                      <label>Rupees for run * </label>
                       <input id="run" class="required character digits" type="text" value="<?php echo $select->rupess_for_run;?>" name="run" />
                        
					  
                    </div>
					<div class="rowone">
                      <label>Rupees for invite * </label>
                       <input id="invite" class="required character digits" type="text" value="<?php echo $select->rupee_for_invite;?>" name="invite" />
                        
					  
                    </div>
					<div class="rowone">
                      <label>Time for user entry * </label>
                       <input id="time_entry" class="required character digits" type="text" value="<?php echo $select->time_user_entry;?>" name="time_entry" />
                        
					  
                    </div>
                    <div class="rowone">
                      <label>From email id for sucessfull user*  </label>
                      <input id="sucess_user" class="required email" type="text" value="<?php echo $select->register_user_email ;?>" name="sucess_user">
                    </div>
               <div class="rowone">
                      <label>Contact us email id *  </label>
                      <input id="contact" class="required email" type="text" value="<?php echo $select->contact_us_email ;?>" name="contact">
                    </div>
					<div class="rowone">
                      <label>suggestion  email id *  </label>
                      <input id="suggestion" class="required email" type="text" value="<?php echo $select->suggestion_email ;?>" name="suggestion">
                    </div>
                   <div class="rowone">
                      <label>API url * </label>
                       <input id="api_url" class="required character" type="text" value="<?php echo $select->api_url ;?>" name="api_url" />
                        
					  
                    </div>
                    <div class="add-right-mn">
                      <div class="total" style="padding-left: 268px;">
                        <div class="cl"></div>
                       
                        <input name="Submit"  id="BtnSubmit" type="submit" class="btn_save" value="Save;" />
							<a href="<?php echo base_url()?>admin_panel/dashboard"><img src="<?php echo base_url()?>public/images/cancel.png" /></a>
						<span id="errormess"><?php
								if( isset ($msgg))
									{
								echo $msgg;
									}
							?></span>
                      </div>
					   
                    </div>
                  </form>
                </div>
              </div>
              <div class="cl"></div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!--Content Start-->
    <div class="cl"></div>
  </div>
</section>
<!-- Mid Section End -->
<?php $this->view($footer); ?>
<!-- Footer Section End -->
</body>
</html>
