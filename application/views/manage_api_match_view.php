
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Manage API</title>
<meta name="keywords" content="">
<meta name="description" content="">
<link rel="shortcut icon" href="<?php  echo base_url(); ?>public/images/favicon.ico" />
<link href="<?php  echo base_url(); ?>public/css/style.css" rel="stylesheet" type="text/css" media="handheld, screen" />
<link href="<?php  echo base_url(); ?>public/css/jquery-ui.css" rel="stylesheet" type="text/css" />
<link href="<?php  echo base_url(); ?>public/css/admin_panel.css" rel="stylesheet" type="text/css" />
<!--<link rel="stylesheet" href="http://invoice.enthuons.com/js/admin_panel/lavalamp_test.css" type="text/css" media="screen" />-->
<!--[if IE]>  
<script src="http://invoice.enthuons.com/js/html5.js"></script>
<![endif]-->
<!--[if lte IE 6]>
<script type="text/javascript" src="http://invoice.enthuons.com/js/pngfix.js"></script>
<script type="text/javascript" src="http://invoice.enthuons.com/js/ie6.js"></script>
<link rel="stylesheet" href="http://invoice.enthuons.com/css/ie6.css" type="text/css" />
<![endif]-->
<!-- Menu Bar Start -->
<script type="text/javascript" src="<?php  echo base_url(); ?>public/js/jquery.js"></script>
<script type="text/javascript" src="<?php  echo base_url(); ?>public/js/jquery.validate.js"></script>
<script type="text/javascript" src="<?php  echo base_url(); ?>public/js/jquery-ui.js"></script>
<script type="text/javascript" src="<?php  echo base_url(); ?>public/js/maskedinput.js"></script>
<script type="text/javascript" src="<?php  echo base_url(); ?>public/js/admin_panel.js"></script>
<!--<script type="text/javascript" src="http://invoice.enthuons.com/js/banner/jquery.min.js"></script>-->
<!--<script type="text/javascript" src="http://invoice.enthuons.com/js/admin_panel/jquery.lavalamp.js"></script>
<script type="text/javascript" src="http://invoice.enthuons.com/js/admin_panel/jquery.easing.min.js"></script>
<script type="text/javascript" src="http://invoice.enthuons.com/js/admin_panel/jquery.lavalamp.min.js"></script>-->
<!--<script type="text/javascript"> 
    $(function() {
        $("#1, #2, #3").lavaLamp({
            fx: "backout", 
            speed: 900,
            click: function(event, admin_panelItem) {
                return true;
            }
        });
    });
    //jQuery.noConflict();
</script>-->
<!-- Menu Bar End -->
<script type="text/javascript"> 
    function dynamic_change(){
        var sel = document.getElementById("select_lan").value;
        document.getElementById("m_lan").innerHTML = sel;
    }
</script>
<!--Text Field Start-->
<script type="text/javascript"> 
 
    function clearText(thefield){
        if (thefield.defaultValue==thefield.value)
            thefield.value = ""
    } 
 
    function fillText(thefield){
        if (thefield.value=="")
            thefield.value = thefield.defaultValue;
    }
</script>
<script>
 function check()
 {
 
     var status=document.forms['frm']['team1'].value;
	 
	  if(status=="select")
	  {
document.getElementById("team11").innerHTML= "Please select any one to search";

		  return false;
		  }

 }
</script>

<script type="text/javascript">        
    ddsmoothadmin_panel.init({
        mainadmin_panelid: "smoothadmin_panel1", //admin_panel DIV id
        orientation: 'h', //Horizontal or vertical admin_panel: Set to "h" or "v"
        classname: 'ddsmoothadmin_panel', //class added to admin_panel's outer DIV
        //customtheme: ["#1c5a80", "#18374a"],
        contentsource: "markup" //"markup" or ["container_id", "path_to_admin_panel_file"]
    })    
    ddsmoothadmin_panel.init({
        mainadmin_panelid: "smoothadmin_panel2", //Menu DIV id
        orientation: 'v', //Horizontal or vertical admin_panel: Set to "h" or "v"
        classname: 'ddsmoothadmin_panel-v', //class added to admin_panel's outer DIV
        //customtheme: ["#804000", "#482400"],
        contentsource: "markup" //"markup" or ["container_id", "path_to_admin_panel_file"]
    })        
    </script>
    
    <script language="javascript" type="text/javascript">
function update(id)
{
	 document.getElementById('form1').submit();
}

        
	</script>
<!--Text Field End-->
</head>
<body>
<!-- Navigation Section Start -->

<?php $this->view($header); ?>

<!-- Navigation Section End -->
<!-- Banner Section Start -->

<?php $this->view($banner); ?>

<!-- Banner Section End -->
<div class="cl"></div>
<!-- Mid Section Start -->
<!-- Mid Section Start -->
<section id="mid_pan">
  <div class="mid-wrapper">
    <!--Title Bar-->
    	<div class=""><h1 style="margin-left: 12px; padding-top: 15px;">Manage API</h1></div>
    <form method="post" action="<?php echo base_url();?>manage_api/search" name="frm" onsubmit="return check();">
      <div class="pad">
        <div class="title-bg">
          <div class="title-main">
            <div class="colmh1 colm-first" style="width:160px;">
              <label style="width:296px;">Select Tournament :</label>
            </div>
            <div class="colmh1 colmh3">
              <div class="serchm">
                           <select id="abc" class="select" name="team1">
                            <option value="select">please select</option>
                            <?php 
					              foreach($tournament_name as $key=>$value)
					                {?>
					                      <option value="<?php echo $value->team_id;?>"><?php echo $value->team_name;?></option>
					            <?php    }
                    		?>
                            </select><span id="team11" class="serror"></span>
                </div>
			
            </div><div class="serchm"></div>
            <div>
           <input type="submit" value="search" /></div>
		<span id="set"><?php if($this->uri->segment(2)=="search12"){ ?> <a href="<?php echo base_url()?>admin_panel/manage_member"><img src="<?php echo base_url()?>public/images/reset.png"  style="padding-left: 185px;padding-top: 4px;"/></a><?php } ?> </span>
            <div class="colmh1 colmh1-last" style="width: 42%; padding-left:2px;padding-top: 10px;">
              <ul style="float:right;">
                              </ul><span id="sea" style="color:#FF0000"></span>
            </div>
            <!--<span class="view_all"><a href="http://invoice.enthuons.com/client/">View All</a></span>--> </div>
        </div>
      </div>
    </form>
    <!--Title Bar end-->
    <span id="message" style="color:#006600;margin-left: 490px; font-size:12px; font-weight:bold"></span>
    <div class="cl"></div><span class="add_client"><a href="<?php  echo base_url(); ?>manage_player/add_player_open" title="Add Player"></a></span>
    <!--Content Start-->
    <div id="mid-cont" style="margin-top:0px;">
      <div class="mid-ctr">
        <div class="mid-lt">
          <div class="mid-rt">
            <div class="cl"></div>
            <div class="main-table">
              <!--Head--><span id="message" style="color:#006600;margin-left: 490px; font-size:12px; font-weight:bold"><?php if(isset($message4)){ echo"$message4";} ?></span>
			  <?php if($message1!=""){?>
              <div class="colm colmh colm-first" style="width:80px;">
                <div class="short-div"> <a href="#"><span class="short-div-lt">S.No</span><span class="fr"></span></a> </div>
              </div>
              <div class="colm colmh" style="width:150px;">
                <div class="short-div"> <a href="#"><span style="float: left; padding: 9px 35px;">Player Name</span><span class="fr"></span></a> </div>
              </div>
              <div class="colm colmh" style="width:230px;">
                <div class="short-div"> <a href="#"><span class="manas_div">Unique Name</span><span class="fr"></span></a> </div>
              </div>
              <div class="colm colmh" style="width:160px;">
                <div class="short-div"> <a href="#"><span style="float: left;padding: 9px 49px;">API Name</span></a> </div>
              </div>
              
              <div class="colm colmh colm-last" style="width:140px; margin-left: 61px;">
                <div class="short-div"> <a href="#"><span class="short-div-lt">API Unique Name</span><span class="fr"></span></a> </div>
              </div>
              <!--Head end-->
              <div class="col-wt">
			   <?php   if(isset($err)) { ?>
			  <div class="colmcClient3" style="width:1000px; color:#006600; text-align:center;"><?php  echo $err; ?></div>
			  <?php }?>
              <form action="<?php echo base_url();?>manage_api/update" method="post" name="myform" id="form1">
			   <?php 
			   $ii=1;
			   foreach($m_use as $key=>$value)
					 { ?>
                     <div class="row row3" style="width:100%; background:url('http://www.demotbs.com/dev/tbsinvoice/images/row_bg2.jpg') repeat-y left top;"><div class="colmc" style="width:100px;"> <span class="cont"><?php  echo $ii;?></span> </div>					
                  <div class="colmcClient3" style="width:166px; text-align:center;"><span class="cont">
                 <?php echo $value->player_name;?> </span> </div>
				 
                  <div class="colmcClient3" style="width:255px; text-align:center;"><span class="cont"><input type="text" name="player_name[]" value="<?php if(isset($value->api_unike_name)) echo $value->api_unike_name;?>"/></span> </div>
                     <div class="colmcClient3" style="width:136px; text-align:center;"><span class="cont">
                    <input type="hidden" name="player_id[]" value="<?php echo $value->player_id;?>" /> </span> </div>
                  <div class="colmcClient3" style="width:255px; text-align:center;"><span class="cont"> </span> </div>
                   </div>
</span>
				 <?php
				 
					$ii++;
						
					}
					
					?>
                  
				  <span id="message" style="color:#006600;margin-left: 490px; font-size:12px; font-weight:bold"><?php if(isset($message2)){ echo"$message2";} ?></span>
       
                  
                </div>
                 <div class="team_lft" id="score7" >
          <ul>
            <li><strong>
              <?php  echo $a; ?>
              </strong></li>
            <?php if(isset($score["data"]["card"]["teams"]["a"]["match"]))
	  {  
	   foreach($score["data"]["card"]["teams"]["a"]["match"]["playing_xi"] as $key=>$value)
			{
			foreach($score["data"]["card"]["players"] as $key1=>$value1)
			{
			if($value==$key1)
			{ ?>
            <li>
              <?php  echo $score["data"]["card"]["players"][$key1]["fullname"]; ?>
            </li>
			<li><?php echo $value?></li>
            <?php  }}}
			} ?>
          </ul>
        </div>
        <div class="team_rgt" id="score8" >
          <ul>
            <li><strong>
              <?php  echo $b; ?>
              </strong></li>
            <?php if(isset($score["data"]["card"]["teams"]["b"]["match"]))
	  { 
	  foreach($score["data"]["card"]["teams"]["b"]["match"]["playing_xi"] as $key=>$value)
			{
			foreach($score["data"]["card"]["players"] as $key1=>$value1)
			{
			if($value==$key1)
			{ ?>
            <li>
              <?php  echo $score["data"]["card"]["players"][$key1]["fullname"]; ?>
            </li>
			<li><?php echo $value;?></li>
            <?php  }}}
			} ?>
          </ul>
        </div>
	
               
                              
              </form><div class="cl"></div></div>
			          <div class="pre_next">
    <ul> <input type="button" onclick="update(document.myform.player_id);" value="update selected" name="submit" style="height:28px; width:96px;" />
    <p><?php }  ?></p>
	</ul>
    </div>
            </div>
          </div>
        </div>
      </div>
    </div>
	  
    <!--Content end-->
    <div class="cl"></div>
	
	

  </div>
</section>
<!-- Mid Section End -->
<!-- Footer Section Start -->

<?php $this->view($footer); ?>

<!-- Footer Section End -->
</body>
</html>
