<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Invite_friends extends CI_Controller {
 
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	 public  function __construct(){
        parent::__construct();
      	$this->load->database();
	    $this->load->library('session');
	    $this ->load->model('setting_model');
		$this ->load->model('invite_friend_model');
		$this->load->library('GmailContacts_lib');
	   $this->load->helper('mail_helper');
	   	 $this ->load->model('helper_model');
	}
	public function index()
	{
	
	 $se=$this->session->userdata;
	 if(isset($se['log']))
		{
		 $email=$se['log']->u_email;
		
		$data['msg']=$se['log']->u_name ;
		$data['done_msg']="";
		$data['footer']	="main/include1/footer";
		$data['header']	="main/include1/header";
		$data['sidebar']	="main/include1/sidebar";
		$this->load->view('main/user_invite_mail',$data);
		}
		else
		{
		
			redirect('login');
		}
		
	}
	
	public function fetch()
	{
					
				
		if($_POST['email1']!='')
		{
		
		$uname=$_POST['email1'];
		$passwd=$_POST['pass1'];
	
		$gmail = new GmailContacts_lib();
		$gmailContactsList=$gmail->get_gmail_contacts($uname, $passwd);
	$i=0;
		foreach($gmailContactsList as $k=>$v){
			
			
			$mail[$i]=array(
				'email'.$i=>$k,'name'.$i=>$v['fullName']
			);$i++;
			}
				//print_r($mail);exit;
			
			
			 $se=$this->session->userdata;
		 $email=$se['log']->u_email;
	
		$data['msg']=$se['log']->u_name ;
		$data['mail']=$mail;
		$data['selected_mail']="";
		$data['done_msg']="";
		$data['footer']	="main/include1/footer";
		$data['header']	="main/include1/header";
		$data['sidebar']	="main/include1/sidebar";
		$this->load->view('main/user_gmail_friends',$data);	
		
		}
		else
		{
	redirect('invite_friends');
		}
		
		
		
	}
	public function add()
	{
	$i=0;
	$emails='';
$ins='';
/*print_r($_POST);exit;*/
	foreach($_POST as $key=>$value)
	{
	
	$emails[$i]=$value;
	$i++;
	}
	//print_r($emails);
	$t_time=time();
	 $se=$this->session->userdata;
		
			$user_id=$se['log']->user_id;
	$select =$this->invite_friend_model->fetch_i_email($emails);
//print_r($select);exit;
	$i=0;
	foreach($_POST as $key=>$value)
	{
	$count=0;
	
	if($select!='')
	{
	foreach($select as $key1=>$value1)
	{
	if($value1->i_email==$value)
	{
	$count=1;
	//break;
	}
	}
	}
	if($count==0)
	{
	$ins[$i]=array('i_email'=>$value,'freiend_id'=>$user_id,'invite_date'=>$t_time);
	$i++;
	}
	}
		/*if($ins!='')
		{
	$insert =$this->invite_friend_model->insert_i_email($ins);
	}*/
//print_r($ins);exit;
	 $se=$this->session->userdata;
		 $email=$se['log']->u_email;
		 $data['total_i']=$i;
		$data['selected_mail']=$ins;
		$data['msg']=$se['log']->u_name ;
		$data['done_msg']="";
		$data['mail']='' ;
		$data['footer']	="main/include1/footer";
		$data['header']	="main/include1/header";
		$data['sidebar']	="main/include1/sidebar";
		$this->load->view('main/result',$data);
	}
	
	
	public function send_request()
	{
	
	$i=0;
	$emails='';
$ins='';
	foreach($_POST as $key=>$value)
	{
	$emails[$i]=$value;
	$i++;
	}
	//print_r($emails);
	$t_time=time();
	 $se=$this->session->userdata;
		
			$user_id=$se['log']->user_id ;
			$u_name=$se['log']->u_name ;
	$select =$this->invite_friend_model->fetch_i_email($emails);

	$i=0;
	foreach($_POST as $key=>$value)
	{
	$count=0;
	
	if($select!='')
	{
	foreach($select as $key1=>$value1)
	{
	if($value1->i_email==$value)
	{
	$count=1;
	//break;
	}
	}
	}
	if($count==0)
	{
	$send_mail[$i]=$value;
	$ins[$i]=array('i_email'=>$value,'freiend_id'=>$user_id,'invite_date'=>$t_time);
	$i++;
	}
	}	//print_r($send_mail);exit;
		if($ins!='')
		{
	$insert =$this->invite_friend_model->insert_i_email($ins);
	}

		$s_email =$this->helper_model->register();
		if(isset($send_mail)){
		foreach($send_mail as $key=>$value)
		{
	regiseter_mail($value,$u_name,$s_email->register_user_email,4,0);
	}
	$select =$this->invite_friend_model->select_price($user_id);
	$total_price=$select->total_account_amount;
	$select_invite=$this->invite_friend_model->select_invite_price($user_id);
	$current_price=$select_invite->rupee_for_invite * $i;
	$total_price=array('total_account_amount'=>$total_price+$current_price);
	
	$update =$this->invite_friend_model->update_price($user_id,$total_price);
//print_r($ins);exit;
	 $se=$this->session->userdata;
		 $email=$se['log']->u_email;
		
		$data['done_msg']="Invitation send sucessfully. you have earn Rs".$current_price;
		$data['msg']=$se['log']->u_name ;
		$data['footer']	="main/include1/footer";
		$data['header']	="main/include1/header";
		$data['sidebar']	="main/include1/sidebar";
		$this->load->view('main/user_invite_mail',$data);
		}
		else
		{
		redirect('invite_friends');
		}
	}
	
	public function result()
	{
		$se=$this->session->userdata;
		// $user_id=$se['log']->user_id;
		$clientid	=	'378238922618-0ifa31n3n6ffah44uai80q0eah4g7t49.apps.googleusercontent.com';
		$clientsecret	=	'o0PzNzc5Muu5Wu6M-hQU5uqk';
		$maxresults	=	 9999;
		$redirecturi	=	'http://www.bossofmatch.com/invite_friends/result';
		$authcode = $_GET["code"];
		$fields=array('code'=>  urlencode($authcode),'client_id'=>  urlencode($clientid),'client_secret'=>  urlencode($clientsecret),'redirect_uri'=>  urlencode($redirecturi),'grant_type'=>  urlencode('authorization_code') );

		//url-ify the data for the POST
		$fields_string = '';
		foreach($fields as $key=>$value){ $fields_string .= $key.'='.$value.'&'; }
		$fields_string	=	rtrim($fields_string,'&');
		//open connection
		$ch = curl_init();
		curl_setopt($ch,CURLOPT_URL,'https://accounts.google.com/o/oauth2/token'); //set the url, number of POST vars, POST data
		curl_setopt($ch,CURLOPT_POST,5);
		curl_setopt($ch,CURLOPT_POSTFIELDS,$fields_string);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); // Set so curl_exec returns the result instead of outputting it.
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false); //to trust any ssl certificates
		$result = curl_exec($ch); //execute post
		curl_close($ch); //close connection
		//print_r($result);
		//extracting access_token from response string
		$response   =  json_decode($result);
		if(isset($response->access_token)){
		
				$accesstoken = $response->access_token;
				if( $accesstoken!='')
				$_SESSION['token']= $accesstoken;
				//passing accesstoken to obtain contact details
				$xmlresponse=  file_get_contents('https://www.google.com/m8/feeds/contacts/default/full?max-results='.$maxresults.'&oauth_token='. $_SESSION['token']);
				//reading xml using SimpleXML
				$xml=  new SimpleXMLElement($xmlresponse);
				$xml->registerXPathNamespace('gd', 'http://schemas.google.com/g/2005');
				$result = $xml->xpath('//gd:email');
				$i=0;
				$emails='';
				$ins='';
				foreach($result as $title){
			
						$emails.=$title->attributes()->address."+";
						$i++;
				}
				$emails=explode("+",$emails);
				$select =$this->invite_friend_model->fetch_i_email($emails);
				
				$i=0;
				foreach($result as $title){
				
						$count=0;
			
					if($select!=''){
					
							foreach($select as $key1=>$value1){
							
									if($value1->i_email==$title->attributes()->address){
			
												$count=1;
												//break;
									}
							}
					}
					if($count==0){
		
								$ins[$i]=array('i_email'=>$title->attributes()->address);
								$i++;
					}
				}
				if($ins==NULL){
						
						 $email=$se['log']->u_email;
						 $data['done_msg']="All your friend already invited";
						$data['msg']=$se['log']->u_name ;
						$data['footer']	="main/include1/footer";
						$data['header']	="main/include1/header";
						$data['sidebar']	="main/include1/sidebar";
						$this->load->view('main/user_invite_mail',$data);
				}
				else{
				// $email=$se['log']->u_email;
				$data['selected_mail']=$ins;
				 $data['total_i']=$i;
				if(isset($se['log']->u_name)){
				$data['msg']=$se['log']->u_name ;}
				else{
				redirect('invite_friends');
				}
				$data['mail']='' ;
				$data['done_msg']="";
				$data['footer']	="main/include1/footer";
				$data['header']	="main/include1/header";
				$data['sidebar']	="main/include1/sidebar";
				$this->load->view('main/result',$data);
				}
			}
			else{
				
				redirect('invite_friends');
			}
	}

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */