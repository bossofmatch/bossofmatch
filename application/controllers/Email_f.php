<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Email_f extends CI_Controller {
  
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	  public  function __construct() 
	 {
       	 parent::__construct();
      	 $this->load->database();
	  	 $this->load->library('session');
	     $this ->load->model('s_email_model');
		 $this ->load->model('winners_model');
		 $this->load->helper('livescore_helper');
		  $this->load->helper('next_match_helper');
	
		
    }
	public function index()
	{
		$data['header']	="main/include/header_main";
		$data['footer']	="main/include/footer_main";
		$data['top']	="main/include/top_main";
		$this->load->view('main/index_view',$data);
	}
	public function email1()
	{
	$next_match=next_match();
	//$select =$this->player_rates->select_player_rates();
	$data=live_score();
//print_r($data);exit;
// echo $count;
//print_r($score);exit;
//print_r($data);exit;
//date_default_timezone_set("Asia/Kolkata");
$date=$data["score"]["data"]["card"]["start_date"]["iso"];
//echo $date."<br>";
$startdate = strtotime($date);
//echo $startdate."<br>";
//$startdate=date("m/d/Y H:i",$startdate);	

$select_user =$this->livescore_model->select_user($startdate);
if($select_user!="")
{
foreach($select_user as $key=>$value)
{
$u_id[]=$value->user_id;
}
$select_user_team =$this->livescore_model->select_user_team($u_id);

if($select_user_team!="")
{
$i=1;foreach($select_user as $key44=>$value44) {
   $price=0; 
   foreach($select_user_team as $key=>$value)
 {
 if($value->user_id==$value44->user_id)
			{

			$price=$price+$value->price;
			} 
			}
			if($value44->user_image=="")
			{
			$img="";
			}
			else
			{
				$imagename=explode('public/upload_folder/',$value44->user_image);
				$filename = 'public/upload_folder/medium/'.$imagename[1];
				if(file_exists($filename)){
					$img=base_url().$filename;
				}
				else{
					$img=base_url().$value44->user_image;
				}
			}

  $dat[]=array($img,$i,$value44->team_name,$value44->u_name,$price);
        $i++;

			
    }
$last_match_winner =$this->winners_model->last_match_winner();
//print_r($last_match_winner);exit;
$data['last_match_winner']=$last_match_winner;
$data['next_match']=$next_match;
//	sort($dat);
	//print_r($dataaaa);
	foreach ($dat as $key => $row) {
    $volume[$key]  = $row['4'];
}
array_multisort($volume, SORT_DESC, $dat);
$data['select_user']=$dat;
}}

$cap = $_POST['cap'];
							
							$textvalue 	= 	$_COOKIE['strSec'];
					
							if ($cap == $textvalue){
								
	$s_email =$this->s_email_model->find_suggestion();
	if($s_email)
	{
	
		ini_set('safe_mode','off');
		$name = $_POST['name1'];
		
		$email = $_POST['email1'];
		$contact = $_POST['contact_no'];
		$sugg = $_POST['suggest'];
		$from =$s_email->suggestion_email;
		$subject = "Suggestion For Boss Of Match";
		$message = '
		<html lang="en">
		<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta name="viewport" content="initial-scale=1.0">    <!-- So that mobile webkit will display zoomed in -->
		<meta name="format-detection" content="telephone=no"> <!-- disable auto telephone linking in iOS -->
	
		<title>BossOfMatch</title>
	
		<style type="text/css">
		  
		/* Resets: see reset.css for details */
		.ReadMsgBody { width: 100%; background-color: #ebebeb;}
		.ExternalClass {width: 100%; background-color: #ebebeb;}
		.ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {line-height:100%;}
		body {-webkit-text-size-adjust:none; -ms-text-size-adjust:none;}
		body {margin:0; padding:0;}
		table {border-spacing:0;}
		table td {border-collapse:collapse;}
		.yshortcuts a {border-bottom: none !important;}
		
		
		/* Constrain email width for small screens */
		@media screen and (max-width: 600px) {
			table[class="container"] {
				width: 95% !important;
			}
		}
		
		/* Give content more room on mobile */
		@media screen and (max-width: 480px) {
			td[class="container-padding"] {
				padding-left: 12px !important;
				padding-right: 12px !important;
			}
		 }
		
		  
		/* Styles for forcing columns to rows */
		@media only screen and (max-width : 600px) {
	
			/* force container columns to (horizontal) blocks */
			td[class="force-col"] {
				display: block;
				padding-right: 0 !important;
			}
			table[class="col-2"] {
				/* unset table align="left/right" */
				float: none !important;
				width: 100% !important;
	
				/* change left/right padding and margins to top/bottom ones */
				margin-bottom: 12px;
				padding-bottom: 12px;
				border-bottom: 1px solid #eee;
			}
	
			/* remove bottom border for last column/row */
			table[id="last-col-2"] {
				border-bottom: none !important;
				margin-bottom: 0;
			}
	
			/* align images right and shrink them a bit */
			img[class="col-2-img"] {
				float: right;
				margin-left: 6px;
				max-width: 130px;
			}
		}
	
	
		</style>
	
		</head>
		<body style="margin:0; padding:10px 0;" bgcolor="#ebebeb" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
		
		<br>
		
        
        
        
		<!-- 100% wrapper (grey background) -->
		<table border="0" width="100%" height="100%" cellpadding="0" cellspacing="0" bgcolor="#ebebeb">
	  	<tr>
		<td align="center" valign="top" bgcolor="#ebebeb" style="background-color: #ebebeb;">
	<table style="background-color:#000;width:72%;" >
        <tr>
        <td style="padding-bottom:5px;">
        <img src="'.base_url().'public/f_img/Logo.png">
        </td>
        </tr>
        </table>
		 <!-- 600px container (white background) -->
		 <table border="0" width="600" cellpadding="0" cellspacing="0" class="container" bgcolor="#ffffff">
		<tr>
		 <td class="container-padding" bgcolor="#ffffff" style="background-color: #ffffff; padding-left: 30px; padding-right: 30px; font-size: 14px; line-height: 20px; font-family: Helvetica, sans-serif; color: #333;">
		<br>
	
				
	
	<div style="font-weight: bold; font-size: 18px; line-height: 24px; color: #f80b25;">
	Hello Admin, 
	
	Following suggestion for Boss Of Match :-

	
	</div>
	<br>
	
	<table border="0" cellpadding="0" cellspacing="0" class="columns-container">
	  <tr>
		<td class="force-col" style="padding-right: 20px;" valign="top">
	
			<!-- ### COLUMN 1 ### -->
			<table border="0" cellspacing="0" cellpadding="0" width="260" align="left" class="col-2">
			<tr>
				<td align="center" valign="top" style="font-size:13px; line-height: 20px; font-family: Arial, sans-serif;">
					
					
				
				  Name:<br><br>
				 
				</td>
			</tr>
            <tr>
				<td align="center" valign="top" style="font-size:13px; line-height: 20px; font-family: Arial, sans-serif;">
					
					
				
				  Email:<br><br>
				 
				</td>
			</tr>
			<tr>
				<td align="center" valign="top" style="font-size:13px; line-height: 20px; font-family: Arial, sans-serif;">
					
					
				
				   Contact No.:<br><br>
				 
				</td>
			</tr>
			<tr>
				<td align="center" valign="top" style="font-size:13px; line-height: 20px; font-family: Arial, sans-serif;">
					
					
				
				  suggestion:<br><br>
				 
				</td>
			</tr>
			<tr>
			
			</table>
	
		</td>
		<td class="force-col"  valign="top">
	
			<!-- ### COLUMN 2 ### -->
			<table border="0" cellspacing="0" cellpadding="0" width="260" align="right" class="col-2" id="last-col-2">
				<tr>
					<td align="left" valign="top" style="font-size:13px; line-height: 20px; font-family: Arial, sans-serif;">
						
						
					  
					  '.$name.'<br><br>
					 
					 
					  
					  
					</td>
				</tr>
				<tr>
					<td align="left" valign="top" style="font-size:13px; line-height: 20px; font-family: Arial, sans-serif;">
						
						
					  
					  '.$email.'<br><br>
					 
					 
					  
					  
					</td>
				</tr>
				<tr>
					<td align="left" valign="top" style="font-size:13px; line-height: 20px; font-family: Arial, sans-serif;">
						
						
					  
					  '.$contact.'<br><br>
					 
					 
					  
					  
					</td>
				</tr>
                <tr>
					<td align="left" valign="top" style="font-size:13px; line-height: 20px; font-family: Arial, sans-serif;">
						
						
					  
					  '.$sugg.'<br><br>
					 
					 
					  
					  
					</td>
				</tr>
			</table>
	
		</td>
	  </tr>
	   <tr>
	  <td colspan="2" align="center"><br><br><br><br> </td>
	  </tr>
	</table><!--/ end .columns-container-->
	
	</td>
	</tr>
	
		  </table>
		  <!--/600px container -->
	<table style=";background-color:#000;width:72%;">
    <tr>
     
     <td style="color:#f80b25; text-align:center"> Copyright &copy; 2014 www.bossofmatch.com All Right Reserved.</td>
	 
    <td>
   <a href="https://www.facebook.com/bossofmatch"> <img src="'.base_url().'public/mail/social_7.png"></a>
    <a href="https://plus.google.com/u/0/111838687092225929886/posts"> <img src="'.base_url().'public/mail/social_6.png"></a>
     <a href=""  target="_blank">  <img src="'.base_url().'public/mail/social_4.png"></a>
  <a href="https://twitter.com/bossofmatch">  <img src="'.base_url().'public/mail/social_2.png"></a>
 </td>
  
 
   
    </tr>
    
	 
    </table>
		</td>
	  </tr>
	</table>
    
    
    
    
	<!--/100% wrapper-->
	<br>
	<br>
	</body>
	</html>
	';
	
	$headers = "MIME-Version: 1.0" . "\r\n";
	$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
	
	// More headers
	$headers .= 'From: <'.$s_email->suggestion_email.'>' . "\r\n";
	//$headers .= 'Cc: myboss@example.com' . "\r\n";
	
		
		mail($s_email->suggestion_email,$subject,$message,$headers);
		$message	="Thanks_for_suggestion";
		redirect('suggestions/index/'.$message, 'refresh');
		}
		}else
		{
		
		$data['err']	="Please enter correct captcha";
		$data['header']	="main/include/header_main";
		$data['footer']	="main/include/footer_main";
		$data['top']	="main/include/top_main";
		$this->load->view('main/suggestion_view',$data);
}
	
	}
	public function email()
	{
	$next_match=next_match();
	//$select =$this->player_rates->select_player_rates();
	$data=live_score();
//print_r($data);exit;
// echo $count;
//print_r($score);exit;
//print_r($data);exit;
//date_default_timezone_set("Asia/Kolkata");
$date=$data["score"]["data"]["card"]["start_date"]["iso"];
//echo $date."<br>";
$startdate = strtotime($date);
//echo $startdate."<br>";
//$startdate=date("m/d/Y H:i",$startdate);	

$select_user =$this->livescore_model->select_user($startdate);
if($select_user!="")
{
foreach($select_user as $key=>$value)
{
$u_id[]=$value->user_id;
}
$select_user_team =$this->livescore_model->select_user_team($u_id);

if($select_user_team!="")
{
$i=1;foreach($select_user as $key44=>$value44) {
   $price=0; 
   foreach($select_user_team as $key=>$value)
 {
 if($value->user_id==$value44->user_id)
			{

			$price=$price+$value->price;
			} 
			}
			if($value44->user_image=="")
			{
			$img="";
			}
			else
			{
				$imagename=explode('public/upload_folder/',$value44->user_image);
				$filename = 'public/upload_folder/medium/'.$imagename[1];
				if(file_exists($filename)){
					$img=base_url().$filename;
				}
				else{
					$img=base_url().$value44->user_image;
				}
			}

  $dat[]=array($img,$i,$value44->team_name,$value44->u_name,$price);
        $i++;

			
    }
$last_match_winner =$this->winners_model->last_match_winner();
//print_r($last_match_winner);exit;
$data['last_match_winner']=$last_match_winner;
$data['next_match']=$next_match;
//	sort($dat);
	//print_r($dataaaa);
	foreach ($dat as $key => $row) {
    $volume[$key]  = $row['4'];
}
array_multisort($volume, SORT_DESC, $dat);
$data['select_user']=$dat;
}}

$cap = $_POST['cap'];
							
							$textvalue 	= 	$_COOKIE['strSec'];
						
							if ($cap == $textvalue){
	$s_email =$this->s_email_model->find_suggestion();
	if($s_email)
	{
	
		ini_set('safe_mode','off');
		$name = $_POST['name1'];
		
		$email = $_POST['email1'];
		$contact = $_POST['contact_no'];
		$sugg = $_POST['suggest'];
		$from =$s_email->suggestion_email;
		$subject = "Cotact by".$name;
		$message = '
		<html lang="en">
		<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta name="viewport" content="initial-scale=1.0">    <!-- So that mobile webkit will display zoomed in -->
		<meta name="format-detection" content="telephone=no"> <!-- disable auto telephone linking in iOS -->
	
		<title>BossOfMatch</title>
	
		<style type="text/css">
		  
		/* Resets: see reset.css for details */
		.ReadMsgBody { width: 100%; background-color: #ebebeb;}
		.ExternalClass {width: 100%; background-color: #ebebeb;}
		.ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {line-height:100%;}
		body {-webkit-text-size-adjust:none; -ms-text-size-adjust:none;}
		body {margin:0; padding:0;}
		table {border-spacing:0;}
		table td {border-collapse:collapse;}
		.yshortcuts a {border-bottom: none !important;}
		
		
		/* Constrain email width for small screens */
		@media screen and (max-width: 600px) {
			table[class="container"] {
				width: 95% !important;
			}
		}
		
		/* Give content more room on mobile */
		@media screen and (max-width: 480px) {
			td[class="container-padding"] {
				padding-left: 12px !important;
				padding-right: 12px !important;
			}
		 }
		
		  
		/* Styles for forcing columns to rows */
		@media only screen and (max-width : 600px) {
	
			/* force container columns to (horizontal) blocks */
			td[class="force-col"] {
				display: block;
				padding-right: 0 !important;
			}
			table[class="col-2"] {
				/* unset table align="left/right" */
				float: none !important;
				width: 100% !important;
	
				/* change left/right padding and margins to top/bottom ones */
				margin-bottom: 12px;
				padding-bottom: 12px;
				border-bottom: 1px solid #eee;
			}
	
			/* remove bottom border for last column/row */
			table[id="last-col-2"] {
				border-bottom: none !important;
				margin-bottom: 0;
			}
	
			/* align images right and shrink them a bit */
			img[class="col-2-img"] {
				float: right;
				margin-left: 6px;
				max-width: 130px;
			}
		}
	
	
		</style>
	
		</head>
		<body style="margin:0; padding:10px 0;" bgcolor="#ebebeb" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
		
		<br>
		
        
        
        
		<!-- 100% wrapper (grey background) -->
		<table border="0" width="100%" height="100%" cellpadding="0" cellspacing="0" bgcolor="#ebebeb">
	  	<tr>
		<td align="center" valign="top" bgcolor="#ebebeb" style="background-color: #ebebeb;">
	<table style="background-color:#000;width:72%;" >
        <tr>
        <td style="padding-bottom:5px;">
        <img src="'.base_url().'public/f_img/Logo.png">
        </td>
        </tr>
        </table>
		 <!-- 600px container (white background) -->
		 <table border="0" width="600" cellpadding="0" cellspacing="0" class="container" bgcolor="#ffffff">
		<tr>
		 <td class="container-padding" bgcolor="#ffffff" style="background-color: #ffffff; padding-left: 30px; padding-right: 30px; font-size: 14px; line-height: 20px; font-family: Helvetica, sans-serif; color: #333;">
		<br>
	
				
	
	<div style="font-weight: bold; font-size: 18px; line-height: 24px; color: #f80b25;">
	Hello Admin, 
	
	Following user want to contact you:-

	
	</div>
	<br>
	
	<table border="0" cellpadding="0" cellspacing="0" class="columns-container">
	  <tr>
		<td class="force-col" style="padding-right: 20px;" valign="top">
	
			<!-- ### COLUMN 1 ### -->
			<table border="0" cellspacing="0" cellpadding="0" width="260" align="left" class="col-2">
			<tr>
				<td align="center" valign="top" style="font-size:13px; line-height: 20px; font-family: Arial, sans-serif;">
					
					
				
				  Name:<br><br>
				 
				</td>
			</tr>
            <tr>
				<td align="center" valign="top" style="font-size:13px; line-height: 20px; font-family: Arial, sans-serif;">
					
					
				
				  Email:<br><br>
				 
				</td>
			</tr>
			<tr>
				<td align="center" valign="top" style="font-size:13px; line-height: 20px; font-family: Arial, sans-serif;">
					
					
				
				   Contact No.:<br><br>
				 
				</td>
			</tr>
			<tr>
				<td align="center" valign="top" style="font-size:13px; line-height: 20px; font-family: Arial, sans-serif;">
					
					
				
				  Message:<br><br>
				 
				</td>
			</tr>
			<tr>
			
			</table>
	
		</td>
		<td class="force-col"  valign="top">
	
			<!-- ### COLUMN 2 ### -->
			<table border="0" cellspacing="0" cellpadding="0" width="260" align="right" class="col-2" id="last-col-2">
				<tr>
					<td align="left" valign="top" style="font-size:13px; line-height: 20px; font-family: Arial, sans-serif;">
						
						
					  
					  '.$name.'<br><br>
					 
					 
					  
					  
					</td>
				</tr>
				<tr>
					<td align="left" valign="top" style="font-size:13px; line-height: 20px; font-family: Arial, sans-serif;">
						
						
					  
					  '.$email.'<br><br>
					 
					 
					  
					  
					</td>
				</tr>
				<tr>
					<td align="left" valign="top" style="font-size:13px; line-height: 20px; font-family: Arial, sans-serif;">
						
						
					  
					  '.$contact.'<br><br>
					 
					 
					  
					  
					</td>
				</tr>
                <tr>
					<td align="left" valign="top" style="font-size:13px; line-height: 20px; font-family: Arial, sans-serif;">
						
						
					  
					  '.$sugg.'<br><br>
					 
					 
					  
					  
					</td>
				</tr>
			</table>
	
		</td>
	  </tr>
	   <tr>
	  <td colspan="2" align="center"><br><br><br><br> </td>
	  </tr>
	</table><!--/ end .columns-container-->
	
	</td>
	</tr>
	
		  </table>
		  <!--/600px container -->
	<table style=";background-color:#000;width:72%;">
    <tr>
     
     <td style="color:#f80b25; text-align:center"> Copyright &copy; 2014 www.bossofmatch.com All Right Reserved.</td>
	 
    <td>
   <a href="https://www.facebook.com/bossofmatch"> <img src="'.base_url().'public/mail/social_7.png"></a>
    <a href="https://plus.google.com/u/0/111838687092225929886/posts"> <img src="'.base_url().'public/mail/social_6.png"></a>
     <a href=""  target="_blank">  <img src="'.base_url().'public/mail/social_4.png"></a>
  <a href="https://twitter.com/bossofmatch">  <img src="'.base_url().'public/mail/social_2.png"></a>
 </td>
  
 
   
    </tr>
    
	 
    </table>
		</td>
	  </tr>
	</table>
    
    
    
    
	<!--/100% wrapper-->
	<br>
	<br>
	</body>
	</html>
	';
	
	$headers = "MIME-Version: 1.0" . "\r\n";
	$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
	
	// More headers
	$headers .= 'From: <'.$s_email->suggestion_email.'>' . "\r\n";
	//$headers .= 'Cc: myboss@example.com' . "\r\n";
	
		
		mail($s_email->suggestion_email,$subject,$message,$headers);
		$message	="Thanks_for_contact_us";
		redirect('contact_us','refresh');
		}}
		else
		{
		$data['err']	="Please enter correct captcha";
		$data['header']	="main/include/header_main";
		$data['footer']	="main/include/footer_main";
		$data['top']	="main/include/top_main";
		$this->load->view('main/contact_us_view',$data);
	}
	}
	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */