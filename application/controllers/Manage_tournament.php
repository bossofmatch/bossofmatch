<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Manage_tournament extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	  public  function __construct(){
			  
			parent::__construct();
			$this->load->database();
			$this->load->library('session');
			$this ->load->model('login_model');
			$this ->load->model('manage_tournament_model');
			$this->load->library("pagination");
		    $se=$this->session->userdata;
	 		if(isset($se['ma'])){
				
			}
			else{
					
				$this->load->view('login_view');
			}
	  }
	
	  public function index(){
		  
	  }
	
	
	  public function add_tournament(){
		  
			 $se=$this->session->userdata;
			 if(isset($se['ma'])){
				 
					$key=$this->uri->segment(3);
					if($key=='ms'){
						
						$data['ms']="Mind Your Own Bussiness!!";
					}
					else{
						
						$data['ms']="";
					}
					$gamename = $this->manage_tournament_model->gamename();
					$data['gamename']=$gamename;
					$data['msg']=$se['ma']->a_name ;
					$data['banner']	="include/banner_view";
					$data['header']	="include/header_view";
					$data['footer']	="include/footer_view";
					$this->load->view('add_tournament_view', $data);
			 }
	  }
	
	
	
	  public function add(){
		  
			  $se=$this->session->userdata;
			  if(isset($se['ma'])){
				  
					$source =$_POST['tou_start_date'];
					$date = new DateTime($source);
					$date1= $date->format('d-m-Y');
					$startdate = strtotime($date1);	 		 
					$source1 =$_POST['tou_end_date'];
					$dateconvert = new DateTime($source1);
					$date2= $dateconvert->format('d-m-Y');
					$enddate = strtotime($date2);
					$add_data=array('t_short_name'=>$_POST['tou_short_name'],'t_full_name'=>$_POST['tou_full_Name'],'game_id'=>$_POST['game_name'],'t_start_date'=>$startdate,'t_end_date'=>$enddate);
					$insert=$this->manage_tournament_model->insertdata($add_data);
					$retrive_data=$this->manage_tournament_model->retrive_data($add_data);
					$loop=$_POST['loop'];
				    for($i=0;$i<$loop;$i++){
						
						   $insert_tou_team=$this->manage_tournament_model->insert_tou_team($retrive_data->t_id,$_POST['tou_team_'.$i]);
					}
					$se=$this->session->userdata;
					$config = array();
					$config["base_url"] = base_url() . "admin_panel/manage_tournament";
					$config["total_rows"] = $this->manage_tournament_model->record_count();
					$config["per_page"] = 10;
					$config["uri_segment"] = 3;
					$config['next_link'] = '&nbsp;&nbsp;&nbsp;&nbspNext';
					$config['prev_link'] = 'Pre&nbsp;&nbsp;&nbsp;&nbsp';
					$config['cur_tag_open']= '&nbsp;&nbsp;&nbsp;&nbsp;<strong>';
					$config['cur_tag_close']= '&nbsp;&nbsp;&nbsp;&nbsp;</strong>';
					$this->pagination->initialize($config);
					$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
					$data["m_use"] = $this->manage_tournament_model->fetch_member($config["per_page"], $page);
					$data["links"] = $this->pagination->create_links();
					$data['message']="New tournament created";
					$data['ii']=$this->uri->segment(3);
					$data['ii']=$data['ii']+1;
					$data['msg']=$se['ma']->a_name;
					$data['banner']	="include/banner_view";
					$data['header']	="include/header_view";
					$data['footer']	="include/footer_view";
					$this->load->view('manage_tournament_view', $data);
			  }
	  }
	
	  public function active(){
		  
			  $id=$_POST['id'];
			  $active=$_POST['active'];
			  if($active==1){
				  
				  $active=0;
			  }
			  else{
				  
				  $active=1;
	  
			  }
			  $m_active =$this->manage_tournament_model->m_active($id,$active);
			  if ($m_active){
				  
				  echo $m_active;
		  
			  }
	  }
	
	
	  public function delete(){
	  
			  $id=$_POST['id'];
			  $m_delete =$this->manage_tournament_model->m_delete($id);
			  if ($m_delete){
				  
				  echo $m_delete;
			  
			  }
	  }
	
	  public function search(){
		  
			  if(isset($_GET['keys'])){
				  
			  		$keys = $_GET['keys'];
			  		redirect('manage_tournament/search12/'.$keys, 'refresh');
			  		exit;
			  }
	  }
	  
	  
	  public function search12()
	  {
			  $key=$this->uri->segment(3);
			  $config["base_url"] = base_url() . 'manage_tournament/search12/'.$key;
			  $config["total_rows"] = $this->manage_tournament_model->record_count1($key);
			  $config["per_page"] = 10;
			  $config["uri_segment"] = 4;
			  $config['next_link'] = '&nbsp;&nbsp;&nbsp;&nbspNext';
			  $config['prev_link'] = 'Pre&nbsp;&nbsp;&nbsp;&nbsp';
			  $config['cur_tag_open']= '&nbsp;&nbsp;&nbsp;&nbsp;<strong>';
			  $config['cur_tag_close']= '&nbsp;&nbsp;&nbsp;&nbsp;</strong>';
			  $this->pagination->initialize($config);
			  $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
			  $data["m_use"] = $this->manage_tournament_model->fetch_search($config["per_page"], $page,$key);
			  if($data["m_use"]==""){
				  
					  $data["err"] ="No Result Found";
			  }
			  $data["links"] = $this->pagination->create_links();
			  $data['message']="";
			  $data['ii']=$this->uri->segment(4);
			  $data['seg']=$key;
			  $data['ii']=$data['ii']+1;
			  $se=$this->session->userdata;
			  $data['msg']=$se['ma']->a_name;
			  $data['banner']	="include/banner_view";
			  $data['header']	="include/header_view";
			  $data['footer']	="include/footer_view";
			  $this->load->view('manage_tournament_view', $data);
	  }
	
	  public function edit_tournament_open($id){
		  
			  $se=$this->session->userdata;
			  if(isset($se['ma'])){
				  
					  $u_id =$this->manage_tournament_model->tournament_edit_open($id);
					  $select=$this->manage_tournament_model->selectdata();
					  $teamselect=$this->manage_tournament_model->teamselect($id);
					  $data['teamselect']=$teamselect;
					  $data['information']=$u_id;
					  $data['select']=$select;
					  $data['msg']=$se['ma']->a_name ;
					  $data['banner']	="include/banner_view";
					  $data['header']	="include/header_view";
					  $data['footer']	="include/footer_view";
					  $this->load->view('edit_tournament_view', $data);
			  
			  }
	  }
	
	
	  public function update_edit(){
		  
			  $se=$this->session->userdata;
			  if(isset($se['ma'])){
				  
					  $source =$_POST['tou_start_date'];
					  $date = new DateTime($source);
					  $date1= $date->format('d-m-Y');
					  $startdate = strtotime($date1);	 		 
					  $source1 =$_POST['tou_end_date'];
					  $dateconvert = new DateTime($source1);
					  $date2= $dateconvert->format('d-m-Y');
					  $enddate = strtotime($date2);
					  $id=$_POST['hidden'];
					  $loop=$_POST['loop'];	
					  $show=$_POST['show'];
					  for($i=1;$i<=$show;$i++){
						  
							 $updatevalue=$this->manage_tournament_model->updateteam($_POST['team_tournament_'.$i],$_POST['tou_team_'.$i]);
					  }
					  $update_tournament=array('t_short_name'=>$_POST['tou_short_name'],'t_full_name'=>$_POST['tou_full_Name'],'game_id'=>$_POST['game_name'],'t_start_date'=>$startdate,'t_end_date'=>$enddate);
					  $success=$this->manage_tournament_model->update_tournament($id,$update_tournament);
					  for($i=1;$i<$loop;$i++){
						  
						  $insertdata=array('t_id'=>$id,'team_name'=>$_POST['tou_team1_'.$i]);
						  $insertteam=$this->manage_tournament_model->new_team_insert($insertdata);   
					  }
					  $se=$this->session->userdata;
					  $data['msg']=$se['ma']->a_name;
					  $config = array();
					  $config["base_url"] = base_url() . "admin_panel/manage_tournament";
					  $config["total_rows"] = $this->manage_tournament_model->record_count();
					  $config["per_page"] = 10;
					  $config["uri_segment"] = 3;
					  $config['next_link'] = '&nbsp;&nbsp;&nbsp;&nbspNext';
					  $config['prev_link'] = 'Pre&nbsp;&nbsp;&nbsp;&nbsp';
					  $config['cur_tag_open']= '&nbsp;&nbsp;&nbsp;&nbsp;<strong>';
					  $config['cur_tag_close']= '&nbsp;&nbsp;&nbsp;&nbsp;</strong>';
					  $this->pagination->initialize($config);
					  $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
					  $data["m_use"] = $this->manage_tournament_model->fetch_member($config["per_page"], $page);
					  $data["links"] = $this->pagination->create_links();
					  $data['message']="Edit Successfully";
					  $data['ii']=$this->uri->segment(3);
					  $data['ii']=$data['ii']+1;
					  $data['msg']=$se['ma']->a_name;
					  $data['banner']	="include/banner_view";
					  $data['header']	="include/header_view";
					  $data['footer']	="include/footer_view";
					  $this->load->view('manage_tournament_view', $data);
			  }
	  }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */