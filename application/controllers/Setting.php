<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Setting extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	  public  function __construct(){
        parent::__construct();
      	$this->load->database();
	    $this->load->library('session');
	    $this ->load->model('setting_model');
	   
	}
	
	
	public function index(){
	
	
			$se=$this->session->userdata;
			if(isset($se['log'])){
			
					$email=$se['log']->u_email;
					$setting =$this->setting_model->select_user_profile($email);
					$data['profile']=$setting;
					$data['msg']=$se['log']->u_name ;
					$data['footer']	="main/include1/footer";
					$data['header']	="main/include1/header";
					$data['sidebar']	="main/include1/sidebar";
					$this->load->view('main/user_setting',$data);
			}
			else{
					
					redirect('login');
				}
		}
			
			public function update_profile_pic(){
					include('public/resize-class.php');
					$upload_location ="public/upload_folder/";
					if(isset($_POST) and $_SERVER['REQUEST_METHOD'] == "POST"){
							$name = $_FILES['vasPhoto_uploads']['name'];
							$size = $_FILES['vasPhoto_uploads']['size'];
							
							$allowedExtensions = array("jpg","jpeg","gif","png");  //Allowed file types
							
							foreach ($_FILES as $file){
									if ($file['tmp_name'] > '' && strlen($name)){
												if (!in_array(end(explode(".", strtolower($file['name']))), $allowedExtensions)){
																echo '<div class="info" style="width:500px;">Sorry, you attempted to upload an invalid file format. <br>Only jpg, jpeg, gif and png image files are allowed. Thanks.</div><br clear="all" />';
												}
												else{
														if($size<(1024*1024)){
																$actual_image_name = $name;
																
																$extensi_name=explode(".",$actual_image_name);

																$extensi_name_new=str_replace($extensi_name[0], rand(1,100),$actual_image_name);

																$actual_image_name=time().$extensi_name_new;
															
																if(move_uploaded_file($_FILES['vasPhoto_uploads']['tmp_name'], $upload_location.$actual_image_name)){
																	
																$resizeObj = new resize($actual_image_name);
																$resizeObj -> resizeImage(114, 90, 'crop');
																$resizeObj -> saveImage('public/upload_folder/resize/'.$actual_image_name, 100);
																$resizeObj -> resizeImage(57, 45, 'crop');
																$resizeObj -> saveImage('public/upload_folder/small_image/'.$actual_image_name, 100);
																//$resizeObj -> resizeImage(width, height, 'crop');
																$resizeObj -> resizeImage(150, 120, 'crop');
																$resizeObj -> saveImage('public/upload_folder/medium/'.$actual_image_name, 100);
																$resizeObj -> resizeImage(96, 96, 'crop');
																$resizeObj -> saveImage('public/upload_folder/winner/'.$actual_image_name, 100);

					//Run your SQL Query here to insert the new image file named $actual_image_name if you deem it necessary
	echo '<span class="uploadeFileWrapper"><img src="'.$upload_location.$actual_image_name.'"  width="40" height="40">';
																			echo '<input type="hidden" value="'.$upload_location.$actual_image_name.'" id="url_name" name="img" id="img" /></span><br clear="all" /><br clear="all" />';
																}
																else {
																			echo "<div class='info' style='width:500px;'>Sorry, Your Image File could not be uploaded at the moment. <br>Please try again or contact the site admin if this problem persist. Thanks.</div><br clear='all' />";
																}
														}
														else{
																echo "<div class='info' style='width:400px;'>File exceeded 1MB max allowed file size. <br>Please upload a file at 1MB in size to proceed. Thanks.</div><br clear='all' />";
														}
												}
									}
									else {
											echo "<div class='info' style='width:400px;'>You have just canceled your file upload process. Thanks.</div><br clear='all' />";
									}
							}
					}
			
			}
			
			public function update_profile(){
				$se=$this->session->userdata;
			$email=$se['log']->u_email;
			$name=$_POST['name1'];
			$age=$_POST['age'];
			$sex=$_POST['sex'];
			if(isset($_POST['img']))
			{
			$img=$_POST['img'];
			}
			else
			{
				$img=$this->setting_model->selectimage($email);
				if($img->user_image==""){
						$img='';
				}
				else{
					$img=$img->user_image;
				}
			}
			
			$data=array('u_name'=>$name,'mobile_no'=>$_POST['number'],'connection_type'=>$_POST['connection_type'],'service_provider'=>$_POST['Service_provider_name'],'age'=>$age,'sex'=>$sex,'user_image'=>$img);
			//print_r($data);exit();
			$setting =$this->setting_model->update_profile($email,$data);
			$data['profile']=$setting;
			$data['msg']=$se['log']->u_name ;
			$data['footer']	="main/include1/footer";
			$data['header']	="main/include1/header";
			$data['sidebar']	="main/include1/sidebar";
			redirect('my_account');
			}



}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */