<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ipl_news extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	 public  function __construct(){
        parent::__construct();
      	$this->load->database();
	    $this->load->library('session');
	    $this ->load->model('login_model');
		  $this ->load->model('livescore_model');
	  	 $this ->load->model('winners_model');
		 $this->load->helper('livescore_helper');
		 // $this->load->helper('next_match_helper');
	   
 		
	   
	}
	public function index()
	{
	 $se=$this->session->userdata;

		if(isset($se['log']))
		{
		
			redirect('dashboard');
		}
		else
		{
	$this->output->cache(5);
	$xml=("https://news.google.co.in/news/feeds?pz=1&cf=all&ned=in&hl=en&q=Indian+Premier+League&output=rss");
		if(isset($xml))
		{
		$xmlDoc = new DOMDocument();
		$xmlDoc->load($xml);
		 
		$channel=$xmlDoc->getElementsByTagName('channel')->item(0);
		
		$channel_desc = $channel->getElementsByTagName('description')->item(0)->childNodes->item(0)->nodeValue;
	
		
		$ii[0]=$channel_desc;
		$x=$xmlDoc->getElementsByTagName('item');
		
		 $i=0;
			foreach($x as $item)
  			{
			
		  		$item_desc=$x->item($i)->getElementsByTagName('description')->item(0)->childNodes->item(0)->nodeValue;
	  			$i++;
				$ii[$i]= $item_desc;
			}
		}
		


		$data['inc']=$ii;
		$data['header']	="main/include/header_main";
		$data['footer']	="main/include/footer_main";
		$data['top']	="main/include/top_main";
		$this->load->view('main/ipl_live_view',$data);
	}
	
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */