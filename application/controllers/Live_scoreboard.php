<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Live_scoreboard extends CI_Controller {
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/
<method_name>
* @see http://codeigniter.com/user_guide/general/urls.html
	 */
	  public  function __construct() 
	 {
        parent::__construct();
      	$this->load->database();
	    $this->load->library('session');
	  $this ->load->model('livescore_model');
		 $this ->load->model('manage_member_model');
		 $this ->load->model('helper_model');
		 $this->load->helper('livescore_helper');
		 
	}
	
	
	public function index()
	{
	 $se=$this->session->userdata;

		if(isset($se['log']))
		{
		
			redirect('dashboard');
		}
		else
		{

	$data=$this->cache->get('li_score2');

		if(!$data)
		{
			$data1=live_score();
			$data1=json_encode($data1);
			$this->cache->save($data1, 'li_score2');
			$data = $this->cache->get('li_score2');
			}
//print_r($data);exit;
if(isset($data["time"])){
$date=$data["time"];

$startdate = strtotime($date);

$select_user = $this->cache->get('select_user');

if(!$select_user){
$select_user1 =$this->livescore_model->select_user($startdate);
$this->cache->write($select_user1, 'select_user');
$select_user = $this->cache->get('select_user');
}
if($select_user!="")
{
foreach($select_user as $key=>$value)
{
$u_id[]=$value->user_id;
}
$select_user_team = $this->cache->get('select_user_team');
if(!$select_user_team){
$select_user_team1 =$this->livescore_model->select_user_team($u_id);
$this->cache->write($select_user_team1, 'select_user_team');
$select_user_team = $this->cache->get('select_user_team');
}
if($select_user_team!="")
{
$i=1;foreach($select_user as $key44=>$value44) {
   $price=0; 
   foreach($select_user_team as $key=>$value)
 {
 if($value->user_id==$value44->user_id)
			{

			$price=$price+$value->price;
			} 
			}
			if($value44->user_image=="")
			{
			$img="";
			}
			else
			{
				$imagename=explode('public/upload_folder/',$value44->user_image);
				$filename = 'public/upload_folder/small_image/'.$imagename[1];
				if(file_exists($filename)){
					$img=base_url().$filename;
				}
				else{
					$img=base_url().$value44->user_image;
				}
			}

  $dat[]=array($img,$value44->c_t_time,$value44->team_name,$value44->u_name,$price);
        $i++;

			
    }

//	sort($dat);
	//print_r($dataaaa);
	if(isset($data['score']['data']['card']['now']['bowling_team']))
						{
	foreach ($dat as $key => $row) {
    $volume[$key]  = $row['4'];
}
}
else{
foreach ($dat as $key => $row) {
    $volume[$key]  = $row['1'];
}
}
array_multisort($volume, SORT_DESC, $dat);
$data['select_user']=$dat;
}}}
$data['comment']	="main/include/comment";
$data['header']	="main/include/header_main";
$data['footer']	="main/include/footer_main";
		
		$this->load->view('main/live_score_view',$data);
		}
	}
	
	

		
	public function all_rank()
	{	
		$data=$this->cache->get('li_score2');

		if(!$data)
		{
			$data1=live_score();
			$this->cache->write($data1, 'li_score2');
			$data = $this->cache->get('li_score2');
		}

$date=$data["time"];
//echo $date."<br>";
$startdate = strtotime($date);
//echo $startdate."<br>";
//$startdate=date("m/d/Y H:i",$startdate);	

$select_user1 =$this->livescore_model->select_user($startdate);
$select_user = $this->cache->get('select_user');

if(!$select_user){
$this->cache->write($select_user1, 'select_user');
$select_user = $this->cache->get('select_user');
}
if($select_user!="")
{
foreach($select_user as $key=>$value)
{
$u_id[]=$value->user_id;
}
$select_user_team1 =$this->livescore_model->select_user_team($u_id);
$select_user_team = $this->cache->get('select_user_team');

if(!$select_user_team){
$this->cache->write($select_user_team1, 'select_user_team');
$select_user_team = $this->cache->get('select_user_team');
}
if($select_user_team!="")
{
$i=1;foreach($select_user as $key44=>$value44) {
   $price=0; 
   foreach($select_user_team as $key=>$value)
 {
 if($value->user_id==$value44->user_id)
			{

			$price=$price+$value->price;
			} 
			}
			if($value44->user_image=="")
			{
			$img="";
			}
			else
			{
				$imagename=explode('public/upload_folder/',$value44->user_image);
				$filename = 'public/upload_folder/small_image/'.$imagename[1];
				if(file_exists($filename)){
					$img=base_url().$filename;
				}
				else{
					$img=base_url().$value44->user_image;
				}
			}

  $dat[]=array($img,$value44->c_t_time,$value44->team_name,$value44->u_name,$price);
        $i++;

			
    }

//	sort($dat);
	//print_r($dataaaa);
if(isset($data['score']['data']['card']['now']['bowling_team']))
						{
	foreach ($dat as $key => $row) {
    $volume[$key]  = $row['4'];
}
}
else{
foreach ($dat as $key => $row) {
    $volume[$key]  = $row['1'];
}
}
array_multisort($volume, SORT_DESC, $dat);

}}

if(isset($dat)){
		$i=0;
		 // asort($select_user);
		 

echo' <a href="'.base_url().'live-scoreboard" class="close"><img src="'.base_url().'public/images/close_pop.png" class="btn_close" title="Close Window" alt="Close" /></a>';

          echo '<table>
            <tr><td>&nbsp;</td>
            <th>Rank</th>
            <th>Team Name</th>
            <th>User Name</th>
            <th>Amount</th>
          </tr>';

 $i=1;
 foreach($dat as $key44=>$value44) {
echo' <tr>';
           echo' <td><img src="';if($value44[0]!=""){echo $value44["0"];}else{echo base_url().'public/u_images/user.jpg';}echo'" class="live_image"> </td>

            <td>';echo $i++; echo'</li>
            <td>';echo substr($value44["2"],0,18); echo'</td>
            <td>';echo $value44["3"]; echo'</td>
            <td>';
			echo $value44["4"];echo'</td>
          </tr>';
        }}
  }
	
	
public function userrank()
	{
		
	
	
if(isset($dat)){
		$i=0;
		 // asort($select_user);
		 

echo' <a href="'.base_url().'live-scoreboard" class="close"><img src="'.base_url().'public/images/close_pop.png" class="btn_close" title="Close Window" alt="Close" /></a>';

          echo '<table>
            <tr><td>&nbsp;</td>
            <th>Rank</th>
            <th>Team Name</th>
            <th>User Name</th>
            <th>Amount</th>
          </tr>';

 $i=1;
 foreach($dat as $key44=>$value44) {
echo' <tr>';
           echo' <td><img src="';if($value44[0]!=""){echo $value44["0"];}else{echo base_url().'public/u_images/user.jpg';}echo'" class="live_image"> </td>

            <td>';echo $i++; echo'</li>
            <td>';echo $value44["2"]; echo'</td>
            <td>';echo $value44["3"]; echo'</td>
            <td>';
			echo $value44["4"];echo'</td>
          </tr>';
        }}
  }
	
 
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */