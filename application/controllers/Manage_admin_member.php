<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Manage_admin_member extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	 public  function __construct(){
		
        parent::__construct();
      	$this->load->database();
		$this->load->library("pagination");
	    $this->load->library('session');
		$this ->load->model('manage_admin_member_model');
		$se=$this->session->userdata;

		if(isset($se['ma'])){
			
		 }
		 else{
			 	
			$this->load->view('login_view');
		 }
	}
	
	
	public function index(){
		
	}
	
	
	public function account(){
	
		$se=$this->session->userdata;
		if(isset($se['ma'])){
			
				$user_email=$_POST['user_email'];
				$password=$_POST['password'];
				$npassword=$_POST['npassword'];
				$cpassword=$_POST['cpassword'];
				$check =$this->manage_admin_member_model->check($password,$user_email);
				$g_password=$check->password;
				$password=md5($password);
				if($password==$g_password){
				
						if($npassword==$cpassword){
							
								$npassword=md5($npassword);
				   				$save =$this->manage_admin_member_model->update($npassword,$user_email);
								if($save){
									
									  $se=$this->session->userdata;
									  $data['msg']=$se['ma']->a_name;
									  $data['a_email']=$se['ma']->a_email;
									  $data['banner']	="include/banner_view";
									  $data['header']	="include/header_view";
									  $data['footer']	="include/footer_view";
								  	  $data['mg'] = "Password change";
									  $this->load->view('manage_account_view', $data);
								}
						}
						else{
							
								$se=$this->session->userdata;
								$data['msg']=$se['ma']->a_name;
								
								$data['a_email']=$se['ma']->a_email;
								$data['banner']	="include/banner_view";
								$data['header']	="include/header_view";
								$data['footer']	="include/footer_view";
							
								$data['mg'] = "New Password or Confirm Password Not Match";
								$this->load->view('manage_account_view', $data);
						}
				}
				else{
					
					  $se=$this->session->userdata;
					  $data['msg']=$se['ma']->a_name;
					  
					  $data['a_email']=$se['ma']->a_email;
					  $data['banner']	="include/banner_view";
					  $data['header']	="include/header_view";
					  $data['footer']	="include/footer_view";
				  
					  $data['mg'] = "Old Password Not Match";
					  $this->load->view('manage_account_view', $data);
				}
		}
	}
	
	
	public function active(){
		
		$id=$_POST['id'];
		$active=$_POST['active'];
		if($active==1)
		{
			$active=0;
		}
		else
		{
			$active=1;

		}
		$m_active =$this->manage_admin_member_model->m_active($id,$active);
		if ($m_active)
		{
			echo $m_active;
	
		}
	}
	
	
	public function delete(){
	
		$email=$_POST['email'];
		$m_delete =$this->manage_admin_member_model->m_delete($email);
		
		if ($m_delete){
			
			echo $m_delete;
		
		}
	}
	
	
	public function add_member(){
		
		 $se=$this->session->userdata;
		if(isset($se['ma'])){
			
			  $role=$this->manage_admin_member_model->selectrole();
			  $data['u_roll']=$role;
			  $data['msg']=$se['ma']->a_name;
			  $data['banner']	="include/banner_view";
			  $data['header']	="include/header_view";
			  $data['footer']	="include/footer_view";
			  $this->load->view('add_admin_account_view', $data);
		}
	}
	
	public function add_user(){
		$se=$this->session->userdata;
		if(isset($se['ma'])){
			
				$password=md5($_POST['password']);
				$time=time();
				$data=array('a_name'=>$_POST['name'],'a_email'=>$_POST['email'],'password'=>$password,'role'=>$_POST['user_roll'],'created_date'=>$time);
				$insertdata=$this->manage_admin_member_model->insertdata($data);
				
				
				$config = array();
				$config["base_url"] = base_url() . "admin_panel/manage_admin_account";
				$config["total_rows"] = $this->manage_admin_member_model->record_count();
				$config["per_page"] = 10;
				$config["uri_segment"] = 3;
				$config['next_link'] = '&nbsp;&nbsp;&nbsp;&nbspNext';
				$config['prev_link'] = 'Pre&nbsp;&nbsp;&nbsp;&nbsp';
				$config['cur_tag_open']= '&nbsp;&nbsp;&nbsp;&nbsp;<strong>';
				$config['cur_tag_close']= '&nbsp;&nbsp;&nbsp;&nbsp;</strong>';
				$this->pagination->initialize($config);
				$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
				$data["m_use"] = $this->manage_admin_member_model->fetch_member($config["per_page"], $page);
				$data["links"] = $this->pagination->create_links();
				$data['message']="Add New User...";
				$data['ii']=$this->uri->segment(3);
				$data['ii']=$data['ii']+1;
				$data['msg']=$se['ma']->a_name;
				$data['banner']	="include/banner_view";
				$data['header']	="include/header_view";
				$data['footer']	="include/footer_view";
				$this->load->view('manage_admin_account_view', $data);
		}
	}
	
	public function edit($id){
		
			 $se=$this->session->userdata;
			 if(isset($se['ma'])){
				 
					$u_id =$this->manage_admin_member_model->m_edit($id);
					$u_roll =$this->manage_admin_member_model->userroll();
					$data['u_roll'] = $u_roll;
				 	$data['ms']=$u_id;
					$data['msg']=$se['ma']->a_name;
					$data['banner']	="include/banner_view";
					$data['header']	="include/header_view";
					$data['footer']	="include/footer_view";
					$this->load->view('edit_admin_account_view', $data);
			}
		
	}
	
	public function Edit_user(){
		
		 $se=$this->session->userdata;
			 if(isset($se['ma'])){
				 
				   $user_id=$_POST['user_id'];
				   				   
				   $data=array('a_name'=>$_POST['user_name'],'a_email'=>$_POST['user_email'],'role'=>$_POST['user_roll']);
				   
				   $update_data=$this->manage_admin_member_model->update_data($user_id,$data);
				   if($update_data){
					   
						$config = array();
						$config["base_url"] = base_url() . "admin_panel/manage_admin_account";
						$config["total_rows"] = $this->manage_admin_member_model->record_count();
						$config["per_page"] = 10;
						$config["uri_segment"] = 3;
						$config['next_link'] = '&nbsp;&nbsp;&nbsp;&nbspNext';
						$config['prev_link'] = 'Pre&nbsp;&nbsp;&nbsp;&nbsp';
						$config['cur_tag_open']= '&nbsp;&nbsp;&nbsp;&nbsp;<strong>';
						$config['cur_tag_close']= '&nbsp;&nbsp;&nbsp;&nbsp;</strong>';
						$this->pagination->initialize($config);
						$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
						$data["m_use"] = $this->manage_admin_member_model->fetch_member($config["per_page"], $page);
						$data["links"] = $this->pagination->create_links();
						$data['message']="Update successfully...";
						$data['ii']=$this->uri->segment(3);
						$data['ii']=$data['ii']+1;
						$data['msg']=$se['ma']->a_name;;
						$data['banner']	="include/banner_view";
						$data['header']	="include/header_view";
						$data['footer']	="include/footer_view";
						$this->load->view('manage_admin_account_view', $data);
				   }
			 }
	}
	
	
	public function update_pass(){

				$user_email=$_POST['user_name'];
				$password=$_POST['password'];
				$npassword=$_POST['npassword'];
				$cpassword=$_POST['cpassword'];
				if($password==""||$npassword==""||$cpassword==""){
					
						echo "all fields are mandetotry";
				}
				else{
					
						$check =$this->manage_admin_member_model->check($password,$user_email);
						$g_password=$check->password;
						$password=md5($password);
						if($password==$g_password){
						
							  if($npassword==$cpassword){
								  
									$npassword=md5($npassword);
									$save =$this->manage_admin_member_model->update($npassword,$user_email);
									if($save){
									
										echo "Password change";
									
									}
							  }
							  else{
								  
									echo "New Password or Confirm Password Not Match";
								
							  }
						}
						else{
						
							echo "Old Password Not Match";
						}	
				}
		}
		
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */