<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class My_team_scroe_live extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	  public  function __construct(){
        parent::__construct();
      	$this->load->database();
	    $this->load->library('session');
	    $this ->load->model('login_model');
	   $this ->load->model('my_team_scroe_live_model');
	     $this ->load->model('setting_model'); 
	    $se=$this->session->userdata;

		if(isset($se['log'])){
		$data['msg']=$se['log']->u_name ;
		}
	}
	
	
	public function index(){
		
		 $se=$this->session->userdata;
		
			 if(isset($se['log'])){
				
					$user_id=$se['log']->user_id;
					$email=$se['log']->u_email;
					$teamname = $this->setting_model->select_match($email);
					$setting =$this->setting_model->select_user_profile($email);
					if($setting->team_selected!='0'){
					
							$select=$this->my_team_scroe_live_model->select_user_team($user_id);
							if($select){
							
									$data['select']= $select;
							}
					}
					$data['teamname']=$teamname;
					//print_r($teamname);print_r($select);exit;
					$data['msg']=$se['log']->u_name ;
					$data['footer']	="main/include1/footer";
					$data['header']	="main/include1/header";
					$data['sidebar']	="main/include1/sidebar";
					$this->load->view('main/user_my_team_scroe_live',$data);
			}
			else{
				
					redirect('login');
			}	
	}
	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */