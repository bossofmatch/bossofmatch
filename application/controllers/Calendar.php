<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Calendar extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	  public  function __construct(){
        parent::__construct();
      	$this->load->database();
	    $this->load->library('session');
	    $this ->load->model('login_model');
	   $this ->load->model('calendar_model');
	    $se=$this->session->userdata;

		if(isset($se['log'])){
		$data['msg']=$se['log']->u_name ;
		}
	}
	
	
	public function index(){
		 $se=$this->session->userdata;
		 if(isset($se['log'])){
		 
				$match=$this->calendar_model->fetch_match();
				
				$data['match']=$match;
				$data['msg']=$se['log']->u_name ;
				$data['footer']	="main/include1/footer";
				$data['header']	="main/include1/header";
				$data['sidebar']	="main/include1/sidebar";
				$this->load->view('main/user_calendar',$data);
			}
			else{
				
					redirect('login');
			}
	}
	
	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */