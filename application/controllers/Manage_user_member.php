<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Manage_user_member extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	 public  function __construct(){
		 
			parent::__construct();
			$this->load->database();
			$this->load->library('session');
			$this ->load->model('manage_member_model');
			$se=$this->session->userdata;
			$this->load->library("pagination");
			if(isset($se['ma'])){
				 
			}
			else{
					
				$this->load->view('login_view');
			 }
	}
	
	
	public function index(){
		
	}
	
	
	public function active(){
		
		$user_name=$_POST['name'];
		$active=$_POST['active'];
		if($active==1){
			
			$active=0;
		}
		else{
			
			$active=1;

		}
		$m_active =$this->manage_member_model->m_active($user_name,$active);
		if ($m_active){
			
			echo"manas";
	
		}
	}
	
	
	public function delete(){
	
		$user_name=$_POST['name'];
		$m_delete =$this->manage_member_model->m_delete($user_name);
		if ($m_delete){
			
			echo"manas";
		
		}
		
	}
	
	public function add_user(){
		
		$se=$this->session->userdata;
		if(isset($se['ma'])){
			
				$password=md5($_POST['password']);
				$user_email=$_POST['email_0'];
				$rupee=5000;
				$c_date=time();
				$data = array( 'u_name'=>$_POST['user_name'],'u_email'=>$_POST['email_0'],'password'=>$password,'total_account_amount'=>$rupee,'created_date'=>$c_date,'is_active'=>'1');
				$insert =$this->manage_member_model->insert($data ,$user_email);
				if($insert){
					
					$config["base_url"] = base_url() . "admin_panel/manage_member";
					$config["total_rows"] = $this->manage_member_model->record_count();
					$config["per_page"] = 10;
					$config["uri_segment"] = 3;
					$config['next_link'] = '&nbsp;&nbsp;&nbsp;&nbspNext';
					$config['prev_link'] = 'Pre&nbsp;&nbsp;&nbsp;&nbsp';
					$config['cur_tag_open']= '&nbsp;&nbsp;&nbsp;&nbsp;<strong>';
					$config['cur_tag_close']= '&nbsp;&nbsp;&nbsp;&nbsp;</strong>';
					$this->pagination->initialize($config);
					$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
					$data["m_use"] = $this->manage_member_model->fetch_member($config["per_page"], $page);
					$data["links"] = $this->pagination->create_links();
					$data['message']="New User Created";
					$data['ii']=$this->uri->segment(3);
					$data['ii']=$data['ii']+1;
					$data['msg']=$se['ma']->a_name;
					$data['banner']	="include/banner_view";
					$data['header']	="include/header_view";
					$data['footer']	="include/footer_view";
					$this->load->view('manage_member_view', $data);
				}
				else{
					
					  $data['msgg']="Email Aready Registerd ";
					  $data['msg']=$se['ma']->a_name ;
					  $data['banner']	="include/banner_view";
					  $data['header']	="include/header_view";
					  $data['footer']	="include/footer_view";
					  $this->load->view('add_user_member_view', $data);
				}
		}
 	}
	
	
	public function add_user_open(){
		
		$se=$this->session->userdata;
		if(isset($se['ma'])){
						
			$data['msg']=$se['ma']->a_name ;
			$data['banner']	="include/banner_view";
			$data['header']	="include/header_view";
 			$data['footer']	="include/footer_view";
			$this->load->view('add_user_member_view', $data);
		
		}
	}
	
	public function edit_user_member_open($id){
		
		$se=$this->session->userdata;
		if(isset($se['ma'])){
			
			$u_id =$this->manage_member_model->member_edit_open($id);
			$data['ms']=$u_id;
			$data['msg']=$se['ma']->a_name ;
			$data['banner']	="include/banner_view";
			$data['header']	="include/header_view";
 			$data['footer']	="include/footer_view";
			$this->load->view('edit_user_member_view', $data);
		
		}
	}
	
	
	public function edit_user(){
		
		  $se=$this->session->userdata;
		  if(isset($se['ma'])){
							  
				$user_email=$_POST['email_0'];
				$u_name=$_POST['user_name'];
				$age=$_POST['age'];
				$sex=$_POST['sex'];
				$total_account_amount=$_POST['total_account_amount'];
				$update =$this->manage_member_model->update($u_name,$age,$sex,$user_email,$total_account_amount);
				if($update){
					
					  $config["base_url"] = base_url() . "admin_panel/manage_member";
					  $config["total_rows"] = $this->manage_member_model->record_count();
					  $config["per_page"] = 10;
					  $config["uri_segment"] = 3;
					  $config['next_link'] = '&nbsp;&nbsp;&nbsp;&nbspNext';
					  $config['prev_link'] = 'Pre&nbsp;&nbsp;&nbsp;&nbsp';
					  $config['cur_tag_open']= '&nbsp;&nbsp;&nbsp;&nbsp;<strong>';
					  $config['cur_tag_close']= '&nbsp;&nbsp;&nbsp;&nbsp;</strong>';
					  $this->pagination->initialize($config);
					  $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
					  $data["m_use"] = $this->manage_member_model->fetch_member($config["per_page"], $page);
					  $data["links"] = $this->pagination->create_links();
					  $data['message']="User Updated";
					  $data['ii']=$this->uri->segment(3);
					  $data['ii']=$data['ii']+1;
					  $data['msg']=$se['ma']->a_name;
					  $data['banner']	="include/banner_view";
					  $data['header']	="include/header_view";
					  $data['footer']	="include/footer_view";
					  $this->load->view('manage_member_view', $data);
				}
		  }
	}
	
	
	public function search(){
		
		 if(isset($_GET['keys'])){
			 
            	$keys = $_GET['keys'];
            	redirect('manage_user_member/search12/'.$keys, 'refresh');
            	exit;
			}
	}
	public function search12(){
		
		  $key=$this->uri->segment(3);
		  $config["base_url"] = base_url() . 'manage_user_member/search12/'.$key;
		  $config["total_rows"] = $this->manage_member_model->record_count1($key);
		  $config["per_page"] = 10;
		  $config["uri_segment"] = 4;
		  $config['next_link'] = '&nbsp;&nbsp;&nbsp;&nbspNext';
		  $config['prev_link'] = 'Pre&nbsp;&nbsp;&nbsp;&nbsp';
		  $config['cur_tag_open']= '&nbsp;&nbsp;&nbsp;&nbsp;<strong>';
		  $config['cur_tag_close']= '&nbsp;&nbsp;&nbsp;&nbsp;</strong>';
		  $this->pagination->initialize($config);
		  $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
		  $data["m_use"] = $this->manage_member_model->fetch_search($config["per_page"], $page,$key);
		  if($data["m_use"]==""){
			  
				  $data["err"] ="No Result Found";
			  
		  }
		  $data["links"] = $this->pagination->create_links();
		  $data['message']="";
		  $data['ii']=$this->uri->segment(4);
		  $data['seg']=$key;
		  $data['ii']=$data['ii']+1;
		  $se=$this->session->userdata;
		  $data['msg']=$se['ma']->a_name;
		  $data['banner']	="include/banner_view";
		  $data['header']	="include/header_view";
		  $data['footer']	="include/footer_view";
		  $this->load->view('manage_member_view', $data);
	}
	
	
	
	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */