<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Manage_match29 extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	 public  function __construct(){
		 
			  parent::__construct();
			  $this->load->database();
			  $this->load->library("pagination");
			  $this->load->library('session');
			  $this ->load->model('manage_match_model');
			  $se=$this->session->userdata;
	  		  if(isset($se['ma'])){
				  
			  }
			  else{
				  	
				  $this->load->view('login_view');
			  }
	 }
	
	 public function index(){
		 
		
	 }
	
	 public function add_match()
	 {
			 $se=$this->session->userdata;
			 if(isset($se['ma'])){
				 
					$key=$this->uri->segment(3);
					if($key=='ms'){
						
						$data['ms']="Mind Your Own Bussiness!!";
					}
					else{
						
						$data['ms']="";
					}
					$t_full_name = $this->manage_match_model->t_full_name();
					$data['full_name']=$t_full_name;
					$data['msg']=$se['ma']->a_name ;
					$data['banner']	="include/banner_view";
					$data['header']	="include/header_view";
					$data['footer']	="include/footer_view";
					$this->load->view('add_match_view', $data);
			
			 }
	 }
	 
	
	 public function add()
	 {
			 $se=$this->session->userdata;
			 if(isset($se['ma']))
			 {
					$source =$_POST['match_start'];
					$date = new DateTime($source);
					$date1= $date->format('d-m-Y H:i');
					$startdate = strtotime($date1);	 		 
					$source1 =$_POST['match_end'];
					$dateconvert = new DateTime($source1);
					$date2= $dateconvert->format('d-m-Y H:i');
					$enddate = strtotime($date2);
					$add_data=array('t_id'=>$_POST['t_fullname'],'team1_id'=>$_POST['team1'],'team2_id'=>$_POST['team2'],'match_start_date_time_minute'=>$startdate,'match_end_date_time_minute'=>$enddate,'player_allowed'=>$_POST['player_allowed'],'is_active'=>$_POST['check_status']);
					$exitdataornot=$this->manage_match_model->exitdataornot($_POST['team1'],$_POST['team2'],$startdate,$enddate);
					if($exitdataornot){
						
							$key=$this->uri->segment(3);
							if($key=='ms'){
									
									$data['ms']="Mind Your Own Bussiness!!";
							}
							else{
						
									$data['ms']="";
							}
							$t_full_name = $this->manage_match_model->t_full_name();
							$data['full_name']=$t_full_name;
							$data['msg']=$se['ma']->a_name ;
							$data['message']="Match Already Register...";
							$data['banner']	="include/banner_view";
							$data['header']	="include/header_view";
							$data['footer']	="include/footer_view";
							$this->load->view('add_match_view', $data);
							
					}
					else{
							$insertdata=$this->manage_match_model->insertdata($add_data);
							$data['insertdata']=$insertdata;
							$se=$this->session->userdata;
							$config = array();
							$config["base_url"] = base_url() . "admin_panel/manage_match";
							$config["total_rows"] = $this->manage_match_model->record_count();
							$config["per_page"] = 10;
							$config["uri_segment"] = 3;
							$config['next_link'] = '&nbsp;&nbsp;&nbsp;&nbspNext';
							$config['prev_link'] = 'Pre&nbsp;&nbsp;&nbsp;&nbsp';
							$config['cur_tag_open']= '&nbsp;&nbsp;&nbsp;&nbsp;<strong>';
							$config['cur_tag_close']= '&nbsp;&nbsp;&nbsp;&nbsp;</strong>';
							$this->pagination->initialize($config);
							$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
							$data["m_use"] = $this->manage_match_model->fetch_match($config["per_page"], $page);
							$data["links"] = $this->pagination->create_links();
							$data['message']="Match Add";
							$data['ii']=$this->uri->segment(3);
							$data['ii']=$data['ii']+1;
							$data['msg']=$se['ma']->a_name;
							$data['banner']	="include/banner_view";
							$data['header']	="include/header_view";
							$data['footer']	="include/footer_view";
							$this->load->view('manage_match_view', $data);
					}
		 }
	}
	
	
	public function teamselect()
	{
		$se=$this->session->userdata;
		 if(isset($se['ma']))
		 {
		    $id=$_POST['name'];
			if($id=="select")
			{
				
			}
			else
			{
					$selectoption=$this->manage_match_model->select($id);
					if($selectoption>0)
					{
						$team1='<label style="width:296px;">Team 1 *</label><select id="abc" name="team1">';
						foreach($selectoption as $key=>$value1)
						{
									$team1=$team1."<option value=".$value1->team_id.">".$value1->team_name."</option>";
						}
						$team1=$team1.'</select>';
						$team2='<div class="rowone"><label style="width:296px;">Team 2 *</label><select id="abc" name="team2">';
						foreach($selectoption as $key=>$value1)
						{
									$team2=$team2."<option value=".$value1->team_id.">".$value1->team_name."</option>";
						}
						$team2=$team2.'</select></div>';  
						echo $team1;
						echo $team2;
					}
					else
					{
					}
			}
		 }
	}
	
	
	public function manage_match_edit($id)
	{
		 
         $se=$this->session->userdata;
		 if(isset($se['ma']))
		 {
			$key=$this->uri->segment(3);
			if($key=='ms')
			{
				 $data['ms']="Mind Your Own Bussiness!!";
			}
			else
			{
				 $data['ms']="";
			}
	        $t_full_name = $this->manage_match_model->t_full_name();
			$team1=$this->manage_match_model->teamselect();
			$team_match=$this->manage_match_model->match_select($id);
			$data['team_match']=$team_match;
			$data['team1']=$team1;
			$data['full_name']=$t_full_name;
			$data['msg']=$se['ma']->a_name ;
			$data['banner']	="include/banner_view";
			$data['header']	="include/header_view";
 			$data['footer']	="include/footer_view";
			$this->load->view('edit_match_view', $data);
		
		}
	}
	
	
	public function update_managematch()
	{
		 $se=$this->session->userdata;
		 if(isset($se['ma']))
		 {
				$source =$_POST['match_start'];
				$date = new DateTime($source);
				$date1= $date->format('d-m-Y H:i');
				$startdate = strtotime($date1);
				$source1 =$_POST['match_end'];
				$dateconvert = new DateTime($source1);
				$date2= $dateconvert->format('d-m-Y H:i');
				$enddate = strtotime($date2);
				$id=$_POST['teammanageid'];
				$is_active=$_POST['check_status'];
				$a=$this->manage_match_model->checkstatus($id);
				$endmatch=$a->is_active;
				if($endmatch==2){
						
						$config = array();
						$config["base_url"] = base_url() ."admin_panel/manage_match";
						$config["total_rows"] = $this->manage_match_model->record_count();
						$config["per_page"] = 10;
						$config["uri_segment"] = 3;
						$config['next_link'] = '&nbsp;&nbsp;&nbsp;&nbspNext';
						$config['prev_link'] = 'Pre&nbsp;&nbsp;&nbsp;&nbsp';
						$config['cur_tag_open']= '&nbsp;&nbsp;&nbsp;&nbsp;<strong>';
						$config['cur_tag_close']= '&nbsp;&nbsp;&nbsp;&nbsp;</strong>';
						$this->pagination->initialize($config);
						$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
						$data["m_use"] = $this->manage_match_model->fetch_match($config["per_page"], $page);
						$data["links"] = $this->pagination->create_links();
						$data['message']="Match Already Finish...";
						$data['ii']=$this->uri->segment(3);
						$data['ii']=$data['ii']+1;
						$data['msg']=$se['ma']->a_name;
						$data['banner']	="include/banner_view";
						$data['header']	="include/header_view";
						$data['footer']	="include/footer_view";
						$this->load->view('manage_match_view', $data);
				}
				else if($is_active==2){
					
						$checkidforend=$this->manage_match_model->checkidforend($id);
						$config = array();
						$config["base_url"] = base_url() ."admin_panel/manage_match";
						$config["total_rows"] = $this->manage_match_model->record_count();
						$config["per_page"] = 10;
						$config["uri_segment"] = 3;
						$config['next_link'] = '&nbsp;&nbsp;&nbsp;&nbspNext';
						$config['prev_link'] = 'Pre&nbsp;&nbsp;&nbsp;&nbsp';
						$config['cur_tag_open']= '&nbsp;&nbsp;&nbsp;&nbsp;<strong>';
						$config['cur_tag_close']= '&nbsp;&nbsp;&nbsp;&nbsp;</strong>';
						$this->pagination->initialize($config);
						$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
						$data["m_use"] = $this->manage_match_model->fetch_match($config["per_page"], $page);
						$data["links"] = $this->pagination->create_links();
						$data['message']="Match Finish...";
						$data['ii']=$this->uri->segment(3);
						$data['ii']=$data['ii']+1;
						$data['msg']=$se['ma']->a_name;
						$data['banner']	="include/banner_view";
						$data['header']	="include/header_view";
						$data['footer']	="include/footer_view";
						$this->load->view('manage_match_view', $data);
				}
				else{
						$update=array('team1_id'=>$_POST['team1'],'team2_id'=>$_POST['team2'],'match_start_date_time_minute'=>$startdate,'match_end_date_time_minute'=>$enddate,'player_allowed'=>$_POST['player_allowed'],'is_active'=>$_POST['check_status']);
						
						$updatevalue=$this->manage_match_model->editmanagematch($id,$update);
						$config = array();
						$config["base_url"] = base_url() ."admin_panel/manage_match";
						$config["total_rows"] = $this->manage_match_model->record_count();
						$config["per_page"] = 10;
						$config["uri_segment"] = 3;
						$config['next_link'] = '&nbsp;&nbsp;&nbsp;&nbspNext';
						$config['prev_link'] = 'Pre&nbsp;&nbsp;&nbsp;&nbsp';
						$config['cur_tag_open']= '&nbsp;&nbsp;&nbsp;&nbsp;<strong>';
						$config['cur_tag_close']= '&nbsp;&nbsp;&nbsp;&nbsp;</strong>';
						$this->pagination->initialize($config);
						$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
						$data["m_use"] = $this->manage_match_model->fetch_match($config["per_page"], $page);
						$data["links"] = $this->pagination->create_links();
						$data['message']="update successfully...";
						$data['ii']=$this->uri->segment(3);
						$data['ii']=$data['ii']+1;
						$data['msg']=$se['ma']->a_name;
						$data['banner']	="include/banner_view";
						$data['header']	="include/header_view";
						$data['footer']	="include/footer_view";
						$this->load->view('manage_match_view', $data);
				}
		 }
	}
	
	
	public function delete(){
		
		$se=$this->session->userdata;
		 if(isset($se['ma'])){
	
			  $id=$_POST['name'];
			  $m_delete =$this->manage_match_model->m_delete($id);
			  if ($m_delete){
				  
				  echo $m_delete;
			 }
		 }
	}
	
	
	
	public function active()
	{
		$id=$_POST['id'];
		$active=$_POST['active'];
		if($active==1)
		{
			$active=0;
		}
		else
		{
			$active=1;

		}
		$m_active =$this->manage_match_model->m_active($id,$active);
		if ($m_active)
		{
			echo $m_active;
	
		}
	}
	
	
	public function search(){
		
			if(isset($_GET['keys'])){
				 
					$keys = $_GET['keys'];
					redirect('manage_match/search12/'.$keys, 'refresh');
					exit;
			 }
	}
	public function search12()
	{
		$key=$this->uri->segment(3);
		$config["base_url"] = base_url() . 'manage_match/search12/'.$key;
  		$config["total_rows"] = $this->manage_match_model->record_count1($key);
       	$config["per_page"] = 10;
       	$config["uri_segment"] = 4;
		$config['next_link'] = '&nbsp;&nbsp;&nbsp;&nbspNext';
		$config['prev_link'] = 'Pre&nbsp;&nbsp;&nbsp;&nbsp';
		$config['cur_tag_open']= '&nbsp;&nbsp;&nbsp;&nbsp;<strong>';
		$config['cur_tag_close']= '&nbsp;&nbsp;&nbsp;&nbsp;</strong>';
		$this->pagination->initialize($config);
		$page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
		$data["m_use"] = $this->manage_match_model->fetch_search($config["per_page"], $page,$key);
		if($data["m_use"]==""){
			
				$data["err"] ="No Result Found";
		}
		$data["links"] = $this->pagination->create_links();
		$data['message']="";
		$data['ii']=$this->uri->segment(4);
		$data['seg']=$key;
		$data['ii']=$data['ii']+1;
	    $se=$this->session->userdata;
		$data['msg']=$se['ma']->a_name;
		$data['banner']	="include/banner_view";
		$data['header']	="include/header_view";
 		$data['footer']	="include/footer_view";
		$this->load->view('manage_match_view', $data);
	}
}
/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */