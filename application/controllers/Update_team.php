<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Update_team extends CI_Controller {
 
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	  public  function __construct(){
        parent::__construct();
      	$this->load->database();
	    $this->load->library('session');
	    $this ->load->model('setting_model'); 
		$this ->load->model('manage_match_model');
		$this ->load->model('live_score_board_model');
		$this ->load->model('create_your_team_model');
		if(isset($se['log'])){
		$data['msg']=$se['log']->u_name ;
		}

	}
	
	
	public function index(){
	
			 $se=$this->session->userdata;
			 if(isset($se['log'])){
		

					$select='';
					$email=$se['log']->u_email;
					$setting1 =$this->setting_model->select_user_profile($email);
					$data['profile1']=$setting1;
					$searchforid=$setting1->t_id;
					$team_name=$setting1->team_name;
					$tour_team_name=$setting1->m_id;
					$email=$se['log']->u_email;
					$user_id=$se['log']->user_id;
					$select=$this->create_your_team_model->select_user_team($user_id);
					
					$setting =$this->create_your_team_model->select_user_profile($email,$team_name,$tour_team_name);
					if($setting==NULL){
					
							$datetime=$this->create_your_team_model->datetimeselect($email);
							$time=gmdate("m/d/Y H:i",$datetime->created_date);
					
					}
					else{
					
							$time=gmdate(DATE_ATOM,$setting->match_start_date_time_minute);
					}
					$date =time();
					$time = strtotime($time);
					$entry_time=$this->setting_model->get_time();
					$time = $time+(60*$entry_time->time_user_entry);

					//print_r($time);exit;
					//$data['time']=$time;
					date_default_timezone_set("Asia/Calcutta");
				$data['time']=date('H:i:s',$time);
					//print_r($data['time']);exit;
					if($date>$time){
			
							$data['time_passed']='Match has been started 5 minutes before so you can not update your team';
					}
					$data['profile']=$setting;
					$receive=$this->create_your_team_model->fetch_data2($tour_team_name,$user_id);
					if($receive){
					
							$data['message1']="1";
					}
					else{
					
							$data['message1']="";
							$data['message2']="No Result Found...";
					}
											$data['select']=$select;
					$data['score']=$this->cache->get('li_score2');
					//print_r($data);exit;
					$data['players']=$receive;
					$data['msg']=$se['log']->u_name ;
					$data['selected']='manas';
					$data['userid']=$se['log']->user_id;
					$data['footer']	="main/include1/footer";
					$data['header']	="main/include1/header";
					$data['sidebar']	="main/include1/sidebar";
						
					$this->load->view('main/user_update_team',$data);
				}
				else{
						
						redirect('login');
				}
		}
		
		
		public function search(){

				$se=$this->session->userdata;
			 	if(isset($se['log'])){
			 
			 		if(isset($_POST['team1'])){
						
						$searchforid=$_POST['team1'];
						$t_name=$_POST['team_name'];
						$team_name = ucwords(strtolower($t_name));
						$tour_team_name=$_POST['tour_team_name'];
						$email=$se['log']->u_email;
						$user_id=$se['log']->user_id;
						$clear =$this->create_your_team_model->clear($user_id);
						$create_team=$this->create_your_team_model->create_team($team_name,$email,$searchforid,$tour_team_name);
						$setting =$this->create_your_team_model->select_user_profile($email,$team_name,$tour_team_name);
						$data['profile']=$setting;
						$receive=$this->create_your_team_model->fetch_data($tour_team_name);
						if($receive){
						
								$data['message1']="1";
						}
						else{
						
								$data['message1']="";
								$data['message2']="No Result Found...";
						}
						$data['players']=$receive;
						$data['msg']=$se['log']->u_name ;
						$data['selected']='manas';
						$data['footer']	="main/include1/footer";
						$data['header']	="main/include1/header";
						$data['sidebar']	="main/include1/sidebar";
						$this->load->view('main/create_your_team_view',$data);
					}
					else{

							redirect('create_your_team');
					}
				}
				else{
					
					redirect('login');
				}
					
			}
		
		
		public function teamselect(){
				
				$se=$this->session->userdata;
				if(isset($se['log'])){
				
						if(isset($_POST['name'])){
						
								$id=$_POST['name'];
						}
						else{
						
								$id="select";
						}
						if($id=="select"){
							
						}
						else{
								
								date_default_timezone_set("Asia/Calcutta");
 								
								$selectoption=$this->create_your_team_model->select($id);
								if($selectoption>0){
								
									echo '<select id="abc" name="tour_team_name"><option value="select_team">Select your match</option>';
									foreach($selectoption as $key=>$value1){

												$date_start=gmdate('d/m/y',$value1->match_start_date_time_minute); 
												$start_time=date("h:i A",$value1->match_start_date_time_minute);
												echo"<option value=".$value1->m_id.">".$value1->team_name.' VS '.$value1->team2.' '.$date_start.' '.$start_time."</option>";
									}
									echo'</select>';
								}
								else
								{
								}
						}
				}
				else{
					
					redirect('login');
				}
		}
	
	
		public function user_team(){
			
				$se=$this->session->userdata;
				if(isset($se['log'])){
				
					$retrivedate=$this->create_your_team_model->retrivedate($_POST['user_id']);
					if($retrivedate){
							echo "redirectpage";
					}
					else{
						$player_allow='';
						$orignal_team_name='';
						$total_amount='1000';
						$player_id=$_POST['player_id'];
						$user_id=$_POST['user_id'];
						$m_id=$_POST['m_id'];
						$data = array('player_id'=>$player_id,'user_id'=>$user_id);
						$se=$this->session->userdata;
						$email=$se['log']->u_email;
						$user_id=$se['log']->user_id;	
						$create_team=$this->create_your_team_model->create_user_team($user_id,$player_id,$data);
						$select=$this->create_your_team_model->select_user_team($user_id);
						$receive=$this->create_your_team_model->fetch_data2($m_id,$user_id);
						$flag=1;$total="0";
						$a='1';$count=1;
						$count5=0;
						foreach($select as $key=>$value1){
						
								$total=$total+$value1->player_price;
								$count++;
						}
						$sdata=$this->cache->get('li_score2');
						$scount=0;
					if(isset($sdata["score"]["data"]["card"]["players"]))
							{
										foreach($sdata["score"]["data"]["card"]["teams"]["b"]["match"]["playing_xi"]as $key12=>$value12)
										{
										$scount++;	
										}
										foreach($sdata["score"]["data"]["card"]["teams"]["a"]["match"]["playing_xi"]as $key12=>$value12)
										{
										$scount++;	
										}
							}
							//echo $scount;exit;
			 			$a=$this->create_your_team_model->updatepurchageplayer($total,$email,$player_id,$user_id);
						$setting =$this->create_your_team_model->select_user_profile1($email);
						echo "<div class='select_team_rgt'>Remaining : Rs.".$setting->total_account_amount."</div>";
						echo '<div class="row-fluid">
				
							<div class="span4"><h3>'.$setting->team2.'</h3>
							<div class="table-responsive">
		           	 	 	 <table class="table table-bordered table-hover table-striped tablesorter">
		              	 	 <thead>
		                	  <tr>
		                    <th class="header">Name <i class="fa fa-sort"></i></th>
		                    <th class="header">Price <i class="fa fa-sort"></i></th>
		                    <th class="header">Action <i class="fa fa-sort"></i></th>
		                  	</tr>
							</thead>
		                <tbody>';
					//print_r($receive);
				 		if($receive!=''){
						
				 				foreach($receive as $key=>$value){
								$count5=0;
				 								if($setting->team2_id==$value->team_id){
													
													
             
                                                           if($select!=''){
                                                                
                                                                foreach($select as $key=>$value3){
                                                                       
                                                                        if($value3->player_id==$value->player_id){
                                                                               
                                                                                $count5=1;
                                                                        }
                       
                                                                }
                                                        }
														if($count5!=1){
														echo "<tr id=".$value->player_id.">
														
														<td>".$value->player_name;
														if(isset($sdata["score"]["data"]["card"]["players"]))
							{ 
								if($scount=='22'){
										foreach($sdata["score"]["data"]["card"]["players"] as $key13=>$value13)
										{
											if($value->player_name==$sdata["score"]["data"]["card"]["players"][$key13]["fullname"])
											{
												foreach($sdata["score"]["data"]["card"]["teams"]["b"]["match"]["playing_xi"]as $key12=>$value12)
												{
													
												if($value12==$key13)
												echo " (p)";
												}
												foreach($sdata["score"]["data"]["card"]["teams"]["a"]["match"]["playing_xi"]as $key12=>$value12)
												{
													
												if($value12==$key13)
												echo " (p)";
												}
											}
										}
									}
							}
														echo "</td>
														<td>".$value->player_price."</td>
														<td><a href='javascript:void(0)'  onclick=\"return validateForma('".$value->player_id."','".$user_id."','".$m_id."','".$setting->total_account_amount."','".$value->player_price."','".$total."','".$count."','".$setting->player_allowed."')\">Add</a>
														</tr></li>
														";

				 								}}}
echo ' </tbody>
		              </table>
		            </div>

				</div>
				<div class="span4">
					<h3>';
					echo $setting->team1;
					echo'</h3>
					<div class="table-responsive">
		              <table class="table table-bordered table-hover table-striped tablesorter">
		                <thead>
						
		                  <tr>
		                    <th class="header">Name <i class="fa fa-sort"></i></th>
		                    <th class="header">Price <i class="fa fa-sort"></i></th>
		                    <th class="header">Action <i class="fa fa-sort"></i></th>
		                  </tr>
		                </thead>
		                <tbody>';
						foreach($receive as $key=>$value){
						$count5=0;
				 								if($setting->team1_id==$value->team_id){
													
													
             
                                                           if($select!=''){
                                                                
                                                                foreach($select as $key=>$value3){
                                                                       
                                                                        if($value3->player_id==$value->player_id){
                                                                               
                                                                                $count5=1;
                                                                        }
                       
                                                                }
                                                        }
														if($count5!=1){
						
												echo "<tr id=".$value->player_id.">
														
														<td>".$value->player_name;
														if(isset($sdata["score"]["data"]["card"]["players"]))
							{ 
								if($scount=='22'){
										foreach($sdata["score"]["data"]["card"]["players"] as $key13=>$value13)
										{
											if($value->player_name==$sdata["score"]["data"]["card"]["players"][$key13]["fullname"])
											{
												foreach($sdata["score"]["data"]["card"]["teams"]["b"]["match"]["playing_xi"]as $key12=>$value12)
												{
													
												if($value12==$key13)
												echo " (p)";
												}
												foreach($sdata["score"]["data"]["card"]["teams"]["a"]["match"]["playing_xi"]as $key12=>$value12)
												{
													
												if($value12==$key13)
												echo " (p)";
												}
											}
										}
									}
							}
														echo "</td>
														<td>".$value->player_price."</td>
														<td><a href='javascript:void(0)'  onclick=\"return validateForma('".$value->player_id."','".$user_id."','".$m_id."','".$setting->total_account_amount."','".$value->player_price."','".$total."','".$count."','".$setting->player_allowed."')\">Add</a>
														</tr></li>
														";

				 								}}}
												echo' </tbody>
		              </table>
		            </div>

				</div>';$i=1;
	  				$total=0;
	  				if($select!=''){
	  
							echo "<div class='your_team'><h2>".$setting->team_name."</h2> <p> <u><strong>S.N.</strong></u> <strong>Player Name</strong><dfn><strong>Action</strong></dfn> <i><strong>Price</strong></i></p>
							<div id='our_team1'>";
							foreach($select as $key=>$value1){
										
									echo"<p id='".$value1->player_id."'> <u>".$i."</u>".$value1->player_name;
									if(isset($sdata["score"]["data"]["card"]["players"]))
							{ 
								if($scount=='22'){
										foreach($sdata["score"]["data"]["card"]["players"] as $key13=>$value13)
										{
											if($value1->player_name==$sdata["score"]["data"]["card"]["players"][$key13]["fullname"])
											{
												foreach($sdata["score"]["data"]["card"]["teams"]["b"]["match"]["playing_xi"]as $key12=>$value12)
												{
													
												if($value12==$key13)
												echo " (p)";
												}
												foreach($sdata["score"]["data"]["card"]["teams"]["a"]["match"]["playing_xi"]as $key12=>$value12)
												{
													
												if($value12==$key13)
												echo " (p)";
												}
											}
										}
									}
							}
									
									echo "<dfn><a href='javascript:void(0)'onclick=\"return delete_player('".$value1->player_id."','".$user_id."','".$m_id."')\"><img src='".base_url()."public/u_images/multi.png'></a></dfn> <i>".$value1->player_price."</i> </p>";
									$i++;
									$total=$total+$value1->player_price;
							}
							echo"</div>
							<p><strong>Total: ".$total."</strong><dfn><strong></strong></dfn></p></div>";
							echo "<div class='submit_box'><form action='".base_url()."create_your_team/update_profile' method='post' name='sub' id='sub'>
							<input type='hidden' value='".$total."'id='total' name='total' /><input type='hidden' value='".$count."' id='count' name='count'/>
							<input type='button' value='Submit' class='submit_bg' onclick=\"update('".$setting->player_allowed."');\"/> </form></div>";
					}
			
				}}}
				else{
						redirect('login');
				}
		}
	
	public function delete_player(){
	
			$se=$this->session->userdata;
			if(isset($se['log'])){
					$retrivedate=$this->create_your_team_model->retrivedate($_POST['user_id']);
					if($retrivedate){
						
							//redirect('my_team_scroe_live','refresh');
							echo "redirectpage";
					}
					else
					{ 
					$select="";
					$player_allow='';
					$orignal_team_name="";
					$total_amount='1000';
					$user_id=$_POST['user_id'];
					$player_id=$_POST['player_id'];
					$count5=0;
					$m_id=$_POST['m_id'];
					$m_delete =$this->create_your_team_model->m_delete($user_id,$player_id);
					$select=$this->create_your_team_model->select_user_team($user_id);
					$se=$this->session->userdata;
					$email=$se['log']->u_email;
					$user_id=$se['log']->user_id;	
					$setting =$this->create_your_team_model->select_user_profile1($email);
					$receive=$this->create_your_team_model->fetch_data2($m_id,$user_id);
					$flag=1;
					$a='1';$total="0";$count=1;
					if($select==0){
					
							$a=$this->create_your_team_model->selectprice($player_id,$email);
							$setting =$this->create_your_team_model->select_user_profile1($email);
					}
			  		if($select!=''){
			  	
							foreach($select as $key=>$value1){
						
									$count++;
							}
							$a=$this->create_your_team_model->selectprice($player_id,$email);
							$setting =$this->create_your_team_model->select_user_profile1($email);
					}
						$sdata=$this->cache->get('li_score2');
						$scount=0;
						if(isset($sdata["score"]["data"]["card"]["players"]))
						{
								foreach($sdata["score"]["data"]["card"]["teams"]["b"]["match"]["playing_xi"]as $key12=>$value12)
								{
								$scount++;	
								}
								foreach($sdata["score"]["data"]["card"]["teams"]["a"]["match"]["playing_xi"]as $key12=>$value12)
								{
								$scount++;	
								}
						}
						echo "<div class='select_team_rgt'>Remaining : Rs.".$setting->total_account_amount."</div>";
						echo '<div class="row-fluid">
				
				<div class="span4"><h3>'.$setting->team2.'</h3>
					<div class="table-responsive">
		              <table class="table table-bordered table-hover table-striped tablesorter">
		                <thead>
		                  <tr>
		                    <th class="header">Name <i class="fa fa-sort"></i></th>
		                    <th class="header">Price <i class="fa fa-sort"></i></th>
		                    <th class="header">Action <i class="fa fa-sort"></i></th>
		                  </tr>
						</thead>
		                <tbody>';
					//print_r($receive);
				 		if($receive!=''){
						
				 				foreach($receive as $key=>$value){
				 								$count5=0;
				 								if($setting->team2_id==$value->team_id){
													
														if($select!=''){
                                                                
                                                                foreach($select as $key=>$value3){
                                                                       
                                                                        if($value3->player_id==$value->player_id){
                                                                               
                                                                                $count5=1;
                                                                        }
                       
                                                                }
                                                        }
														if($count5!=1){
													
														echo "<tr id=".$value->player_id.">
														
														<td>".$value->player_name;
														if(isset($sdata["score"]["data"]["card"]["players"]))
							{ 
								if($scount=='22'){
										foreach($sdata["score"]["data"]["card"]["players"] as $key13=>$value13)
										{
											if($value->player_name==$sdata["score"]["data"]["card"]["players"][$key13]["fullname"])
											{
												foreach($sdata["score"]["data"]["card"]["teams"]["b"]["match"]["playing_xi"]as $key12=>$value12)
												{
													
												if($value12==$key13)
												echo " (p)";
												}
												foreach($sdata["score"]["data"]["card"]["teams"]["a"]["match"]["playing_xi"]as $key12=>$value12)
												{
													
												if($value12==$key13)
												echo " (p)";
												}
											}
										}
									}
							}
														echo "</td>
														<td>".$value->player_price."</td>
														<td><a href='javascript:void(0)'  onclick=\"return validateForma('".$value->player_id."','".$user_id."','".$m_id."','".$setting->total_account_amount."','".$value->player_price."','".$total."','".$count."','".$setting->player_allowed."')\">Add</a>
														</tr></li>
														";

				 								}}}
echo ' </tbody>
		              </table>
		            </div>

				</div>
				<div class="span4">
					<h3>';
					echo $setting->team1;
					echo'</h3>
					<div class="table-responsive">
		              <table class="table table-bordered table-hover table-striped tablesorter">
		                <thead>
						
		                  <tr>
		                    <th class="header">Name <i class="fa fa-sort"></i></th>
		                    <th class="header">Price <i class="fa fa-sort"></i></th>
		                    <th class="header">Action <i class="fa fa-sort"></i></th>
		                  </tr>
		                </thead>
		                <tbody>';
						foreach($receive as $key=>$value){
						$count5=0;
				 								if($setting->team1_id==$value->team_id){
													
													
             
                                                           if($select!=''){
                                                                
                                                                foreach($select as $key=>$value3){
                                                                       
                                                                        if($value3->player_id==$value->player_id){
                                                                               
                                                                                $count5=1;
                                                                        }
                       
                                                                }
                                                        }
														if($count5!=1){
						
												echo "<tr id=".$value->player_id.">
														
														<td>".$value->player_name;
														if(isset($sdata["score"]["data"]["card"]["players"]))
							{ 
								if($scount=='22'){
										foreach($sdata["score"]["data"]["card"]["players"] as $key13=>$value13)
										{
											if($value->player_name==$sdata["score"]["data"]["card"]["players"][$key13]["fullname"])
											{
												foreach($sdata["score"]["data"]["card"]["teams"]["b"]["match"]["playing_xi"]as $key12=>$value12)
												{
													
												if($value12==$key13)
												echo " (p)";
												}
												foreach($sdata["score"]["data"]["card"]["teams"]["a"]["match"]["playing_xi"]as $key12=>$value12)
												{
													
												if($value12==$key13)
												echo " (p)";
												}
											}
										}
									}
							}
														echo "</td>
														<td>".$value->player_price."</td>
														<td><a href='javascript:void(0)'  onclick=\"return validateForma('".$value->player_id."','".$user_id."','".$m_id."','".$setting->total_account_amount."','".$value->player_price."','".$total."','".$count."','".$setting->player_allowed."')\">Add</a>
														</tr></li>
														";

				 								}}}
												echo' </tbody>
		              </table>
		            </div>

				</div>';$i=1;
	  				$total=0;
	  				if($select!=''){
	  
							echo "<div class='your_team'><h2>".$setting->team_name."</h2> <p> <u><strong>S.N.</strong></u> <strong>Player Name</strong><dfn><strong>Action</strong></dfn> <i><strong>Price</strong></i></p>
							<div id='our_team1'>";
							foreach($select as $key=>$value1){
										
									echo"<p id='".$value1->player_id."'> <u>".$i."</u>".$value1->player_name;
									if(isset($sdata["score"]["data"]["card"]["players"]))
							{ 
								if($scount=='22'){
										foreach($sdata["score"]["data"]["card"]["players"] as $key13=>$value13)
										{
											if($value1->player_name==$sdata["score"]["data"]["card"]["players"][$key13]["fullname"])
											{
												foreach($sdata["score"]["data"]["card"]["teams"]["b"]["match"]["playing_xi"]as $key12=>$value12)
												{
													
												if($value12==$key13)
												echo " (p)";
												}
												foreach($sdata["score"]["data"]["card"]["teams"]["a"]["match"]["playing_xi"]as $key12=>$value12)
												{
													
												if($value12==$key13)
												echo " (p)";
												}
											}
										}
									}
							}
									
									
									
									echo "<dfn><a href='javascript:void(0)'onclick=\"return delete_player('".$value1->player_id."','".$user_id."','".$m_id."')\"><img src='".base_url()."public/u_images/multi.png'></a></dfn> <i>".$value1->player_price."</i> </p>";
									$i++;
									$total=$total+$value1->player_price;
							}
							echo"</div>
							<p><strong>Total: ".$total."</strong><dfn><strong></strong></dfn></p></div>";
							echo "<div class='submit_box'><form action='".base_url()."create_your_team/update_profile' method='post' name='sub' id='sub'>
							<input type='hidden' value='".$total."'id='total' name='total' /><input type='hidden' value='".$count."' id='count' name='count'/>
							<input type='button' value='Submit' class='submit_bg' onclick=\"update('".$setting->player_allowed."');\"/> </form></div>";
					}
			
				}}}
			else{
				
				redirect('login');
			}
		}
	
	public function update_profile(){
           
            $se=$this->session->userdata;
            if(isset($se['log'])){
   
                    $total=$_POST['total'];
                    $count=$_POST['count'];
                    $u_email=$se['log']->u_email;
                    $total_account_amount=$se['log']->total_account_amount;
                    $total_account_amount=$total_account_amount-$total;
                    
                    $data=array('team_selected'=>1);
                    $insert=$this->create_your_team_model->insert_sected_team($u_email,$data);
                    if($insert){
                   
                            redirect('my_team_scroe_live');
                    }
            }
            else{
           
                    redirect('login');
            }
    }

	}
		
/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */