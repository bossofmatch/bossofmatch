<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller {
  
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	 
	  public  function __construct() 
	 {
        parent::__construct();
      	$this->load->database();
	    $this->load->library('session');
	    $this ->load->model('login_user_model');
		  $this ->load->model('livescore_model');
	    $se=$this->session->userdata;
 		$this->load->library("pagination");
 	 	$this ->load->model('winners_model');
		$this->load->helper('livescore_helper');
	//	$this->load->helper('next_match_helper');
	    $se=$this->session->userdata;
 		
		 if(isset($se['log']))
		 {
		 }
		
	}
	public function index()
	{
	
		
	 $se=$this->session->userdata;

		 if(isset($se['log']))
		 {
			  redirect('dashboard');
		 }
		 else
		 {

	

$key=$this->uri->segment(3);

	
	if($key!="")
	{
	$data['message']="You have registered successfully! For create your team, please login now. ";
	}
		$data['header']	="main/include/header_main";
		$data['footer']	="main/include/footer_main";
		$data['top']	="main/include/top_main";
		$this->load->view('main/login_main',$data);
		 }
	
	}
	public function user_login()
	{
		 $se=$this->session->userdata;

		if(isset($se['log']))
		{
		
			redirect('dashboard');
		}
		else
		{
		if(isset($_POST['email1'])&& isset($_POST['password1']))
			{
			$email=$_POST['email1'];
			$password=$_POST['password1'];
			
				$select =$this->login_user_model->sel($email,$password);
			
				if($select){
					$check=$select->is_active;
					if($check!=0){
						$this->session->set_userdata('log',$select);
						$session_id=$this->session->userdata('log',true);
						$update =$this->login_user_model->update_last_login($email);
						
						redirect('dashboard');
					}
					else
					{
					
						$data['msg']="Your are Dactivate by Admin.";
						$data['header']	="main/include/header_main";
						$data['footer']	="main/include/footer_main";
						$data['top']	="main/include/top_main";
						$this->load->view('main/login_main',$data);
					}
				}
				else
				{
									$data['msg']="Email id or Password does not match..";
					$data['header']	="main/include/header_main";
					$data['footer']	="main/include/footer_main";
					$data['top']	="main/include/top_main";
					$this->load->view('main/login_main',$data);
				}
			}
			else
			{
			
				$data['msg']="Email id or Password does not match.";
				
				$data['header']	="main/include/header_main";
				$data['footer']	="main/include/footer_main";
				$data['top']	="main/include/top_main";
				$this->load->view('main/login_main',$data);
			}
		}	
		
	}
	
	public function logout() 
    {
	 $se=$this->session->userdata;
		 if(isset($se['log']))
		 {
		 $this->session->sess_destroy();

		
		redirect('login');
				 
			 
		 }
		 else
		 {
		redirect('login');
		 }
    }
	
}