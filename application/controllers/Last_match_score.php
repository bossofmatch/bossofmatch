<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Last_match_score extends CI_Controller {
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/
<method_name>
* @see http://codeigniter.com/user_guide/general/urls.html
	 */
	  public  function __construct() 
	 {
        parent::__construct();
      	$this->load->database();
	    $this->load->library('session');
	  $this ->load->model('livescore_model');
		 $this ->load->model('manage_member_model');
		 $this ->load->model('helper_model');
		 		 $this ->load->model('winners_model');
		 $this->load->helper('prev_match_helper');
		 
	}
	
	
	public function index()
	{
	 $se=$this->session->userdata;

		if(isset($se['log']))
		{
		
			redirect('dashboard');
		}
		else
		{
//$this->output->cache(.45);
$data=$this->cache->get('prev_match');

		if(!$data)
		{
		$data1=prev_match();
			$this->cache->write($data1, 'prev_match');
			$data = $this->cache->get('prev_match');
			}

$last_match_winner1 =$this->winners_model->last_match_winner1();
$data['last_match_winner1']=$last_match_winner1;
$data['header']	="main/include/header_main";
$data['footer']	="main/include/footer_main";
		
		$this->load->view('main/last_score_view',$data);
	}
	}
	
 
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */