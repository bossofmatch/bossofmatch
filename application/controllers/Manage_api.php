<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Manage_api extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	 
	public  function __construct(){
		
        parent::__construct();
      	$this->load->database();
		  $this ->load->model('livescore_model');
		$this->load->library("pagination");
	    $this->load->library('session');
		$this ->load->model('manage_api_model');
		$this->load->helper('livescore_helper');
		$se=$this->session->userdata;

		if(isset($se['ma']))
		 {
		 }
		 else
		 {	
			$this->load->view('login_view');
		 }
	}
	
	public function index(){
		
		
	}
	
	
	public function search(){
				$data=$this->cache->get('li_score2');

		if(!$data)
		{
			$data1=live_score();
			$this->cache->write($data1, 'li_score2');
			$data = $this->cache->get('li_score2');
			}
				$se=$this->session->userdata;
				if(isset($se['ma'])){
				
							$searchforid=$_POST['team1'];
							$receive=$this->manage_api_model->fetch_data($searchforid);
							 if($receive){
						  
									$data['message1']="1";
									
							  }
							  else{
								  
									$data['message1']="";
									$data['message4']="No Result Found...";
							  }
							  
							  $data['msg']=$se['ma']->a_name;
							  $tournament_name = $this->manage_api_model->tournament_name();
							  $data['tournament_name']=$tournament_name;
							  $data['m_use']=$receive;
							  $data['message3']="";
							  $data['banner']	="include/banner_view";
							  $data['header']	="include/header_view";
							  $data['footer']	="include/footer_view";
							  $this->load->view('manage_api_match_view', $data);
				
				}
	}	
	
	
	public function update(){
			
			$se=$this->session->userdata;
			if(isset($se['ma'])){
				
			  if(isset($_POST['player_id'])){
					
					$player_id=$_POST['player_id'];
					$player_name=$_POST['player_name'];
					$tournament_name = $this->manage_api_model->tournament_name();
					$checkexitsornot=$this->manage_api_model->checkexitsornot($player_id,$player_name);
					$data['tournament_name']=$tournament_name;
					$data['message4']="Update Successfully";
					$data['message3']="1";
					$data['message1']="";
					$data['msg']=$se['ma']->a_name;
					$data['banner']	="include/banner_view";
					$data['header']	="include/header_view";
					$data['footer']	="include/footer_view";
					$this->load->view('manage_api_match_view', $data);
				}
			}
		}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */