<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Manage_email extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	 
	 public  function __construct() 
	 {
        parent::__construct();
      	$this->load->database();
	    $this->load->library('session');
	    $this ->load->model('manage_email_model');
	    $se=$this->session->userdata;
 		$this->load->library("pagination");

		 if(isset($se['ma']))
		 {
		 }
		 else
		 {	
			$this->load->view('login_view');
		 }
	}
	 
	public function index()
	{
		
	}
	
	public function delete()
	{
	
		$id=$_POST['id'];
		$m_delete =$this->manage_email_model->m_delete($id);
		if ($m_delete)
		{
			echo"manas";
		
		}
		
	}
	
	
	public function search()
	{
	 if(isset($_GET['keys'])){
            $keys = $_GET['keys'];
            
            redirect('manage_email/search12/'.$keys, 'refresh');
            exit;
			}
	}
	
	
	public function search12()
	{
			$key=$this->uri->segment(3);

			$config["base_url"] = base_url() . 'manage_email/search12/'.$key;
  			$config["total_rows"] = $this->manage_email_model->record_count1($key);
       		$config["per_page"] = 10;
       		$config["uri_segment"] = 4;
			$config['next_link'] = '&nbsp;&nbsp;&nbsp;&nbspNext';
			$config['prev_link'] = 'Pre&nbsp;&nbsp;&nbsp;&nbsp';
			$config['cur_tag_open']= '&nbsp;&nbsp;&nbsp;&nbsp;<strong>';
			$config['cur_tag_close']= '&nbsp;&nbsp;&nbsp;&nbsp;</strong>';
			$this->pagination->initialize($config);
			$page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
			$data["m_use"] = $this->manage_email_model->fetch_search($config["per_page"], $page,$key);
			if($data["m_use"]=="")
							{
							$data["err"] ="No Result Found";
							}
			$data["links"] = $this->pagination->create_links();
			$data['message']="";
			$data['ii']=$this->uri->segment(4);
			$data['seg']=$key;
			$data['ii']=$data['ii']+1;
			$se=$this->session->userdata;
			$data['msg']=$se['ma']->a_name;
			$data['banner']	="include/banner_view";
			$data['header']	="include/header_view";
			$data['footer']	="include/footer_view";
			$this->load->view('manage_email_view', $data);
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */