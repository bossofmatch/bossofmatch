<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class My_account extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	  public  function __construct(){
        parent::__construct();
      	$this->load->database();
	    $this->load->library('session');
	    $this ->load->model('setting_model');
		 $se=$this->session->userdata;
		 if(isset($se['log'])){
		 }
		 else
		 {
			 redirect('login');
		 }
	   
	}
	
	
	public function index(){
		 $se=$this->session->userdata;
		 if(isset($se['log'])){
		 $email=$se['log']->u_email;
		 $setting =$this->setting_model->select_user_profile($email);
		
		$data['profile']=$setting;
		$data['msg']=$se['log']->u_name ;
		$data['footer']	="main/include1/footer";
		$data['header']	="main/include1/header";
		$data['sidebar']	="main/include1/sidebar";
		$this->load->view('main/my_account_view',$data);
		}
		else{
			redirect('login');
		}
	}

	
	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */