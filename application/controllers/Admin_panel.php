<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin_panel extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	 public  function __construct(){
		 
       	 parent::__construct();
      	 $this->load->database();
	  	 $this->load->library('session');
	     $this ->load->model('login_model');
		 $this ->load->model('manage_member_model');
		 $this ->load->model('manage_tournament_model');
		 $this ->load->model('manage_match_model');
		 $this ->load->model('manage_player_model');
		 $this ->load->model('manage_email_model');
		 $this ->load->model('manage_match_model'); 
		 $this ->load->model('live_score_board_model');
		 $this ->load->model('manage_admin_member_model');
		 $this ->load->model('winners_model');
		 $this ->load->model('manage_api_model');
	     $this->load->library("pagination");
		 $se=$this->session->userdata;
		 $this ->load->model('helper_model');
         $this->load->helper('livescore_helper');
		 $this->load->helper('fmail_helper');

		 if(isset($se['ma'])){
			 
		 }
		 else{
			 	
			$this->load->view('login_view');
		 }
    }
	
	
	public function index(){
		
		$data['msg']="";
 		$se=$this->session->userdata;
		if(isset($se['ma'])){
			
			redirect('admin_panel/dashboard');
		}
	}
	
	
	public function logout(){

		 $se=$this->session->userdata;
		 if(isset($se['ma'])){
			 
			 $this->load->view('login_view');
			 $this->session->sess_destroy();
		 }
	}
	
	public function dashboard(){
		
		$se=$this->session->userdata;
		if(isset($se['ma'])){
			
			$key=$this->uri->segment(3);
			if($key=='ms'){
				
				$data['ms']="Mind Your Own Bussiness!!";
			}
			else{
				
				$data['ms']="";
			}
	
			$data['msg']=$se['ma']->a_name ;
			$data['banner']	="include/banner_view";
			$data['header']	="include/header_view";
 			$data['footer']	="include/footer_view";
			$this->load->view('dashboard_view', $data);
		
		}
	}
	
	
	public function manage_member(){
		
		$se=$this->session->userdata;
		if(isset($se['ma'])){
		  
				$config = array();
    		    $config["base_url"] = base_url() . "admin_panel/manage_member";
				$config["total_rows"] = $this->manage_member_model->record_count();
				$config["per_page"] = 10;
				$config["uri_segment"] = 3;
				$config['next_link'] = '&nbsp;&nbsp;&nbsp;&nbspNext';
				$config['prev_link'] = 'Pre&nbsp;&nbsp;&nbsp;&nbsp';
				$config['cur_tag_open']= '&nbsp;&nbsp;&nbsp;&nbsp;<strong>';
				$config['cur_tag_close']= '&nbsp;&nbsp;&nbsp;&nbsp;</strong>';
	        	$this->pagination->initialize($config);
	      		$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
     			$data["m_use"] = $this->manage_member_model->fetch_member($config["per_page"], $page);
        		$data["links"] = $this->pagination->create_links();
				$data['message']="";
				$data['ii']=$this->uri->segment(3);
				$data['ii']=$data['ii']+1;
				$data['msg']=$se['ma']->a_name;
				$data['banner']	="include/banner_view";
				$data['header']	="include/header_view";
				$data['footer']	="include/footer_view";
				$this->load->view('manage_member_view', $data);
			
		}
	}
	
	
	public function manage_tournament(){
		
		$se=$this->session->userdata;
		if(isset($se['ma'])){
			
			    $config = array();
    		    $config["base_url"] = base_url() . "admin_panel/manage_tournament";
				$config["total_rows"] = $this->manage_tournament_model->record_count();
				$config["per_page"] = 10;
				$config["uri_segment"] = 3;
				$config['next_link'] = '&nbsp;&nbsp;&nbsp;&nbspNext';
				$config['prev_link'] = 'Pre&nbsp;&nbsp;&nbsp;&nbsp';
				$config['cur_tag_open']= '&nbsp;&nbsp;&nbsp;&nbsp;<strong>';
				$config['cur_tag_close']= '&nbsp;&nbsp;&nbsp;&nbsp;</strong>';
	        	$this->pagination->initialize($config);
	      		$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
     			$data["m_use"] = $this->manage_tournament_model->fetch_member($config["per_page"], $page);
        		$data["links"] = $this->pagination->create_links();
				$data['message']="";
				$data['ii']=$this->uri->segment(3);
				$data['ii']=$data['ii']+1;
				$data['msg']=$se['ma']->a_name;
				$data['banner']	="include/banner_view";
				$data['header']	="include/header_view";
				$data['footer']	="include/footer_view";
			    $this->load->view('manage_tournament_view', $data);
		}
	}
	
	
	public function manage_match(){
		
		$se=$this->session->userdata;
		if(isset($se['ma'])){

			
			    $config = array();
    		    $config["base_url"] = base_url() . "admin_panel/manage_match";
				$config["total_rows"] = $this->manage_match_model->record_count();
				$config["per_page"] = 10;
				$config["uri_segment"] = 3;
				$config['next_link'] = '&nbsp;&nbsp;&nbsp;&nbspNext';
				$config['prev_link'] = 'Pre&nbsp;&nbsp;&nbsp;&nbsp';
				$config['cur_tag_open']= '&nbsp;&nbsp;&nbsp;&nbsp;<strong>';
				$config['cur_tag_close']= '&nbsp;&nbsp;&nbsp;&nbsp;</strong>';
	        	$this->pagination->initialize($config);
	      		$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
     			$data["m_use"] = $this->manage_match_model->fetch_match($config["per_page"], $page);
        		$data["links"] = $this->pagination->create_links();
				$data['message']="";
				$data['ii']=$this->uri->segment(3);
				$data['ii']=$data['ii']+1;
				$data['msg']=$se['ma']->a_name;
				$data['banner']	="include/banner_view";
				$data['header']	="include/header_view";
				$data['footer']	="include/footer_view";
			    $this->load->view('manage_match_view', $data);

		
		}
	}
	
	public function manage_player(){
		
			 $se=$this->session->userdata;
		     if(isset($se['ma'])){
				 
					$config = array();
					$config["base_url"] = base_url() . "admin_panel/manage_player";
					$config["total_rows"] = $this->manage_player_model->record_count();
					$config["per_page"] = 10;
					$config["uri_segment"] = 3;
					$config['next_link'] = '&nbsp;&nbsp;&nbsp;&nbspNext';
					$config['prev_link'] = 'Pre&nbsp;&nbsp;&nbsp;&nbsp';
					$config['cur_tag_open']= '&nbsp;&nbsp;&nbsp;&nbsp;<strong>';
					$config['cur_tag_close']= '&nbsp;&nbsp;&nbsp;&nbsp;</strong>';
					$this->pagination->initialize($config);
					$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
					$data["m_use"] = $this->manage_player_model->fetch_match($config["per_page"], $page);
					$data["links"] = $this->pagination->create_links();
					$data['message']="";
					$data['ii']=$this->uri->segment(3);
					$data['ii']=$data['ii']+1;
					$data['msg']=$se['ma']->a_name;
					$data['banner']	="include/banner_view";
					$data['header']	="include/header_view";
					$data['footer']	="include/footer_view";
					$this->load->view('manage_player_view', $data);
			  }
	}
	
	
	public function manage_email(){
		
		$se=$this->session->userdata;
		if(isset($se['ma'])){
			
			    $config = array();
    		    $config["base_url"] = base_url() . "admin_panel/manage_email";
				$config["total_rows"] = $this->manage_email_model->record_count();
				$config["per_page"] = 10;
				$config["uri_segment"] = 3;
				$config['next_link'] = '&nbsp;&nbsp;&nbsp;&nbspNext';
				$config['prev_link'] = 'Pre&nbsp;&nbsp;&nbsp;&nbsp';
				$config['cur_tag_open']= '&nbsp;&nbsp;&nbsp;&nbsp;<strong>';
				$config['cur_tag_close']= '&nbsp;&nbsp;&nbsp;&nbsp;</strong>';
	        	$this->pagination->initialize($config);
	      		$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
     			$data["m_use"] = $this->manage_email_model->fetch_emails($config["per_page"], $page);
        		$data["links"] = $this->pagination->create_links();
				$data['message']="";
				$data['ii']=$this->uri->segment(3);
				$data['ii']=$data['ii']+1;
				$data['msg']=$se['ma']->a_name;
				$data['banner']	="include/banner_view";
				$data['header']	="include/header_view";
				$data['footer']	="include/footer_view";
				$this->load->view('manage_email_view', $data);
		}
	}
	
	
	public function live_score_board(){
		
			$se=$this->session->userdata;
			if(isset($se['ma'])){
			
				$selectoption = $this->live_score_board_model->t_full_name();
				$data['selectoption']=$selectoption;
				$data['msg']=$se['ma']->a_name;
				$data['message1']="";
				$data['banner']	="include/banner_view";
				$data['header']	="include/header_view";
				$data['footer']	="include/footer_view";
				$this->load->view('live_score_board_view', $data);
			}
	}
	
	public function account_setting(){
		
		$se=$this->session->userdata;
			if(isset($se['ma'])){
				
				$data['msg']=$se['ma']->a_name;
				$data['a_email']=$se['ma']->a_email;
				$data['message']="";
				$data['banner']	="include/banner_view";
				$data['header']	="include/header_view";
				$data['footer']	="include/footer_view";
				$this->load->view('manage_account_view', $data);
			}
	}
	
	
	public function manage_admin_account(){
	
		$se=$this->session->userdata;
		
			if(isset($se['ma'])){
				
				$config = array();
				$config["base_url"] = base_url() . "admin_panel/manage_admin_account";
				$config["total_rows"] = $this->manage_admin_member_model->record_count();
				$config["per_page"] = 10;
				$config["uri_segment"] = 3;
				$config['next_link'] = '&nbsp;&nbsp;&nbsp;&nbspNext';
				$config['prev_link'] = 'Pre&nbsp;&nbsp;&nbsp;&nbsp';
				$config['cur_tag_open']= '&nbsp;&nbsp;&nbsp;&nbsp;<strong>';
				$config['cur_tag_close']= '&nbsp;&nbsp;&nbsp;&nbsp;</strong>';
				$this->pagination->initialize($config);
				$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
				$data["m_use"] = $this->manage_admin_member_model->fetch_member($config["per_page"], $page);
				$data["links"] = $this->pagination->create_links();
				$data['message']="";
				$data['ii']=$this->uri->segment(3);
				$data['ii']=$data['ii']+1;
				$data['msg']=$se['ma']->a_name;;
				$data['banner']	="include/banner_view";
				$data['header']	="include/header_view";
				$data['footer']	="include/footer_view";
				$this->load->view('manage_admin_account_view', $data);
			}
	}
	
		public function manage_API(){
			
				$se=$this->session->userdata;
				if(isset($se['ma'])){
			
							$tournament_name = $this->manage_api_model->tournament_name();
							$data['tournament_name']=$tournament_name;
							$data['msg']=$se['ma']->a_name;
							$data['message1']="";
							$data['banner']	="include/banner_view";
							$data['header']	="include/header_view";
							$data['footer']	="include/footer_view";
							$this->load->view('manage_api_match_view', $data);
				}
		}


		public function tournament_view(){

				$se=$this->session->userdata;
				if(isset($se['ma'])){

							$this->output->cache(.45);
							$data=live_score();
							$data['msg']=$se['ma']->a_name;
							$data['banner']	="include/banner_view";
							$data['header']	="include/header_view";
							$data['footer']	="include/footer_view";
							$this->load->view('tournament_manag_view', $data);
				}
		}

	public function winner(){

		$se=$this->session->userdata;
		if(isset($se['ma'])){
		  
			$config = array();
			$config["base_url"] = base_url() . "admin_panel/winner";
			$config["total_rows"] = $this->winners_model->record_count();
			$config["per_page"] = 10;
			$config["uri_segment"] = 3;
			$config['next_link'] = '&nbsp;&nbsp;&nbsp;&nbspNext';
			$config['prev_link'] = 'Pre&nbsp;&nbsp;&nbsp;&nbsp';
			$config['cur_tag_open']= '&nbsp;&nbsp;&nbsp;&nbsp;<strong>';
			$config['cur_tag_close']= '&nbsp;&nbsp;&nbsp;&nbsp;</strong>';
			$this->pagination->initialize($config);
			$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
			$data["m_use"] = $allwinner=$this->winners_model->allwinner($config["per_page"], $page);
			$data["links"] = $this->pagination->create_links();
			$data['message']="";
			$data['ii']=$this->uri->segment(3);
			$data['ii']=$data['ii']+1;
			$data['msg']=$se['ma']->a_name;
			//print_r($allwinner);exit;
			$data['banner']	="include/banner_view";
			$data['header']	="include/header_view";
			$data['footer']	="include/footer_view";
			$this->load->view('match_winner_view', $data);
			
		}
	}
		public function sendmail(){
			$se=$this->session->userdata;
		if(isset($se['ma'])){
			$data['msg']=$se['ma']->a_name;
			$data['banner']	="include/banner_view";
			$data['header']	="include/header_view";
			$data['footer']	="include/footer_view";
			$this->load->view('senmail', $data);
		}
		}
	
	public function fmail(){
$se=$this->session->userdata;
		if(isset($se['ma'])){
			$selemail=$this->winners_model->selemail();
		//print_r($selemail);exit;
			$selmatch=$this->winners_model->selmatch();
			$team_name=$selmatch->team_name.' VS '.$selmatch->team2;
			date_default_timezone_set("Asia/Kolkata");
			$match_start_date_time_minute= date("h:i A",$selmatch->match_start_date_time_minute);
				//print_r($selmatch);exit;
				foreach($selemail  as $key=>$value){
			sfmail($value->u_name,1,$value->u_email,$team_name,$match_start_date_time_minute);
				}
				$data['smsg']='Mail send successfully';
	$data['msg']=$se['ma']->a_name;
			$data['banner']	="include/banner_view";
			$data['header']	="include/header_view";
			$data['footer']	="include/footer_view";
			$this->load->view('senmail', $data);
		}
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */