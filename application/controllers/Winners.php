<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Winners extends CI_Controller {
 
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
		public  function __construct(){
				parent::__construct();
				$this->load->database();
				$this->load->library('session');
				$this ->load->model('login_model');
				$this ->load->model('livescore_model');
				$this ->load->model('winners_model');
				$this->load->helper('livescore_helper');
				$this->load->helper('winner_selfmail_helper');
				//$this->load->helper('next_match_helper');
		}
		public function index(){
			$se=$this->session->userdata;
			if(isset($se['log'])){

				redirect('dashboard');
			}
			else{
				$next_match=$this->cache->get('next_match');
				
				$last_match_winner =$this->winners_model->last_match_winner();
				$data['last_match_winner']=$last_match_winner;
				//print_r($last_match_winner);exit;
				$last_match_winner1 =$this->winners_model->last_match_winner1();
				$data['last_match_winner1']=$last_match_winner1;
				$data['next_match']=$next_match;
				$match_history=$this->winners_model->fech_record();
				$data['header']	="main/include/header_main";
				$data['footer']	="main/include/footer_main";
				$data['top']	="main/include/top_main";
				$data['select']=$match_history;
				$this->load->view('main/user_winner',$data);
			}
		}



			public function my_rank(){
				
				$last_match_winner1 =$this->winners_model->last_match_winner2();
				if($last_match_winner1!=""){
						$i=1;
					echo' <a href="'.base_url().'winners" class="close"><img src="'.base_url().'public/images/close_pop.png" class="btn_close" title="Close Window" alt="Close" /></a>';
					echo '<table border="0"><tr>';foreach($last_match_winner1 as $key1 => $value1){
						echo '<th colspan="4"><div align="center">'.$value1->team_name;echo ' Vs '; echo $value1->team2;
						break;
					}
					echo'</div></th></tr>
						<tr><td>&nbsp;</td>
						<th>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Name</th>
						<th>Rank</th>
						<th>Points</th>
						
						</tr>';
					foreach ($last_match_winner1 as $key => $value) {

								echo' <tr>';
								echo'<td><img src="';
								if($value->user_image==""){
										echo base_url().'public/u_images/user.jpg';
								}
								else{	
									$imagename=explode('public/upload_folder/',$value->user_image);
									$filename = 'public/upload_folder/small_image/'.$imagename[1];
									if(file_exists($filename)){
											echo base_url().$filename;
									}
									else{
											echo base_url().$value->user_image;
									}
								}
								echo'" class="live_image"></td>
								<td>';echo $value->u_name; echo'</li></td>
								<td>';echo $i++; echo'</td>
								<td>';echo $value->total_earn; echo'</td>'
								;
								echo'</tr>';
						}
						echo'</table>';
					} }
					
			public function sendmailtowinner(){
						
						$s_email ="bossofmatch@gmail.com";
							 if($_POST['batches']==1)
								{
									$batchname="Boss of Match";
									$pmoney='50';
								}
								else if($_POST['batches']==2){
									$batchname="Guru of Match";
									$pmoney='10';
									}
										else if($_POST['batches']==3){
											$batchname="Man of Match";
									$pmoney='10';
									}
										else if($_POST['batches']==4){
											$batchname="Attraction of Match";
									$pmoney='10';
									}
									
						winner_selfmail($_POST['u_name'],$s_email,1,$batchname,$_POST['u_email'],$_POST['mobile_no'],$_POST['service_provider'],$pmoney);
						echo"mail sent";
			
			
			}
			
			
				
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */