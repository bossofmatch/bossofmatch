<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login_admin extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	  public  function __construct() 
	 {
        parent::__construct();
      	$this->load->database();
	    $this->load->library('session');
	    $this ->load->model('login_model');
	   
	    $se=$this->session->userdata;

		if(isset($se['ma']))
		{
		}
	}
	
	
	public function index()
	{
		 $se=$this->session->userdata;

		 if(isset($se['ma']))
		 {
			  redirect('welcome/dashboard');
		 }
		 else
		 {
		 $this->load->view('login_view');
		 }
	}
	
	
	public function user_login()
	{  
		 $se=$this->session->userdata;

		if(isset($se['ma']))
		{
			redirect('welcome/dashboard');
		}
		else
		{
			if(isset($_POST['txtUsername'])){
				
				$name=$_POST['txtUsername'];
				$password=$_POST['txtPassword'];
				if(($name!="")&&($password!=""))
				{
					$k =$this->login_model->sel($name,$password);
				
					if($k)
					{
						$check=$k->is_active;
						if($check!=0)
						{
							$this->session->set_userdata('ma',$k);
							$session_id=$this->session->userdata('ma',true);
							$aa=$session_id->a_name;
							$data['ms']="";
							$data['banner']	="include/banner_view";
							$data['header']	="include/header_view";
							$data['footer']	="include/footer_view";
							$data['msg']=$aa;
							$data['about']='admin_panel';
							redirect('admin_panel/dashboard');
						}
						else
						{
							$dataa['msg']="Your are Dactivate by Admin.";
							$this->load->view('login_view',$dataa);
						}
					}
					else
					{
						$dataa['msg']="Email and password do not match.";
						$this->load->view('login_view',$dataa);
					}
				}
				else
				{
					$dataa['msg']="Please enter the Email & Password ";
					$this->load->view('login_view',$dataa);
				}
			}
			else{
				redirect('admin_panel');
			}
		}	
	} 	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */