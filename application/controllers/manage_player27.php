<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Manage_player27 extends CI_Controller {
		

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public  function __construct(){
		
        parent::__construct();
      	$this->load->database();
		$this->load->library("pagination");
	    $this->load->library('session');
		$this ->load->model('manage_player_model');
		$se=$this->session->userdata;

		if(isset($se['ma']))
		 {
		 }
		 else
		 {	
			$this->load->view('login_view');
		 }
	}
	
	public function index(){
		
		
	}
	
	
	public function add_player_open(){
		
		 $se=$this->session->userdata;
		 if(isset($se['ma'])){
		
			  $data['msg']=$se['ma']->a_name;
			  $t_full_name = $this->manage_player_model->t_full_name();
			  $data['full_name']=$t_full_name;
			  $data['banner']	="include/banner_view";
			  $data['header']	="include/header_view";
			  $data['footer']	="include/footer_view";
			  $this->load->view('add_player_view', $data);
		 }
	}
	
	
	public function add_player(){
		
		$se=$this->session->userdata;
		 if(isset($se['ma'])){
		
					  $uni_playe_name=$_POST['player_name'];
					  
					  $trim_name=preg_replace('/\s\s+/', ' ',$uni_playe_name);
		
					  $unique_name=strtolower($trim_name);
			  
					  $replace=str_replace(" ","_",trim($unique_name));
			  
					  $check_name = $this->manage_player_model->check_name($replace);
							  
					  if($check_name){
		
							$data['msg']=$se['ma']->a_name;
							$t_full_name = $this->manage_player_model->t_full_name();
							$data['full_name']=$t_full_name;
							$data['message']="This Player already exits CHOOSE DIFFERENT PLAYER NAME";
							$data['banner']	="include/banner_view";
							$data['header']	="include/header_view";
							$data['footer']	="include/footer_view";
							$this->load->view('add_player_view', $data);
					  }
					  else
					  {
		
					  		$add_player=array('t_id'=>$_POST['t_fullname'],'team_id'=>$_POST['team1'],'unike_payer_name'=>$replace,'player_name'=>$trim_name,'player_price'=>$_POST['player_price']);
							$insert_player=$this->manage_player_model->insert_player($add_player);
							
							$config = array();
							$config["base_url"] = base_url() . "admin_panel/manage_player";
							$config["total_rows"] = $this->manage_player_model->record_count();
							$config["per_page"] = 10;
							$config["uri_segment"] = 3;
							$config['next_link'] = '&nbsp;&nbsp;&nbsp;&nbspNext';
							$config['prev_link'] = 'Pre&nbsp;&nbsp;&nbsp;&nbsp';
							$config['cur_tag_open']= '&nbsp;&nbsp;&nbsp;&nbsp;<strong>';
							$config['cur_tag_close']= '&nbsp;&nbsp;&nbsp;&nbsp;</strong>';
							$this->pagination->initialize($config);
							$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
							$data["m_use"] = $this->manage_player_model->fetch_match($config["per_page"], $page);
							$data["links"] = $this->pagination->create_links();
							$data['message']="Player Add for Tournament";
							$data['ii']=$this->uri->segment(3);
							$data['ii']=$data['ii']+1;
							$data['msg']=$se['ma']->a_name;
							$data['banner']	="include/banner_view";
							$data['header']	="include/header_view";
							$data['footer']	="include/footer_view";
							$this->load->view('manage_player_view', $data);
					  }
		 }
	}
	
	
	public function delete(){
		
		$user_name=$_POST['name'];
		$m_delete =$this->manage_player_model->m_delete($user_name);
		if ($m_delete){
		
			echo $m_delete;
		
		}
	}
	
	
	public function edit_player_open($id,$mid){

			 $se=$this->session->userdata;
			 if(isset($se['ma'])){
		
				  $t_full_name = $this->manage_player_model->t_full_name();
				  $t_id=$this->manage_player_model->t_id($mid);
				  $ediplayer=$this->manage_player_model->ediplayer($id);
				  $data['t_id']=$t_id;
				  $data['full_name']=$t_full_name;				  
				  $data['ediplayer']=$ediplayer;
				  $data['msg']=$se['ma']->a_name;
				  $data['banner']	="include/banner_view";
				  $data['header']	="include/header_view";
				  $data['footer']	="include/footer_view";
				  $this->load->view('edit_player_view', $data);
			 }
	}
	
	
	public function update_player(){
		
			$se=$this->session->userdata;
			if(isset($se['ma'])){
				 
				 	$uni_playe_name=$_POST['player_name'];
					$unique_name=strtolower($uni_playe_name);
					$trim_name=preg_replace('/\s\s+/', ' ',$unique_name);
					$replace=str_replace(" ","_",trim($trim_name));
					
					$uni_playe_name=$_POST['player_name'];
					$unique_name=preg_replace('/\s\s+/', ' ',$uni_playe_name);
					
					$player_id=$_POST['player_id'];
					$edit_player=array('t_id'=>$_POST['t_fullname'],'unike_payer_name'=>$replace,'player_name'=>$unique_name,'player_price'=>$_POST['player_price']);
					$updatepalyer=$this->manage_player_model->update_manage_player($player_id,$edit_player);
					if($updatepalyer){
						
							$config = array();
							$config["base_url"] = base_url() . "admin_panel/manage_player";
							$config["total_rows"] = $this->manage_player_model->record_count();
							$config["per_page"] = 10;
							$config["uri_segment"] = 3;
							$config['next_link'] = '&nbsp;&nbsp;&nbsp;&nbspNext';
							$config['prev_link'] = 'Pre&nbsp;&nbsp;&nbsp;&nbsp';
							$config['cur_tag_open']= '&nbsp;&nbsp;&nbsp;&nbsp;<strong>';
							$config['cur_tag_close']= '&nbsp;&nbsp;&nbsp;&nbsp;</strong>';
							$this->pagination->initialize($config);
							$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
							$data["m_use"] = $this->manage_player_model->fetch_match($config["per_page"], $page);
							$data["links"] = $this->pagination->create_links();
							$data['message']="update player successfully...";
							$data['ii']=$this->uri->segment(3);
							$data['ii']=$data['ii']+1;
							$data['msg']=$se['ma']->a_name;
							$data['banner']	="include/banner_view";
							$data['header']	="include/header_view";
							$data['footer']	="include/footer_view";
							$this->load->view('manage_player_view', $data);
						
						
						
					}
			}
	}
	
	
	public function teamselect()
	{
		    $id=$_POST['name'];
			if($id=="select")
			{				
			}
			else
			{
					$selectoption=$this->manage_player_model->select($id);
					if($selectoption>0)
					{
							$team1='<label style="width:296px;">Select Team *</label><select id="abc" name="team1">';
							foreach($selectoption as $key=>$value1)
							{
										$team1=$team1."<option value=".$value1->team_id.">".$value1->team_name."</option>";
							}
							$team1=$team1.'</select>';
							echo $team1;
					}
					else
					{
					}
			}
	}
	
	
	public function search(){
		
		   if(isset($_GET['keys'])){
			   
			   
				  $keys = $_GET['keys'];
				  
				  redirect('manage_player/search12/'.$keys, 'refresh');
				  exit;
				  }
	}
	
	
	public function search12(){
		
		
		$key=$this->uri->segment(3);

		$config["base_url"] = base_url() . 'manage_player/search12/'.$key;
  		$config["total_rows"] = $this->manage_player_model->record_count1($key);
       	$config["per_page"] = 10;
       	$config["uri_segment"] = 4;
		$config['next_link'] = '&nbsp;&nbsp;&nbsp;&nbspNext';
		$config['prev_link'] = 'Pre&nbsp;&nbsp;&nbsp;&nbsp';
		$config['cur_tag_open']= '&nbsp;&nbsp;&nbsp;&nbsp;<strong>';
		$config['cur_tag_close']= '&nbsp;&nbsp;&nbsp;&nbsp;</strong>';
		$this->pagination->initialize($config);
		$page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
		$data["m_use"] = $this->manage_player_model->fetch_search($config["per_page"], $page,$key);
		if($data["m_use"]==""){
			
			
				$data["err"] ="No Result Found";
		}
		
		$data["links"] = $this->pagination->create_links();
		$data['message']="";
		$data['ii']=$this->uri->segment(4);
		$data['seg']=$key;
		$data['ii']=$data['ii']+1;
	    $se=$this->session->userdata;
		$data['msg']=$se['ma']->a_name;
		$data['banner']	="include/banner_view";
		$data['header']	="include/header_view";
 		$data['footer']	="include/footer_view";
		$this->load->view('manage_player_view', $data);
	}
	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */