<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	
	  public  function __construct() 
	 {
		header('Access-Control-Allow-Origin: *');
		header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
         parent::__construct();
         $this->load->database();
		//  $this->load->library('javascript/jquery');
		 $this->load->library('javascript');
		 $this->output->cache(200);
	     $this->load->library('session');
	     $this ->load->model('player_rates');
		 $this ->load->model('helper_model');
		 $this ->load->model('livescore_model');
		 $this ->load->model('winners_model');
		 $this->load->helper('livescore_helper');
		 // $this->load->helper('next_match_helper');
	    $se=$this->session->userdata;
 		
		
	}
	public function index()
	{
	 $se=$this->session->userdata;
		if(isset($se['log']))
		{
			redirect('dashboard');
		}
		else
		{
			$select1 =$this->player_rates->select_player_rates();
			$select = $this->cache->get('select_player_rates');
			if(!$select){
				$this->cache->save($select1, 'select_player_rates');
				$select = $this->cache->get('select_player_rates');
			}

		$data=$this->cache->get('li_score2');

		if(!$data)
		{
			// $data1=live_score();
			$data1=json_encode(live_score());
			$this->cache->save($data1, 'li_score2');
			$data = $this->cache->get('li_score2');
			}
if(isset($data["score"]["data"]["card"]["start_date"]["iso"]))
{
$date=$data["score"]["data"]["card"]["start_date"]["iso"];
$startdate = strtotime($date);

$select_user =$this->livescore_model->select_user($startdate);
if($select_user!="")
{
foreach($select_user as $key=>$value)
{
$u_id[]=$value->user_id;
}
$select_user_team =$this->livescore_model->select_user_team($u_id);

if($select_user_team!="")
{
$i=1;foreach($select_user as $key44=>$value44) {
   $price=0; 
   foreach($select_user_team as $key=>$value)
 {
 if($value->user_id==$value44->user_id)
			{

			$price=$price+$value->price;
			} 
			}
			if($value44->user_image=="")
			{
			$img="";
			}
			else
			{
				$imagename=explode('public/upload_folder/',$value44->user_image);
				$filename = 'public/upload_folder/small_image/'.$imagename[1];
				if(file_exists($filename)){
					$img=base_url().$filename;
				}
				else{
					$img=base_url().$value44->user_image;
				}
			}

  	$dat[]=array($img,$value44->c_t_time,$value44->team_name,$value44->u_name,$price);
        $i++;

			
    }

if(isset($data['score']['data']['card']['now']['bowling_team'])){
	foreach ($dat as $key => $row) {
    $volume[$key]  = $row['4'];
}
}
else{
foreach ($dat as $key => $row) {
    $volume[$key]  = $row['1'];
}
}
array_multisort($volume, SORT_DESC, $dat);
$data['select_user']=$dat;
}}

}

	$accesskey = $this->cache->get('accesskey');
if(!$accesskey){
	$accesskeyy =$this->player_rates->accesskey();
	$accesskey1 = json_encode($accesskeyy);
$this->cache->save($accesskey1, 'accesskey');
$accesskey = $this->cache->get('accesskey');
}



$data['accesskey']=$accesskey;
//print_r($accesskey);exit;

$count=$this->winners_model->count1();

$last_match_winner =$this->winners_model->last_match_winner();
//print_r($last_match_winner);exit;
$data['last_match_winner']=$last_match_winner;
$data['player_price']=$select;

$data['count']=$count;
	//$data['next_match']=$next_match;

		$data['header']	="main/include/header_main";
		$data['footer']	="main/include/footer_main";
		$data['top']	="main/include/top_main";
		// $data1=json_decode($data);
		$this->load->view('main/index_view',$data);
		}
	}
	
	
public function li_score2()
	{
	
		$this->output->delete_cache();
		$data=$this->cache->get('li_score2');

		if(!$data)
		{
			$data2=live_score();
			$data1=json_encode($data2);
			$this->cache->save($data1, 'li_score2');
			$data = $this->cache->get('li_score2');
			}
			print_r($data);

			echo ' <div class="right_now_point"> <dfn>';
			if(isset($data["time"])){
			$date=$data["time"];
			$startdate = strtotime($date);
			
			$select_user =$this->livescore_model->select_user($startdate);
			if($select_user!="")
			{
				foreach($select_user as $key=>$value)
				{
					$u_id[]=$value->user_id;
				}
				$select_user_team =$this->livescore_model->select_user_team($u_id);

				if($select_user_team!="")
				{
				$i=1;
					foreach($select_user as $key44=>$value44) 
					{
						 $price=0; 
						 foreach($select_user_team as $key=>$value)
							{
							if($value->user_id==$value44->user_id)
							{
	
							$price=$price+$value->price;
							} 
							}
						if($value44->user_image=="")
						{
						$img="";
						}
						else
			{
				$imagename=explode('public/upload_folder/',$value44->user_image);
				$filename = 'public/upload_folder/small_image/'.$imagename[1];
				if(file_exists($filename)){
					$img=base_url().$filename;
				}
				else{
					$img=base_url().$value44->user_image;
				}
			}
	
						$dat[]=array($img,$value44->c_t_time,$value44->team_name,$value44->u_name,$price);
						 $i++;

			
  			  }


				if(isset($data['score']['data']['card']['now']['bowling_team']))
						{
							foreach ($dat as $key => $row) {
 							$volume[$key]  = $row['4'];
							}
						}
				else{
					foreach ($dat as $key => $row) {
    				$volume[$key]  = $row['1'];
				}
			}
			array_multisort($volume, SORT_DESC, $dat);
			}}

			}
			echo'<img src="';
			if(isset($dat))
			{
				if($dat["0"]["0"]!="")
				{
					 echo $dat["0"]["0"];
				 }
				 else
				 {
				 echo base_url().'public/u_images/user.jpg"'; 
				 }
			 }
			 else
			 {
				 echo base_url().'public/u_images/user.jpg"';
			  }
   			echo ' " class="live_image"/><p>';
              if(isset($dat))
			  { 
			    echo $name = ucwords($dat["0"]["3"]);
			  if(isset($data['score']['data']['card']['status']))
			  if($data['score']['data']['card']['status']=='notstarted'){
			  echo "  has created a team for ";
			  if(isset($data['aa'])){echo strtoupper($data['aa'])." VS ".strtoupper($data['bb']); }
			  echo ". Create your team";
			  }
			  else{
			  
			  echo " is ahead by "; 
			  echo $dat["0"]["4"]." points";
			  }
			  }
			  else
			  {
			   echo "Create your team to become Boss Of Match";}
			   if(isset($data['score']['data']['card']['status'])){
			 if($data['score']['data']['card']['status']=='notstarted'){
			 echo ' <a href="'.base_url().'login">Now</a>';
			 }else{
				echo '<br/><a href="'.base_url().'live-scoreboard">All updates</a>';
					}}
				echo'   </p></dfn> </div>
							<div class="right_now_point"> <dfn>';
							
				$last_match_winner1=$this->winners_model->last_match_winner();
				$last_match_winner=$this->cache->get('last_match_winner');
				// print_r($last_match_winner1);
				
				if(!$last_match_winner1)
				{
					$last_match_winner1=json_encode($last_match_winner1);
					$this->cache->save($last_match_winner1, 'last_match_winner');
					$last_match_winner = $this->cache->get('last_match_winner');
				}


				echo'<img src="';
				if(isset($last_match_winner1))
				if($last_match_winner1->user_image!="")
				{
					$imagename=explode('public/upload_folder/',$last_match_winner1->user_image);
					$filename = 'public/upload_folder/small_image/'.$imagename[1];
					if(file_exists($filename)){
							echo base_url().$filename;
					}
					else{
						echo base_url().$last_match_winner1->user_image;
					}
 					
 				}
				else{
						 echo base_url().'public/u_images/user.jpg';
						 }echo'" class="live_image"/>
									  <p>';
						if(isset($last_match_winner1))
						{ 
							echo $fo1 = ucwords($last_match_winner1->u_name).' Won by ';
							echo $last_match_winner1->total_earn.' points (';
							echo $last_match_winner1->team_name.' VS ';
							echo $last_match_winner1->team2;  
						}
						echo') <br>
										<a href="'.base_url().'winners">All updates</a></p>
									  </dfn> </div>
									  <div class="right_now_point"> <dfn><img src="'.base_url().'public/f_img/bom3.png" class="live_image"/><p>';
						if(isset($data['score']['data']['card']['now']['bowling_team']))
						{
							if($data['score']['data']['card']['now']['bowling_team']=='b')
							{
							echo $data['a']."";
							}
							else{
							echo $data['b']."";
							}
							echo $data['score']['data']['card']['now']['runs_str']."<br/>";
							echo strtoupper($data['aa'])." VS ".strtoupper($data['bb']); 
						}
						else
						{ 
						if(isset($data['aa'])){
							echo strtoupper($data['aa'])." VS ".strtoupper($data['bb'])."<br>starts in "; 
							
							 date_default_timezone_set("Asia/Kolkata");
							$t=time();
							$date2= date("Y-m-d H:i",$t);
							$date1 =$data['time'];
							$date1 = strtotime($date1);
							$date1 = date("Y-m-d H:i", $date1);
							/*$date1=str_replace("+","T",$date1);
							$date1=explode("T",$date1);
							$date1=$date1[0].' '.$date1[1];*/
							
							$date1=date_create($date1);
							$date2=date_create($date2);
							$diff=date_diff($date2,$date1);
							if($diff->d!='0')
							{
								echo $diff->d." day";
							}
							else
							{
								if($diff->h!='0')
								{
									$h=1;
									echo $diff->h+$h." hrs";
								}
								else
								{
									echo $diff->i." min.";
								}
							}
						 }
  					}
	
				echo'<br/>
								<a href="'.base_url().'live-scoreboard">See full scoreboard</a></p>
							  </dfn> </div>
				
							<div class="right_now_point"><dfn>';
						
						echo'<img src="'.base_url().'public/f_img/bom3.png" class="live_image"/>'; 
						
				/*$next_match1=next_match();
$next_match=$this->cache->get('next_match');

		if(!$next_match)
		{
			$this->cache->write($next_match1, 'next_match');
			$next_match = $this->cache->get('next_match');
			}*/
$accesskey = $this->cache->get('accesskey');

if(!$accesskey){
	$accesskey1 =$this->player_rates->accesskey();
	$accesskey1 = json_encode($accesskey1);
$this->cache->save($accesskey1, 'accesskey');
$accesskey = $this->cache->get('accesskey');
}
$accesskey2= json_decode($accesskey1);
// print_r($accesskey2->team_name);

        echo'<p>'; echo"Create your team for next match ";
		 if(isset($accesskey1)){
echo $accesskey2->team_name." VS ".$accesskey2->team2;}
		echo'
              <a href="'.base_url().'index.php/login">Click here</a></p>
              </dfn> </div>';
		
		
	}
	
	
	
}