<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('live_score'))
{
    function live_score()
    {
	$CI =&get_instance();
    $CI->load->model('livescore_model');
	$CI->load->model('manage_match_model');
	ini_set('display_startup_errors',1);
	ini_set('display_errors',1);
	error_reporting(-1);
	$access_token = '66892a59fd96dca72a3f5d30fcc5f784';
	$match_ke=$CI->livescore_model->accesskey();
	$data['match_key']	=$match_ke->mkey;
	$match_key=$match_ke->mkey;
	$config=$CI->livescore_model->config();
	$insert=$CI->livescore_model->insert($config);//just for checking api hits	
  	$ch = curl_init();
	// curl_setopt($ch, CURLOPT_URL,"https://api.litzscore.com/rest/v2/match/$match_key/?access_token=66892a59fd96dca72a3f5d30fcc5f784");
	curl_setopt($ch, CURLOPT_URL,"https://api.litzscore.com/rest/v2/match/iplt20_2014_g30/?access_token=66892a59fd96dca72a3f5d30fcc5f784");
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	$server_output = curl_exec ($ch);
	$score=json_decode($server_output, TRUE);
	print_r($score);
	$data['serv']=$score;
	if(isset($score['status_msg']))
	if($score['status_code']=='403'||$score['status_msg']=='Invalid Access Token'){
		$insert=$CI->livescore_model->insert($config);//just for checking api hits	
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL,"https://api.litzscore.com/rest/v2/auth/");
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS,"access_key=86caf516afd31179e21281cf298d6302&secret_key=c68dd992e91418dc6e857c7527e783f7&app_id=enthuons_bossofmatch&device_id=abr344mkd99");
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$server_output = curl_exec ($ch);		
		curl_close ($ch);
		$access = json_decode($server_output);
		if(isset($access->auth->access_token))
		{
			$access_token = $access->auth->access_token;
			$data['access_token']=$access_token;
			$insert=$CI->livescore_model->insert($config);//just for checking api hits	
			$ch = curl_init();
		    curl_setopt($ch, CURLOPT_URL,"https://api.litzscore.com/rest/v2/match/$match_key/?access_token=$access_token");
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			$server_output = curl_exec ($ch);
			$score = json_decode($server_output, TRUE);
		}
	}
	if(isset($score["data"]))
	{
	curl_close ($ch);
	$i=0;
	$count=0;
	$data['a'] =$score["data"]["card"]["teams"]["a"]["name"];
	$data['aa'] =$score["data"]["card"]["teams"]["a"]["key"];
	$data['b'] = $score["data"]["card"]["teams"]["b"]["name"];
	$data['bb'] =$score["data"]["card"]["teams"]["b"]["key"];
	$data['time'] =$score["data"]["card"]["start_date"]["iso"];
 	if(isset($score["data"]["card"]["balls"]))
 	{
		foreach($score["data"]["card"]["balls"] as $key=>$value)
		{
			if($score["data"]["card"]["balls"][$key]['wicket_type']!="")
			{
				$wicket_type=$score["data"]["card"]["balls"][$key]['wicket_type'];
				$bowler=$score["data"]["card"]["balls"][$key]['bowler']['key'];
				$wicket=$score["data"]["card"]["balls"][$key]['wicket'];
				$fielder=$score["data"]["card"]["balls"][$key]['fielder'];
				$select_update =$CI->livescore_model->select_update($wicket);
				$dataa ="";
 				if($select_update==""){
					if($wicket_type=='runout')
					 {
						 $bowler="";
					 }
					$dataa = array('player_key'=>$wicket,'wicket_type'=>$wicket_type,'bowler_key'=>$bowler,'fielder'=>$fielder);}
					if($dataa!="")
					{
						$update =$CI->livescore_model->update($dataa);
					}
				}
			}
		}

		$config=$CI->livescore_model->config();
		$i=0;
 		if(isset($score["data"]["card"]["players"]))
		{
			foreach($score["data"]["card"]["players"] as $key1=>$value1)
			{
				if(isset($score["data"]["card"]["players"][$key1]["match"]["innings"]["1"]["batting"]["runs"]))
				{
					$player_name=$key1;
				 	$runs=$score["data"]["card"]["players"][$key1]["match"]["innings"]["1"]["batting"]["runs"]; 
				  	$update_runs =$CI->livescore_model->update_runs($runs,$player_name,$config[0]->rupess_for_run,$config[0]->rupess_for_wicket);
					$i++;
				 }
			 }
		}
		$i=0;
 		if(isset($score["data"]["card"]["players"]))
		{
			foreach($score["data"]["card"]["players"] as $key2=>$value2)
			{
				if(isset($score["data"]["card"]["players"][$key2]["match"]["innings"]["1"]["bowling"]["wickets"]))
				{
					$player_name=$key2;
              		$wickets=$score["data"]["card"]["players"][$key2]["match"]["innings"]["1"]["bowling"]["wickets"]; 
			 		$update_wickets =$CI->livescore_model->update_wickets($wickets,$player_name,$config[0]->rupess_for_run,$config[0]->rupess_for_wicket);
					$i++;
				}
			 }
		}
		
		//$insert=$CI->livescore_model->insert($config);	
 		$select =$CI->livescore_model->select();
 		$data['select']=$select;
 		$data['score']	=$score;		
		
		$data['score122']	=	$score["data"]["card"]["status"];
 		
			if($score["data"]["card"]["status"]=='completed')
			{		
				$findid=$CI->livescore_model->findid($match_key);
				
				$checkidforend=$CI->manage_match_model->checkidforend($findid->team2_id,$findid->team1_id);
	
			}
			else
			{
				$m_key=array('mkey'=>$match_key);
				$keya =$CI->livescore_model->key_insert($m_key);
			}

 		return $data;
	}
	else
	{
		return $data;
	}
 	}
   }   
